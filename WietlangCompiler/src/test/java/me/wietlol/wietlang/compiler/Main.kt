package me.wietlol.wietlang.compiler

import me.wietlol.common.recursiveMapPreOrder
import me.wietlol.wietlang.generators.llvm.compiler.LlvmCompiler
import me.wietlol.metacode.MetaCodeParser
import me.wietlol.metacode.lexer.FileSource
import me.wietlol.metacode.lexer.MetaCodeLexer
import me.wietlol.metacode.lexer.StreamSource
import me.wietlol.wietlang.ast.AstBuilder
import me.wietlol.wietlang.generators.llvm.LlvmIrGenerator
import me.wietlol.wietlang.pid.multistage.AstToPidConverter
import me.wietlol.windows.ExecutableRunner
import java.io.File
import kotlin.system.measureTimeMillis

object Main
{
	@JvmStatic
	fun main(args: Array<String>)
	{
		val projectDir = args.first()
		
		measureTimeMillis {
			val inputFiles = sequenceOf(File(projectDir).resolve("WietlangCompiler/src/test/resources/wietlang/sdk"))
				.recursiveMapPreOrder {
					if (it.isDirectory)
						it.listFiles().asSequence()
					else
						emptySequence()
				}
				.filter { it.isFile }
				.filter { it.name.endsWith(".wlang") }
				.map { FileSource(it) }
//				.onEach { println("using file ${it.name}") }
//				.map { it.inputStream() }
			
			val outputFile = File(projectDir)
				.resolve("WietlangCompiler/src/test/resources/wietlang/output/LlvmIr.ll")
				.also { it.createNewFile() }
			
			val parserCode = StreamSource("Wietlang.parser.mcg", javaClass.getResourceAsStream("/me/wietlol/wietlang/parser/Wietlang.parser.mcg"))
			
			val lexer = MetaCodeLexer()
			val parser = MetaCodeParser.fromGrammar(parserCode)
			
			val astGenerator = AstBuilder()
			
			inputFiles
				.map { lexer.lex(it) }
				.map { parser.parse("file", it.toList()).first() }
				.forEach { astGenerator.add(it) }
			
			val astProgram = astGenerator.buildProgram()
			
//			println(astProgram.toJson())
			
			val pidConverter = AstToPidConverter()
			
			val (pidProgram, linker) = pidConverter.convert(astProgram)
			
			println(pidProgram.toJson())
			
			val generator = LlvmIrGenerator(linker)
			
			val llvmProgram = generator.generate(pidProgram)
			
			outputFile.writeText(llvmProgram.toIr())
			
			val executable = LlvmCompiler.compile(llvmProgram)
			
			val exitCode = ExecutableRunner.run(executable)
			
			println("Program finished with exit code $exitCode")
		}.also { println("total time: ${it}ms.") }
	}
}