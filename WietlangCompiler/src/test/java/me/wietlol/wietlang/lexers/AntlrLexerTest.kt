//package me.wietlol.wietlang.lexers
//
//import me.wietlol.wietlang.parser.WietlangLexer
//import me.wietlol.wietlang.parser.WietlangParser
//import org.antlr.v4.runtime.CharStreams
//import org.antlr.v4.runtime.CommonTokenStream
//import org.junit.Test
//import kotlin.test.assertEquals
//import kotlin.test.assertTrue
//
//class AntlrLexerTest
//{
//	@Test
//	fun foo()
//	{
//		val sourceCode = "@Native(\"system:text:string\")"
//
//		val lexer = WietlangLexer(CharStreams.fromString(sourceCode))
//		val tokens = CommonTokenStream(lexer)
//		val parser = WietlangParser(tokens)
//
//		val result = parser.annotation()
//
//		assertEquals("Native", result.name.parts[0].text)
//		assertTrue { result.arguments[0] is WietlangParser.LArgumentContext }
//		assertTrue { (result.arguments[0] as WietlangParser.LArgumentContext).mExpression is WietlangParser.LLiteralExpressionContext }
//		assertEquals("\"system:text:string\"", ((result.arguments[0] as WietlangParser.LArgumentContext).mExpression as WietlangParser.LLiteralExpressionContext).value.text)
//	}
//
//	@Test
//	fun bar()
//	{
//		val sourceCode = """@Native("system:text:string")
//			|@PrivateConstructor
//			|class NativeString extends String
//			|{
//			|	@Override
//			|	@Native("system:text:string:getLength")
//			|	method getLength() -> NativeInteger
//			| } """.trimMargin()
//
//		val lexer = WietlangLexer(CharStreams.fromString(sourceCode))
//		val tokens = CommonTokenStream(lexer)
//		val parser = WietlangParser(tokens)
//
//		val result = parser.classDeclaration()
//
//		assertEquals("NativeString", result.name.text)
//	}
//
//	@Test
//	fun testMethod()
//	{
//		val sourceCode = """@Override
//			|@Native("system:text:string:getLength")
//			|method getLength() -> NativeInteger""".trimMargin()
//
//		val lexer = WietlangLexer(CharStreams.fromString(sourceCode))
//		val tokens = CommonTokenStream(lexer)
//		val parser = WietlangParser(tokens)
//
//		val result = parser.classMethodDeclaration()
//
//		assertEquals("getLength", result.name.text)
//	}
//}