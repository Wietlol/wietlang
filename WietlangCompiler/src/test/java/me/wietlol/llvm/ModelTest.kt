package me.wietlol.llvm

import me.wietlol.wietlang.generators.llvm.data.LlvmArrayTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmPointerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmStringConstantDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmVariable
import org.junit.Test
import kotlin.test.assertEquals

class ModelTest
{
	@Test
	fun foo()
	{
		val text = "Hello World!"
		
		assertEquals(12, text.length)
		
		val stringConstantDeclaration = LlvmStringConstantDeclaration.of("foo", text)
		
		assertEquals(13, stringConstantDeclaration.variable.type.reference.size)
	}
	
	@Test
	fun bar()
	{
		val text = "Hello World!"
		
		assertEquals(12, text.length)
		
		val variable = LlvmVariable("bar", true, LlvmPointerTypeReference(LlvmArrayTypeReference(text.length + 1, LlvmIntegerTypeReference(8))))
		
		assertEquals(13, variable.type.reference.size)
		
		println("success")
		println()
	}
	
	@Test
	fun baz()
	{
		val text = "Hello"
		
		assertEquals(5, text.length)
		
		val stringConstantDeclaration = LlvmStringConstantDeclaration.of("baz", text)
		
		assertEquals(6, stringConstantDeclaration.variable.type.reference.size)
	}
	
	private fun generateHelloWorld()
	{
//		val helloWorld = LlvmStringConstantDeclaration.of("c1", "Hello World!")
//
//		val stringDec = LlvmStructDeclaration(
//			"String",
//			listOf(
//				LlvmIntegerTypeReference(32),
//				LlvmPointerTypeReference(
//					LlvmIntegerTypeReference(8)
//				)
//			)
//		)
//
//		val puts = LlvmFunctionDeclaration(
//			LlvmVariable(
//				"puts",
//				true,
//				LlvmPointerTypeReference(
//					LlvmFunctionTypeReference(
//						listOf(
//							LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
//						),
//						LlvmIntegerTypeReference(32)
//					)
//				)
//			)
//		)
//
//		val printLine2 = LlvmFunction(
//			LlvmVariable("printLine2", true),
//			listOf(
//				LlvmStructTypeReference(stringDec)
//			),
//			LlvmVoidTypeReference(),
//			listOf(
//				LlvmExtractValueExpression(
//					LlvmStructTypeReference(stringDec),
//					LlvmVariable("0"),
//					listOf(1),
//					LlvmVariable("v0")
//				),
//				LlvmFunctionCallExpression(
//					puts,
//					listOf(
//						LlvmVariableExpression(LlvmVariable("v0"))
//					),
//					null
//				),
//				LlvmReturnExpression(
//					LlvmVoidTypeReference(),
//					null
//				)
//			)
//		)
//		val printLine = LlvmFunction(
//			LlvmVariable("printLine", true),
//			listOf(
//				LlvmPointerTypeReference(
//					LlvmStructTypeReference(stringDec)
//				)
//			),
//			LlvmVoidTypeReference(),
//			listOf(
//				LlvmLoadExpression(
//					LlvmStructTypeReference(stringDec),
//					LlvmVariableExpression(LlvmVariable("0")),
//					LlvmVariable("v0")
//				),
//				LlvmFunctionCallExpression(
//					printLine2,
//					listOf(
//						LlvmVariableExpression(LlvmVariable("v0"))
//					),
//					null
//				),
//				LlvmReturnExpression(
//					LlvmVoidTypeReference(),
//					null
//				)
//			)
//		)
//		val main = LlvmFunction(
//			LlvmVariable("main", true),
//			listOf(),
//			LlvmNativeIntegerTypeReference(32),
//			listOf(
//				LlvmGetElementPointerExpression(
//					LlvmArrayTypeReference(
//						14,
//						LlvmNativeIntegerTypeReference(8)
//					),
//					LlvmVariableExpression(helloWorld.result),
//					listOf(0, 0),
//					LlvmVariable("v0")
//				),
//				LlvmInsertValueExpression(
//					LlvmStructTypeReference(stringDec),
//					null,
//					LlvmIntegerLiteralExpression(14),
//					0,
//					LlvmVariable("v1")
//				),
//				LlvmInsertValueExpression(
//					LlvmStructTypeReference(stringDec),
//					LlvmVariableExpression(LlvmVariable("v1")),
//					LlvmVariableExpression(LlvmVariable("v0")),
//					1,
//					LlvmVariable("v2")
//				),
//				LlvmAllocateExpression(
//					LlvmStructTypeReference(stringDec),
//					LlvmVariable("v3")
//				),
//				LlvmStoreExpression(
//					LlvmVariableExpression(LlvmVariable("v2")),
//					LlvmStructTypeReference(stringDec),
//					LlvmVariableExpression(LlvmVariable("v3"))
//				),
//				LlvmFunctionCallExpression(
//					printLine2,
//					listOf(
//						LlvmVariableExpression(LlvmVariable("v2"))
//					),
//					null
//				),
//				LlvmFunctionCallExpression(
//					printLine,
//					listOf(
//						LlvmVariableExpression(LlvmVariable("v3"))
//					),
//					null
//				),
//				LlvmReturnExpression(
//					LlvmNativeIntegerTypeReference(32),
//					LlvmIntegerLiteralExpression(0)
//				)
//			)
//		)
//
//		val constants = listOf(
//			helloWorld
//		)
//
//		val types = listOf(
//			stringDec
//		)
//
//		val functions = listOf(
//			LlvmFunctionDeclaration(printLine),
//			LlvmFunctionDeclaration(printLine2),
//			LlvmFunctionDeclaration(puts),
//			LlvmFunctionDeclaration(main)
//		)
//
//		val program = LlvmProgram(
//			constants,
//			types,
//			functions,
//			emptyList()
//		)
//
//		println(program.toIr())
	}
}