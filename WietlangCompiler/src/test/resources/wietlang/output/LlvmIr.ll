target triple = "x86_64-pc-windows-msvc19.15.26726"

@c_0 = private unnamed_addr constant [13 x i8] c"Hello World!\00"

%Class = type { %Class*, i32, i8*(%Class*)* }
%NativeInteger = type { %Class*, i32 }
%NativeString = type { %Class*, i32, i8* }
%String = type { %Class* }

%Class_VTable = type { i8 }
%NativeInteger_VTable = type { i8 }
%NativeString_VTable = type { i8, %NativeInteger*(%NativeString*)* }
%String_VTable = type { i8, %NativeInteger*(%String*)* }

@Class_Class_vTable_data = global %Class_VTable {
	i8 0
}
@Integer_NativeInteger_vTable_data = global %NativeInteger_VTable {
	i8 0
}
@NativeString_String_vTable_data = global %String_VTable {
	i8 0,
	%NativeInteger*(%String*)* bitcast (%NativeInteger*(%NativeString*)* @_getLength to %NativeInteger*(%String*)*)
}
@NativeString_NativeString_vTable_data = global %NativeString_VTable {
	i8 0,
	%NativeInteger*(%NativeString*)* @_getLength
}

define i8* @Class_VTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [ i32 0, label %l0 ]
	l0:
	ret i8* bitcast (%Class_VTable* @Class_Class_vTable_data to i8*)
	none:
	ret i8* null

}
define i8* @NativeInteger_VTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [ i32 1, label %l1 ]
	l1:
	ret i8* bitcast (%NativeInteger_VTable* @Integer_NativeInteger_vTable_data to i8*)
	none:
	ret i8* null

}
define i8* @NativeString_VTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [ i32 3, label %l3 i32 2, label %l2 ]
	l3:
	ret i8* bitcast (%String_VTable* @NativeString_String_vTable_data to i8*)
	l2:
	ret i8* bitcast (%NativeString_VTable* @NativeString_NativeString_vTable_data to i8*)
	none:
	ret i8* null

}
define i8* @String_VTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [  ]
	none:
	ret i8* null

}

@Class_Object = global %Class {
	%Class* @Class_Object,
	i32 0,
	i8*(%Class*)* @Class_VTableLoader
}
@NativeInteger_Object = global %Class {
	%Class* @Class_Object,
	i32 1,
	i8*(%Class*)* @NativeInteger_VTableLoader
}
@NativeString_Object = global %Class {
	%Class* @Class_Object,
	i32 2,
	i8*(%Class*)* @NativeString_VTableLoader
}
@String_Object = global %Class {
	%Class* @Class_Object,
	i32 3,
	i8*(%Class*)* @String_VTableLoader
}

@c1 = global %NativeString {
	%Class* @NativeString_Object,
	i32 12,
	i8* getelementptr ([13 x i8], [13 x i8]* @c_0, i32 0, i32 0)
}

define %NativeInteger* @_main() {
	call void @_printLine(%NativeString* @c1)
	%v1 = getelementptr %NativeString, %NativeString* @c1, i32 0, i32 0
	%v2 = load %Class*, %Class** %v1
	%v3 = load %Class, %Class* %v2
	%v4 = extractvalue %Class %v3, 2
	%v5 = call i8* %v4(%Class* @String_Object)
	%v6 = bitcast i8* %v5 to %String_VTable*
	%v7 = getelementptr %String_VTable, %String_VTable* %v6, i32 0, i32 1
	%v8 = load %NativeInteger*(%String*)*, %NativeInteger*(%String*)** %v7
	%v9 = call %NativeInteger* %v8(%String* bitcast (%NativeString* @c1 to %String*))
	ret %NativeInteger* %v9
}

define void @_printLine(%NativeString*) {
	%v0 = load %NativeString, %NativeString* %0
	%v1 = extractvalue %NativeString %v0, 2
	call i32 @puts(i8* %v1)
	ret void
}

define %NativeInteger* @_getLength(%NativeString*) {
	%v0 = getelementptr %NativeString, %NativeString* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	%v2 = insertvalue %NativeInteger undef, %Class* @NativeInteger_Object, 0
	%v3 = insertvalue %NativeInteger %v2, i32 %v1, 1
	%v4 = alloca %NativeInteger
	store %NativeInteger %v3, %NativeInteger* %v4
	ret %NativeInteger* %v4
}

declare i32 @puts(i8*)

define i32 @main() {
	%v0 = call %NativeInteger* @_main()
	%v1 = getelementptr %NativeInteger, %NativeInteger* %v0, i32 0, i32 1
	%v2 = load i32, i32* %v1
	ret i32 %v2
}


