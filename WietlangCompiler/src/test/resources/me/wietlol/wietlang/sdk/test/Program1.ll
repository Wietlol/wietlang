
target triple = "x86_64-pc-windows-msvc19.15.26726"

@c0 = private unnamed_addr constant [15 x i8] c"Hello Myself!\0A\00"

%NativeString = type { i32, i8* }

define void @_main() {
	%v1 = getelementptr [15 x i8], [15 x i8]* @c0, i32 0, i32 0
	%v2 = insertvalue %NativeString undef, i32 13, 0
	%v3 = insertvalue %NativeString %v2, i8* %v1, 1
	%v4 = alloca %NativeString
	store %NativeString %v3, %NativeString* %v4
	call void @_printLine(%NativeString* %v4)
	ret void
}

define void @_printLine(%NativeString*) {
	%v0 = load %NativeString, %NativeString* %0
	%v1 = extractvalue %NativeString %v0, 1
	call i32 @puts(i8* %v1)
	ret void
}

declare i32 @puts(i8*)

define i32 @main() {
	call void @_main()
	ret i32 0
}


