
target triple = "x86_64-pc-windows-msvc19.15.26726"

%Class = type { i8*(%Class*)* }

%String_vTable = type { ... }
%String = type { %Class*, ... }

%NativeString_vTable = type { i32(%String*)* }
%NativeString = type { %Class*, i32, i8* }

%NativeInteger_vTable = type { i32(%String*)* }
%NativeInteger = type { %Class*, i32, i8* }

@String_Class = global %Class {
	i8*(%Class*)* @String_loadVTable
}

@String_String_vTable_data = global %string_vTable {
    i32(%string*)* @string_getLength
}

@NativeString_Class = global %Class {
	i8*(%Class*)* @NativeString_loadVTable
}

@String_String_vTable_data = global %string_vTable {
    i32(%string*)* @string_getLength
}

@NativeInteger_Class = global %Class {
	i8*(%Class*)* @NativeInteger_loadVTable
}

@String_String_vTable_data = global %string_vTable {
    i32(%string*)* @string_getLength
}

