
target triple = "x86_64-pc-windows-msvc19.15.26726"

; todo dataold
; - statements and expressions
; - type references (especially functions)
; - give variables a type
; todo builder
; - use string constants
; - move string constants below class object declarations (test if this is necessary, I hope not)

@c0 = private unnamed_addr constant [6 x i8] c"Hello\00"

%String = type { %Class* }
%Class = type { %Class*, i32, i8*(%Class*)* }
%NativeInteger = type { %Class*, i32 }
%NativeString = type { %Class*, i32, i8* }

%Class_vTable = type { i8 }
%NativeString_vTable = type { i8, %NativeInteger*(%NativeString*)* }
%String_vTable = type { i8, %NativeInteger*(%String)* }
%NativeInteger_vTable = type { i8 }

@NativeString_String_vTable_data = global %String_vTable {
	i8 0,
	%NativeInteger*(%String)* bitcast (%NativeInteger*(%NativeString*)* @_getLength to %NativeInteger*(%String)*)
}

@NativeString_NativeString_vTable_data = global %NativeString_vTable {
	i8 0,
	%NativeInteger*(%NativeString*)* bitcast (%NativeInteger*(%NativeString*)* @_getLength to %NativeInteger*(%NativeString*)*)
}

@Class_Class_vTable_data = global %Class_vTable {
	i8 0
}

define i8* @Class_vTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [
		i32 0, label %l0
		]
	l0:
	ret i8* bitcast (%Class_vTable* @Class_Class_vTable_data to i8*)
	none:
	ret i8* null
}

define i8* @NativeString_vTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [
		i32 2, label %l2
		i32 1, label %l1
		]
	l2:
	ret i8* bitcast (%String_vTable* @NativeString_String_vTable_data to i8*)
	l1:
	ret i8* bitcast (%NativeString_vTable* @NativeString_NativeString_vTable_data to i8*)
	none:
	ret i8* null
}

define i8* @String_vTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [
		]
	none:
	ret i8* null
}

define i8* @NativeInteger_vTableLoader(%Class*) {
	%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	switch i32 %v1, label %none [
		]
	none:
	ret i8* null
}


@Class_class = global %Class {
	%Class* @Class_class,
	i32 0,
	i8*(%Class*)* @Class_vTableLoader
}

@NativeString_class = global %Class {
	%Class* @Class_class,
	i32 1,
	i8*(%Class*)* @NativeString_vTableLoader
}

@String_class = global %Class {
	%Class* @Class_class,
	i32 2,
	i8*(%Class*)* @String_vTableLoader
}

@NativeInteger_class = global %Class {
	%Class* @Class_class,
	i32 3,
	i8*(%Class*)* @NativeInteger_vTableLoader
}



@c0_ = global %NativeString {
	%Class* @NativeString_class,
	i32 5,
	i8* getelementptr ([6 x i8], [6 x i8]* @c0, i32 0, i32 0)
}

define void @_printLine(%NativeString*) {
	%v0 = load %NativeString, %NativeString* %0
	%v1 = extractvalue %NativeString %v0, 2
	call i32 @puts(i8* %v1)
	ret void
}

define void @_main() {
	%v6 = getelementptr %NativeString, %NativeString* @c0_, i32 0, i32 0
	%v7 = load %Class*, %Class** %v6
	%v8 = load %Class, %Class* %v7
	%v9 = extractvalue %Class %v8, 2
	%v10 = call i8* %v9(%Class* @NativeString_class)
	%v11 = bitcast i8* %v10 to %NativeString_vTable*
	%v12 = getelementptr %NativeString_vTable, %NativeString_vTable* %v11, i32 0, i32 1
	%v13 = load %NativeInteger*(%NativeString*)*, %NativeInteger*(%NativeString*)** %v12
	%v14 = call %NativeInteger* %v13(%NativeString* @c0_)
	ret void
}

define %NativeInteger* @_getLength(%NativeString*) {
	%v0 = getelementptr %NativeString, %NativeString* %0, i32 0, i32 1
	%v1 = load i32, i32* %v0
	%v2 = insertvalue %NativeInteger undef, %Class* @NativeInteger_class, 0
	%v3 = insertvalue %NativeInteger %v2, i32 %v1, 1
	%v4 = alloca %NativeInteger
	store %NativeInteger %v3, %NativeInteger* %v4
	ret %NativeInteger* %v4
}

declare i32 @puts(i8*)

define i32 @main() {
	call void @_main()
	ret i32 0
}


