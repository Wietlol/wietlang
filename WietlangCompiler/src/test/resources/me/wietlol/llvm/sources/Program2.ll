
target triple = "x86_64-pc-windows-msvc19.15.26726"

%class = type { i8*(%class*)* }

%anything_vTable = type { i32(%anything*)* }
%anything = type { %class* }

%string_vTable = type { i32(%string*)* }
%string = type { %class*, i32, i8* }

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@string_string_vTable_data = global %string_vTable {
    i32(%string*)* @string_getLength
}

@string_anything_vTable_data = global %anything_vTable {
    i32(%anything*)* @string_getType
}

@string_class = global %class {
	i8*(%class*)* @string_loadVTable
}

@anything_class = global %class {
	i8*(%class*)* @anything_loadVTable
}

define i8* @string_loadVTable(%class*) {
	l1:
		%c1 = icmp eq %class* %0, @string_class
		br i1 %c1, label %l2, label %l3
	l2:
		%t1 = bitcast %string_vTable* @string_string_vTable_data to i8*
		ret i8* %t1
	l3:
		%c2 = icmp eq %class* %0, @anything_class
		br i1 %c2, label %l4, label %l5
	l4:
		%t2 = bitcast %anything_vTable* @string_anything_vTable_data to i8*
		ret i8* %t2
	l5:
		ret i8* null
}

define i8* @anything_loadVTable(%class*) {
	ret i8* null
}

define i32 @string_getType(%anything*) {
	ret i32 42
}

define i32 @string_getLength(%string*) {
	%value = load %string, %string* %0
	%length = extractvalue %string %value, 1
	ret i32 %length
}

define void @string_construct(%string* %this, i32 %length, i8* %chars) {
	%1 = getelementptr %string, %string* %this, i32 0, i32 0
    store %class* @string_class, %class** %1
    %2 = getelementptr %string, %string* %this, i32 0, i32 1
    store i32 %length, i32* %2
    %3 = getelementptr %string, %string* %this, i32 0, i32 2
    store i8* %chars, i8** %3
	ret void
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


define i32 @main() {
	; stack immediate value
	%t1 = getelementptr [14 x i8], [14 x i8]* @.c1, i64 0, i64 0
	%t2 = insertvalue %string undef, %class* @string_class, 0
	%t3 = insertvalue %string %t2, i32 14, 1
	%l1 = insertvalue %string %t3, i8* %t1, 2

	; stack pointer value
	%l2 = alloca %string
	store %string %l1, %string* %l2

	; heap pointer value
	; todo

;	simple direct call
;	%l3 = call i32 @string_getLength(%string* %l2)

;	single-dispatch call
	%x1 = load %string, %string* %l2
	%clp = extractvalue %string %x1, 0
	%cl = load %class, %class* %clp
	%vTableLoader = extractvalue %class %cl, 0

	%vTableBytesPtr = call i8* %vTableLoader(%class* @string_class)
	%vTablePtr = bitcast i8* %vTableBytesPtr to %string_vTable*
	%vTable = load %string_vTable, %string_vTable* %vTablePtr
	%getLength = extractvalue %string_vTable %vTable, 0
	%l3 = call i32 %getLength(%string* %l2)

;	%vTableBytesPtr = call i8* %vTableLoader(%class* @anything_class)
;	%vTablePtr = bitcast i8* %vTableBytesPtr to %string_vTable*
;	%vTable = load %string_vTable, %string_vTable* %vTablePtr
;	%getLength = extractvalue %string_vTable %vTable, 0
;	%l3 = call i32 %getLength(%string* %l2)

	ret i32 %l3
}


@.c1 = private unnamed_addr constant [14 x i8] c"Hello World!\0A\00"
