; ModuleID = 'Test.cpp'
source_filename = "Test.cpp"
target datalayout = "e-m:w-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-windows-msvc19.15.26726"

%rtti.CompleteObjectLocator = type { i32, i32, i32, i32, i32, i32 }
%rtti.TypeDescriptor7 = type { i8**, i8*, [8 x i8] }
%rtti.ClassHierarchyDescriptor = type { i32, i32, i32, i32 }
%rtti.BaseClassDescriptor = type { i32, i32, i32, i32, i32, i32, i32 }
%class.B = type { i32 (...)**, i32 }
%class.C = type { %class.A, %class.B, i32 }
%class.A = type { i32 (...)**, i32 }

$"?bar@B@@UEAAHXZ" = comdat any

$"?bar@C@@UEAAHXZ" = comdat any

$"??0C@@QEAA@XZ" = comdat any

$"??0B@@QEAA@AEBV0@@Z" = comdat any

$"??0A@@QEAA@XZ" = comdat any

$"??0B@@QEAA@XZ" = comdat any

$"?foo@A@@UEAAHXZ" = comdat any

$"??_7C@@6BA@@@" = comdat largest

$"??_7C@@6BB@@@" = comdat largest

$"??_R4C@@6BA@@@" = comdat any

$"??_R0?AVC@@@8" = comdat any

$"??_R3C@@8" = comdat any

$"??_R2C@@8" = comdat any

$"??_R1A@?0A@EA@C@@8" = comdat any

$"??_R1A@?0A@EA@A@@8" = comdat any

$"??_R0?AVA@@@8" = comdat any

$"??_R3A@@8" = comdat any

$"??_R2A@@8" = comdat any

$"??_R1BA@?0A@EA@B@@8" = comdat any

$"??_R0?AVB@@@8" = comdat any

$"??_R3B@@8" = comdat any

$"??_R2B@@8" = comdat any

$"??_R1A@?0A@EA@B@@8" = comdat any

$"??_R4C@@6BB@@@" = comdat any

$"??_7A@@6B@" = comdat largest

$"??_R4A@@6B@" = comdat any

$"??_7B@@6B@" = comdat largest

$"??_R4B@@6B@" = comdat any

@0 = private unnamed_addr constant { [2 x i8*] } { [2 x i8*] [i8* bitcast (%rtti.CompleteObjectLocator* @"??_R4C@@6BA@@@" to i8*), i8* bitcast (i32 (%class.A*)* @"?foo@A@@UEAAHXZ" to i8*)] }, comdat($"??_7C@@6BA@@@")
@1 = private unnamed_addr constant { [2 x i8*] } { [2 x i8*] [i8* bitcast (%rtti.CompleteObjectLocator* @"??_R4C@@6BB@@@" to i8*), i8* bitcast (i32 (i8*)* @"?bar@C@@UEAAHXZ" to i8*)] }, comdat($"??_7C@@6BB@@@")
@"??_R4C@@6BA@@@" = linkonce_odr constant %rtti.CompleteObjectLocator { i32 1, i32 0, i32 0, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVC@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3C@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.CompleteObjectLocator* @"??_R4C@@6BA@@@" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_7type_info@@6B@" = external constant i8*
@"??_R0?AVC@@@8" = linkonce_odr global %rtti.TypeDescriptor7 { i8** @"??_7type_info@@6B@", i8* null, [8 x i8] c".?AVC@@\00" }, comdat
@__ImageBase = external dso_local constant i8
@"??_R3C@@8" = linkonce_odr constant %rtti.ClassHierarchyDescriptor { i32 0, i32 1, i32 3, i32 trunc (i64 sub nuw nsw (i64 ptrtoint ([4 x i32]* @"??_R2C@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R2C@@8" = linkonce_odr constant [4 x i32] [i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.BaseClassDescriptor* @"??_R1A@?0A@EA@C@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.BaseClassDescriptor* @"??_R1A@?0A@EA@A@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.BaseClassDescriptor* @"??_R1BA@?0A@EA@B@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 0], comdat
@"??_R1A@?0A@EA@C@@8" = linkonce_odr constant %rtti.BaseClassDescriptor { i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVC@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 2, i32 0, i32 -1, i32 0, i32 64, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3C@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R1A@?0A@EA@A@@8" = linkonce_odr constant %rtti.BaseClassDescriptor { i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVA@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 0, i32 0, i32 -1, i32 0, i32 64, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3A@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R0?AVA@@@8" = linkonce_odr global %rtti.TypeDescriptor7 { i8** @"??_7type_info@@6B@", i8* null, [8 x i8] c".?AVA@@\00" }, comdat
@"??_R3A@@8" = linkonce_odr constant %rtti.ClassHierarchyDescriptor { i32 0, i32 0, i32 1, i32 trunc (i64 sub nuw nsw (i64 ptrtoint ([2 x i32]* @"??_R2A@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R2A@@8" = linkonce_odr constant [2 x i32] [i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.BaseClassDescriptor* @"??_R1A@?0A@EA@A@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 0], comdat
@"??_R1BA@?0A@EA@B@@8" = linkonce_odr constant %rtti.BaseClassDescriptor { i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVB@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 0, i32 16, i32 -1, i32 0, i32 64, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3B@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R0?AVB@@@8" = linkonce_odr global %rtti.TypeDescriptor7 { i8** @"??_7type_info@@6B@", i8* null, [8 x i8] c".?AVB@@\00" }, comdat
@"??_R3B@@8" = linkonce_odr constant %rtti.ClassHierarchyDescriptor { i32 0, i32 0, i32 1, i32 trunc (i64 sub nuw nsw (i64 ptrtoint ([2 x i32]* @"??_R2B@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R2B@@8" = linkonce_odr constant [2 x i32] [i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.BaseClassDescriptor* @"??_R1A@?0A@EA@B@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 0], comdat
@"??_R1A@?0A@EA@B@@8" = linkonce_odr constant %rtti.BaseClassDescriptor { i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVB@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 0, i32 0, i32 -1, i32 0, i32 64, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3B@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@"??_R4C@@6BB@@@" = linkonce_odr constant %rtti.CompleteObjectLocator { i32 1, i32 16, i32 0, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVC@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3C@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.CompleteObjectLocator* @"??_R4C@@6BB@@@" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@2 = private unnamed_addr constant { [2 x i8*] } { [2 x i8*] [i8* bitcast (%rtti.CompleteObjectLocator* @"??_R4A@@6B@" to i8*), i8* bitcast (i32 (%class.A*)* @"?foo@A@@UEAAHXZ" to i8*)] }, comdat($"??_7A@@6B@")
@"??_R4A@@6B@" = linkonce_odr constant %rtti.CompleteObjectLocator { i32 1, i32 0, i32 0, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVA@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3A@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.CompleteObjectLocator* @"??_R4A@@6B@" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat
@3 = private unnamed_addr constant { [2 x i8*] } { [2 x i8*] [i8* bitcast (%rtti.CompleteObjectLocator* @"??_R4B@@6B@" to i8*), i8* bitcast (i32 (%class.B*)* @"?bar@B@@UEAAHXZ" to i8*)] }, comdat($"??_7B@@6B@")
@"??_R4B@@6B@" = linkonce_odr constant %rtti.CompleteObjectLocator { i32 1, i32 0, i32 0, i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.TypeDescriptor7* @"??_R0?AVB@@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.ClassHierarchyDescriptor* @"??_R3B@@8" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32), i32 trunc (i64 sub nuw nsw (i64 ptrtoint (%rtti.CompleteObjectLocator* @"??_R4B@@6B@" to i64), i64 ptrtoint (i8* @__ImageBase to i64)) to i32) }, comdat

@"??_7C@@6BA@@@" = unnamed_addr alias i8*, getelementptr inbounds ({ [2 x i8*] }, { [2 x i8*] }* @0, i32 0, i32 0, i32 1)
@"??_7C@@6BB@@@" = unnamed_addr alias i8*, getelementptr inbounds ({ [2 x i8*] }, { [2 x i8*] }* @1, i32 0, i32 0, i32 1)
@"??_7A@@6B@" = unnamed_addr alias i8*, getelementptr inbounds ({ [2 x i8*] }, { [2 x i8*] }* @2, i32 0, i32 0, i32 1)
@"??_7B@@6B@" = unnamed_addr alias i8*, getelementptr inbounds ({ [2 x i8*] }, { [2 x i8*] }* @3, i32 0, i32 0, i32 1)

; Function Attrs: noinline optnone uwtable
define dso_local i32 @"?f@@YAHVB@@@Z"(%class.B*) #0 {
  %2 = call i32 @"?bar@B@@UEAAHXZ"(%class.B* %0)
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local i32 @"?bar@B@@UEAAHXZ"(%class.B*) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.B*, align 8
  store %class.B* %0, %class.B** %2, align 8
  %3 = load %class.B*, %class.B** %2, align 8
  %4 = getelementptr inbounds %class.B, %class.B* %3, i32 0, i32 1
  %5 = load i32, i32* %4, align 8
  ret i32 %5
}

; Function Attrs: noinline optnone uwtable
define dso_local i32 @"?f2@@YAHVC@@@Z"(%class.C*) #0 {
  %2 = bitcast %class.C* %0 to i8*
  %3 = getelementptr i8, i8* %2, i64 16
  %4 = call i32 @"?bar@C@@UEAAHXZ"(i8* %3)
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local i32 @"?bar@C@@UEAAHXZ"(i8*) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.C*, align 8
  %3 = alloca %class.C*, align 8
  %4 = bitcast i8* %0 to %class.C*
  store %class.C* %4, %class.C** %2, align 8
  %5 = load %class.C*, %class.C** %2, align 8
  store %class.C* %5, %class.C** %3, align 8
  %6 = load %class.C*, %class.C** %3, align 8
  %7 = bitcast %class.C* %6 to i8*
  %8 = getelementptr inbounds i8, i8* %7, i32 -16
  %9 = bitcast i8* %8 to %class.C*
  %10 = getelementptr inbounds %class.C, %class.C* %9, i32 0, i32 2
  %11 = load i32, i32* %10, align 8
  ret i32 %11
}

; Function Attrs: noinline norecurse optnone uwtable
define dso_local i32 @main() #2 {
  %1 = alloca i32, align 4
  %2 = alloca %class.C, align 8
  %3 = alloca %class.B, align 8
  store i32 0, i32* %1, align 4
  %4 = call %class.C* @"??0C@@QEAA@XZ"(%class.C* %2) #3
  %5 = bitcast %class.C* %2 to i8*
  %6 = getelementptr inbounds i8, i8* %5, i64 16
  %7 = bitcast i8* %6 to %class.B*
  %8 = call %class.B* @"??0B@@QEAA@AEBV0@@Z"(%class.B* %3, %class.B* dereferenceable(16) %7) #3
  %9 = call i32 @"?f@@YAHVB@@@Z"(%class.B* %3)
  ret i32 42
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local %class.C* @"??0C@@QEAA@XZ"(%class.C* returned) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.C*, align 8
  store %class.C* %0, %class.C** %2, align 8
  %3 = load %class.C*, %class.C** %2, align 8
  %4 = bitcast %class.C* %3 to %class.A*
  %5 = call %class.A* @"??0A@@QEAA@XZ"(%class.A* %4) #3
  %6 = bitcast %class.C* %3 to i8*
  %7 = getelementptr inbounds i8, i8* %6, i64 16
  %8 = bitcast i8* %7 to %class.B*
  %9 = call %class.B* @"??0B@@QEAA@XZ"(%class.B* %8) #3
  %10 = bitcast %class.C* %3 to i32 (...)***
  store i32 (...)** bitcast (i8** @"??_7C@@6BA@@@" to i32 (...)**), i32 (...)*** %10, align 8
  %11 = bitcast %class.C* %3 to i8*
  %12 = getelementptr inbounds i8, i8* %11, i64 16
  %13 = bitcast i8* %12 to i32 (...)***
  store i32 (...)** bitcast (i8** @"??_7C@@6BB@@@" to i32 (...)**), i32 (...)*** %13, align 8
  ret %class.C* %3
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local %class.B* @"??0B@@QEAA@AEBV0@@Z"(%class.B* returned, %class.B* dereferenceable(16)) unnamed_addr #1 comdat align 2 {
  %3 = alloca %class.B*, align 8
  %4 = alloca %class.B*, align 8
  store %class.B* %1, %class.B** %3, align 8
  store %class.B* %0, %class.B** %4, align 8
  %5 = load %class.B*, %class.B** %4, align 8
  %6 = bitcast %class.B* %5 to i32 (...)***
  store i32 (...)** bitcast (i8** @"??_7B@@6B@" to i32 (...)**), i32 (...)*** %6, align 8
  %7 = getelementptr inbounds %class.B, %class.B* %5, i32 0, i32 1
  %8 = load %class.B*, %class.B** %3, align 8
  %9 = getelementptr inbounds %class.B, %class.B* %8, i32 0, i32 1
  %10 = load i32, i32* %9, align 8
  store i32 %10, i32* %7, align 8
  ret %class.B* %5
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local %class.A* @"??0A@@QEAA@XZ"(%class.A* returned) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.A*, align 8
  store %class.A* %0, %class.A** %2, align 8
  %3 = load %class.A*, %class.A** %2, align 8
  %4 = bitcast %class.A* %3 to i32 (...)***
  store i32 (...)** bitcast (i8** @"??_7A@@6B@" to i32 (...)**), i32 (...)*** %4, align 8
  ret %class.A* %3
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local %class.B* @"??0B@@QEAA@XZ"(%class.B* returned) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.B*, align 8
  store %class.B* %0, %class.B** %2, align 8
  %3 = load %class.B*, %class.B** %2, align 8
  %4 = bitcast %class.B* %3 to i32 (...)***
  store i32 (...)** bitcast (i8** @"??_7B@@6B@" to i32 (...)**), i32 (...)*** %4, align 8
  ret %class.B* %3
}

; Function Attrs: noinline nounwind optnone uwtable
define linkonce_odr dso_local i32 @"?foo@A@@UEAAHXZ"(%class.A*) unnamed_addr #1 comdat align 2 {
  %2 = alloca %class.A*, align 8
  store %class.A* %0, %class.A** %2, align 8
  %3 = load %class.A*, %class.A** %2, align 8
  %4 = getelementptr inbounds %class.A, %class.A* %3, i32 0, i32 1
  %5 = load i32, i32* %4, align 8
  ret i32 %5
}

attributes #0 = { noinline optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline norecurse optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 2}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 8.0.0 (tags/RELEASE_800/final)"}
