
target triple = "x86_64-pc-windows-msvc19.15.26726"

; @Native("system:text:string")
; @PrivateConstructor
; class NativeString
%struct.string = type { i32, i8* }

; @Native("system:std:print-line")
; function printLine(text: NativeString)
define void @printLine(%struct.string*) {
	%value = load %struct.string, %struct.string* %0
	call void @printLine2(%struct.string %value)
	ret void
}
define void @printLine2(%struct.string) {
	%chars = extractvalue %struct.string %0, 1
	call i32 @puts(i8* %chars)
	ret void
}
declare i32 @puts(i8* nocapture) nounwind

; @EntryPoint
; function main()
; {
;	printLine("Hello World!")
; }
define i32 @main() {
	; value l1 = "Hello World!"

	; stack immediate value
	%t1 = getelementptr [14 x i8], [14 x i8]* @.c1, i64 0, i64 0
	%t2 = insertvalue %struct.string undef, i32 14, 0
	%l1 = insertvalue %struct.string %t2, i8* %t1, 1

	; stack pointer value
	%l2 = alloca %struct.string
	store %struct.string %l1, %struct.string* %l2

	; heap pointer value
	; todo

	; printLine(l1)
	call void @printLine2(%struct.string %l1)
	call void @printLine(%struct.string* %l2)

	ret i32 0
}
;"Hello World!"
@.c1 = private unnamed_addr constant [14 x i8] c"Hello World!\0A\00"

; annotation Native
; {
;	value key: NativeString
; }
; todo

; annotation PrivateConstructor
; todo
; annotation EntryPoint
; todo
