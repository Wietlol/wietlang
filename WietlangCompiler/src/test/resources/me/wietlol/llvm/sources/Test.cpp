class A {
private:
  int a;

public:
  virtual int foo() { return a; }
};

class B {
private:
  int b;

public:
  virtual int bar() { return b; }
};

class C : public A, public B {
private:
  int c;

public:
  int bar() { return c; }
};

int f(B b) {
  return b.bar();
}

int f2(C c) {
  return c.bar();
}

int main() {
	C c;
	f(c);
	return 42;
}