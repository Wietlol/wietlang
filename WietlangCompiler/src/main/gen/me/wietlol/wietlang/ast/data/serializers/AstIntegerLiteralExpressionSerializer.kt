package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstIntegerLiteralExpressionSerializer : ModelSerializer<AstIntegerLiteralExpression, AstIntegerLiteralExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstIntegerLiteralExpression|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val valueIndex = 1
	private const val tokensIndex = 2
	
	override val modelId: UUID
		get() = UUID.fromString("d14f75cf-c245-4f3f-ab71-4a5f81ed8971")
	override val dataClass: Class<AstIntegerLiteralExpression>
		get() = AstIntegerLiteralExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstIntegerLiteralExpression)
	{
		stream.writeUnsignedVarInt(valueIndex)
		schema.serialize(stream, entity.value)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstIntegerLiteralExpression
	{
		val builder = AstIntegerLiteralExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				valueIndex -> builder.value = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
