package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstFunctionSerializer

interface AstFunction : BitSerializable, AstFirstClassComponent, Jsonable
{
	override val serializationKey: UUID
		get() = AstFunctionSerializer.modelId
	
	override val type: String
		get() = "function"
	override val name: String
	override val imports: Collection<AstImport>
	val returnType: AstTypeReference?
	val body: List<AstExpression>?
	val parameters: List<AstParameter>
	override val annotations: List<AstAnnotationExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstFunction) return false
		
		if (type != other.type) return false
		if (name != other.name) return false
		if (imports != other.imports) return false
		if (returnType != other.returnType) return false
		if (body != other.body) return false
		if (parameters != other.parameters) return false
		if (annotations != other.annotations) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(name)
			.with(imports)
			.with(returnType)
			.with(body)
			.with(parameters)
			.with(annotations)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"name":${name.toJson()},"imports":${imports.toJson()},"returnType":${returnType.toJson()},"body":${body.toJson()},"parameters":${parameters.toJson()},"annotations":${annotations.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(name: String, imports: Collection<AstImport>, returnType: AstTypeReference?, body: List<AstExpression>?, parameters: List<AstParameter>, annotations: List<AstAnnotationExpression>, tokens: List<AstCodeToken>): AstFunction =
			object : AstFunction
			{
				override val name: String = name
				override val imports: Collection<AstImport> = imports
				override val returnType: AstTypeReference? = returnType
				override val body: List<AstExpression>? = body
				override val parameters: List<AstParameter> = parameters
				override val annotations: List<AstAnnotationExpression> = annotations
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
