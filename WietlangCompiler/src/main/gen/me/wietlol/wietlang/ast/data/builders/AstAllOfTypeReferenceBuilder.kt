package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstAllOfTypeReferenceBuilder
{
	var types: MutableList<AstMutableTypeReference> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstAllOfTypeReference =
		AstAllOfTypeReference.of(
			types.toList(),
			tokens.toList()
		)
}
