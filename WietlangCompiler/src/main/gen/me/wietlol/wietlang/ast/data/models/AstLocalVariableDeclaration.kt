package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstLocalVariableDeclarationSerializer

interface AstLocalVariableDeclaration : BitSerializable, AstExpression, AstVariable, Jsonable
{
	override val serializationKey: UUID
		get() = AstLocalVariableDeclarationSerializer.modelId
	
	override val type: String
		get() = "variableDeclaration"
	override val name: String
	val isVariable: Boolean
	override val upperBound: AstTypeReference
	val initialValue: AstExpression?
	override val annotations: List<AstAnnotationExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstLocalVariableDeclaration) return false
		
		if (type != other.type) return false
		if (name != other.name) return false
		if (isVariable != other.isVariable) return false
		if (upperBound != other.upperBound) return false
		if (initialValue != other.initialValue) return false
		if (annotations != other.annotations) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(name)
			.with(isVariable)
			.with(upperBound)
			.with(initialValue)
			.with(annotations)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"name":${name.toJson()},"isVariable":${isVariable.toJson()},"upperBound":${upperBound.toJson()},"initialValue":${initialValue.toJson()},"annotations":${annotations.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(name: String, isVariable: Boolean, upperBound: AstTypeReference, initialValue: AstExpression?, annotations: List<AstAnnotationExpression>, tokens: List<AstCodeToken>): AstLocalVariableDeclaration =
			object : AstLocalVariableDeclaration
			{
				override val name: String = name
				override val isVariable: Boolean = isVariable
				override val upperBound: AstTypeReference = upperBound
				override val initialValue: AstExpression? = initialValue
				override val annotations: List<AstAnnotationExpression> = annotations
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
