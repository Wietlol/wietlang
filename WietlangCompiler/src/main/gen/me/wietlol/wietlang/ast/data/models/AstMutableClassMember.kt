package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableClassMember : AstClassMember, AstMutableComponent
{
}
