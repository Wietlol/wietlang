package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstMethodCallExpressionBuilder
{
	var supplierExpression: AstMutableExpression? = null
	var functionName: String? = null
	var arguments: MutableList<AstMutableArgumentExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstMethodCallExpression =
		AstMethodCallExpression.of(
			supplierExpression!!,
			functionName!!,
			arguments.toList(),
			tokens.toList()
		)
}
