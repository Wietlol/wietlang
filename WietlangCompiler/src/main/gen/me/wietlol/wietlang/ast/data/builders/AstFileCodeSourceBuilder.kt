package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstFileCodeSourceBuilder
{
	var filePath: String? = null
	
	fun build(): AstFileCodeSource =
		AstFileCodeSource.of(
			filePath!!
		)
}
