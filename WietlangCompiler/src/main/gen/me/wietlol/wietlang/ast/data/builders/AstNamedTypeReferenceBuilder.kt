package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstNamedTypeReferenceBuilder
{
	var namePath: AstMutableNamePath? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstNamedTypeReference =
		AstNamedTypeReference.of(
			namePath!!,
			tokens.toList()
		)
}
