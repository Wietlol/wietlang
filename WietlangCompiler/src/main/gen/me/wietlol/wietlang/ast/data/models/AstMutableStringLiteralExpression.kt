package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableStringLiteralExpression : AstStringLiteralExpression, AstMutableLiteralExpression
{
	override var value: String
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(value: String, tokens: MutableList<AstCodeToken>): AstMutableStringLiteralExpression =
			object : AstMutableStringLiteralExpression
			{
				override var value: String = value
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
