package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableProgram : AstProgram, AstNode
{
	override var rootPackage: AstMutablePackage
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(rootPackage: AstMutablePackage, tokens: MutableList<AstCodeToken>): AstMutableProgram =
			object : AstMutableProgram
			{
				override var rootPackage: AstMutablePackage = rootPackage
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
