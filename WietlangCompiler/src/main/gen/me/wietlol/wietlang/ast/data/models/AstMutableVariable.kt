package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableVariable : AstVariable, AstMutableNamedComponent
{
}
