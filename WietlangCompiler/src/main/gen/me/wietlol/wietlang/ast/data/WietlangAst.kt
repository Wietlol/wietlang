package me.wietlol.wietlang.ast.data

import me.wietlol.wietlang.ast.data.serializers.*

object WietlangAst
{
	init
	{
		initialize()
	}
	
	fun init()
	{
		// loaded by classloader
	}
	
	private fun initialize()
	{
		Class.forName(AstCodeTokenSerializer::class.java.name)
		Class.forName(AstFileCodeSourceSerializer::class.java.name)
		Class.forName(AstProgramSerializer::class.java.name)
		Class.forName(AstNamePathSerializer::class.java.name)
		Class.forName(AstImportSerializer::class.java.name)
		Class.forName(AstAnnotationSerializer::class.java.name)
		Class.forName(AstClassSerializer::class.java.name)
		Class.forName(AstFunctionSerializer::class.java.name)
		Class.forName(AstPackageSerializer::class.java.name)
		Class.forName(AstAnnotationValueSerializer::class.java.name)
		Class.forName(AstClassMethodSerializer::class.java.name)
		Class.forName(AstAnnotationExpressionSerializer::class.java.name)
		Class.forName(AstNamedTypeReferenceSerializer::class.java.name)
		Class.forName(AstCompoundTypeReferenceSerializer::class.java.name)
		Class.forName(AstAnyOfTypeReferenceSerializer::class.java.name)
		Class.forName(AstAllOfTypeReferenceSerializer::class.java.name)
		Class.forName(AstParameterSerializer::class.java.name)
		Class.forName(AstArgumentExpressionSerializer::class.java.name)
		Class.forName(AstFunctionCallExpressionSerializer::class.java.name)
		Class.forName(AstMethodCallExpressionSerializer::class.java.name)
		Class.forName(AstNameExpressionSerializer::class.java.name)
		Class.forName(AstReturnExpressionSerializer::class.java.name)
		Class.forName(AstIntegerLiteralExpressionSerializer::class.java.name)
		Class.forName(AstStringLiteralExpressionSerializer::class.java.name)
		Class.forName(AstLocalVariableDeclarationSerializer::class.java.name)
	}
}
