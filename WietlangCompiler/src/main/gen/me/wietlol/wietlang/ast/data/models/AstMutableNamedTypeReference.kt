package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableNamedTypeReference : AstNamedTypeReference, AstMutableTypeReference
{
	override var namePath: AstMutableNamePath
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(namePath: AstMutableNamePath, tokens: MutableList<AstCodeToken>): AstMutableNamedTypeReference =
			object : AstMutableNamedTypeReference
			{
				override var namePath: AstMutableNamePath = namePath
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
