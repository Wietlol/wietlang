package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableReturnExpression : AstReturnExpression, AstMutableExpression
{
	override var valueExpression: AstMutableExpression?
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(valueExpression: AstMutableExpression?, tokens: MutableList<AstCodeToken>): AstMutableReturnExpression =
			object : AstMutableReturnExpression
			{
				override var valueExpression: AstMutableExpression? = valueExpression
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
