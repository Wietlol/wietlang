package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableClass : AstClass, AstMutableFirstClassComponent
{
	override var name: String
	override val imports: MutableCollection<AstMutableImport>
	override val members: MutableList<AstMutableClassMember>
	override val annotations: MutableList<AstMutableAnnotationExpression>
	override val supertypes: MutableList<AstMutableTypeReference>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String, imports: MutableCollection<AstMutableImport>, members: MutableList<AstMutableClassMember>, annotations: MutableList<AstMutableAnnotationExpression>, supertypes: MutableList<AstMutableTypeReference>, tokens: MutableList<AstCodeToken>): AstMutableClass =
			object : AstMutableClass
			{
				override var name: String = name
				override val imports: MutableCollection<AstMutableImport> = imports
				override val members: MutableList<AstMutableClassMember> = members
				override val annotations: MutableList<AstMutableAnnotationExpression> = annotations
				override val supertypes: MutableList<AstMutableTypeReference> = supertypes
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
