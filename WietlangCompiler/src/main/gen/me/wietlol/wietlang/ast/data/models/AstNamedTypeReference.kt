package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstNamedTypeReferenceSerializer

interface AstNamedTypeReference : BitSerializable, AstTypeReference, Jsonable
{
	override val serializationKey: UUID
		get() = AstNamedTypeReferenceSerializer.modelId
	
	override val type: String
		get() = "namedTypeReference"
	val namePath: AstNamePath
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstNamedTypeReference) return false
		
		if (type != other.type) return false
		if (namePath != other.namePath) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(namePath)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"namePath":${namePath.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(namePath: AstNamePath, tokens: List<AstCodeToken>): AstNamedTypeReference =
			object : AstNamedTypeReference
			{
				override val namePath: AstNamePath = namePath
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
