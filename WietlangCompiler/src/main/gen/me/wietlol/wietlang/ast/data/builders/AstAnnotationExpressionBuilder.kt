package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstAnnotationExpressionBuilder
{
	var namePath: AstMutableNamePath? = null
	var arguments: MutableList<AstMutableArgumentExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstAnnotationExpression =
		AstAnnotationExpression.of(
			namePath!!,
			arguments.toList(),
			tokens.toList()
		)
}
