package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutablePackage : AstPackage, AstMutableFirstClassComponent
{
	override var name: String
	override val imports: MutableCollection<AstMutableImport>
	override val members: MutableList<AstMutableFirstClassComponent>
	override val annotations: MutableList<AstMutableAnnotationExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String, imports: MutableCollection<AstMutableImport>, members: MutableList<AstMutableFirstClassComponent>, annotations: MutableList<AstMutableAnnotationExpression>, tokens: MutableList<AstCodeToken>): AstMutablePackage =
			object : AstMutablePackage
			{
				override var name: String = name
				override val imports: MutableCollection<AstMutableImport> = imports
				override val members: MutableList<AstMutableFirstClassComponent> = members
				override val annotations: MutableList<AstMutableAnnotationExpression> = annotations
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
