package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstIntegerLiteralExpressionSerializer

interface AstIntegerLiteralExpression : BitSerializable, AstNumberLiteralExpression, Jsonable
{
	override val serializationKey: UUID
		get() = AstIntegerLiteralExpressionSerializer.modelId
	
	override val type: String
		get() = "integerLiteralExpression"
	override val value: Int
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstIntegerLiteralExpression) return false
		
		if (type != other.type) return false
		if (value != other.value) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(value)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"value":${value.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(value: Int, tokens: List<AstCodeToken>): AstIntegerLiteralExpression =
			object : AstIntegerLiteralExpression
			{
				override val value: Int = value
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
