package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableAnnotationExpression : AstAnnotationExpression, AstNode
{
	override var namePath: AstMutableNamePath
	override val arguments: MutableList<AstMutableArgumentExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(namePath: AstMutableNamePath, arguments: MutableList<AstMutableArgumentExpression>, tokens: MutableList<AstCodeToken>): AstMutableAnnotationExpression =
			object : AstMutableAnnotationExpression
			{
				override var namePath: AstMutableNamePath = namePath
				override val arguments: MutableList<AstMutableArgumentExpression> = arguments
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
