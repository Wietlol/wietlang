package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstFunctionCallExpressionSerializer : ModelSerializer<AstFunctionCallExpression, AstFunctionCallExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstFunctionCallExpression|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val functionNameIndex = 1
	private const val argumentsIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("53daea16-9508-4dcb-84fc-87ee46181723")
	override val dataClass: Class<AstFunctionCallExpression>
		get() = AstFunctionCallExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstFunctionCallExpression)
	{
		stream.writeUnsignedVarInt(functionNameIndex)
		schema.serialize(stream, entity.functionName)
		
		if (entity.arguments.isNotEmpty())
		{
			stream.writeUnsignedVarInt(argumentsIndex)
			entity.arguments.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstFunctionCallExpression
	{
		val builder = AstFunctionCallExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				functionNameIndex -> builder.functionName = schema.deserialize(stream)
				argumentsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.arguments.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
