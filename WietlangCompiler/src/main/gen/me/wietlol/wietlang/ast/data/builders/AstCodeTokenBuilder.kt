package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstCodeTokenBuilder
{
	var text: String? = null
	var type: String? = null
	var line: Int? = null
	var column: Int? = null
	var source: AstCodeSource? = null
	
	fun build(): AstCodeToken =
		AstCodeToken.of(
			text!!,
			type!!,
			line!!,
			column!!,
			source!!
		)
}
