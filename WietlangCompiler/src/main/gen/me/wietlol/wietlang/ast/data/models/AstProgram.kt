package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstProgramSerializer

interface AstProgram : BitSerializable, AstNode, Jsonable
{
	override val serializationKey: UUID
		get() = AstProgramSerializer.modelId
	
	override val type: String
		get() = "program"
	val rootPackage: AstPackage
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstProgram) return false
		
		if (type != other.type) return false
		if (rootPackage != other.rootPackage) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(rootPackage)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"rootPackage":${rootPackage.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(rootPackage: AstPackage, tokens: List<AstCodeToken>): AstProgram =
			object : AstProgram
			{
				override val rootPackage: AstPackage = rootPackage
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
