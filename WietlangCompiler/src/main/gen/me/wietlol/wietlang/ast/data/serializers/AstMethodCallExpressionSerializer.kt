package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstMethodCallExpressionSerializer : ModelSerializer<AstMethodCallExpression, AstMethodCallExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstMethodCallExpression|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val supplierExpressionIndex = 1
	private const val functionNameIndex = 2
	private const val argumentsIndex = 3
	private const val tokensIndex = 4
	
	override val modelId: UUID
		get() = UUID.fromString("1178d7d5-3f73-4514-a3fc-4fea33b35f67")
	override val dataClass: Class<AstMethodCallExpression>
		get() = AstMethodCallExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstMethodCallExpression)
	{
		stream.writeUnsignedVarInt(supplierExpressionIndex)
		schema.serialize(stream, entity.supplierExpression)
		
		stream.writeUnsignedVarInt(functionNameIndex)
		schema.serialize(stream, entity.functionName)
		
		if (entity.arguments.isNotEmpty())
		{
			stream.writeUnsignedVarInt(argumentsIndex)
			entity.arguments.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstMethodCallExpression
	{
		val builder = AstMethodCallExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				supplierExpressionIndex -> builder.supplierExpression = schema.deserialize(stream)
				functionNameIndex -> builder.functionName = schema.deserialize(stream)
				argumentsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.arguments.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
