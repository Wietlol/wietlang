package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstNamePathBuilder
{
	var parts: MutableList<String> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstNamePath =
		AstNamePath.of(
			parts.toList(),
			tokens.toList()
		)
}
