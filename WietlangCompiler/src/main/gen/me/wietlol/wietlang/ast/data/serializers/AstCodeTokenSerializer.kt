package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstCodeTokenSerializer : ModelSerializer<AstCodeToken, AstCodeToken>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstCodeToken|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val textIndex = 1
	private const val typeIndex = 2
	private const val lineIndex = 3
	private const val columnIndex = 4
	private const val sourceIndex = 5
	
	override val modelId: UUID
		get() = UUID.fromString("e97d90ed-4020-44ab-8a8d-9727c2bf082c")
	override val dataClass: Class<AstCodeToken>
		get() = AstCodeToken::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstCodeToken)
	{
		stream.writeUnsignedVarInt(textIndex)
		schema.serialize(stream, entity.text)
		
		stream.writeUnsignedVarInt(typeIndex)
		schema.serialize(stream, entity.type)
		
		stream.writeUnsignedVarInt(lineIndex)
		schema.serialize(stream, entity.line)
		
		stream.writeUnsignedVarInt(columnIndex)
		schema.serialize(stream, entity.column)
		
		stream.writeUnsignedVarInt(sourceIndex)
		schema.serialize(stream, entity.source)
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstCodeToken
	{
		val builder = AstCodeTokenBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				textIndex -> builder.text = schema.deserialize(stream)
				typeIndex -> builder.type = schema.deserialize(stream)
				lineIndex -> builder.line = schema.deserialize(stream)
				columnIndex -> builder.column = schema.deserialize(stream)
				sourceIndex -> builder.source = schema.deserialize(stream)
			}
		}
	}
}
