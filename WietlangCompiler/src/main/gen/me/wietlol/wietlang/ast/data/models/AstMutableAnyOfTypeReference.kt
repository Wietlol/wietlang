package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableAnyOfTypeReference : AstAnyOfTypeReference, AstMutableTypeReference
{
	override val types: MutableList<AstMutableTypeReference>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(types: MutableList<AstMutableTypeReference>, tokens: MutableList<AstCodeToken>): AstMutableAnyOfTypeReference =
			object : AstMutableAnyOfTypeReference
			{
				override val types: MutableList<AstMutableTypeReference> = types
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
