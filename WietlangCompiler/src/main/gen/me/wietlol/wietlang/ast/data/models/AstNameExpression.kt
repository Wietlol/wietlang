package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstNameExpressionSerializer

interface AstNameExpression : BitSerializable, AstExpression, Jsonable
{
	override val serializationKey: UUID
		get() = AstNameExpressionSerializer.modelId
	
	override val type: String
		get() = "nameExpression"
	val path: AstNamePath
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstNameExpression) return false
		
		if (type != other.type) return false
		if (path != other.path) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(path)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"path":${path.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(path: AstNamePath, tokens: List<AstCodeToken>): AstNameExpression =
			object : AstNameExpression
			{
				override val path: AstNamePath = path
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
