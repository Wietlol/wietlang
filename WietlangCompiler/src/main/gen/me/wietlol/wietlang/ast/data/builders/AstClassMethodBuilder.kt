package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstClassMethodBuilder
{
	var name: String? = null
	var returnType: AstMutableTypeReference? = null
	var body: MutableList<AstMutableExpression>? = null
	var parameters: MutableList<AstMutableParameter> = mutableListOf()
	var annotations: MutableList<AstMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstClassMethod =
		AstClassMethod.of(
			name!!,
			returnType,
			body?.toList(),
			parameters.toList(),
			annotations.toList(),
			tokens.toList()
		)
}
