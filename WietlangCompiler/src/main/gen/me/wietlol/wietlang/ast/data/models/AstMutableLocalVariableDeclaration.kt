package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableLocalVariableDeclaration : AstLocalVariableDeclaration, AstMutableExpression, AstMutableVariable
{
	override var name: String
	override val isVariable: Boolean
	override var upperBound: AstMutableTypeReference
	override var initialValue: AstMutableExpression?
	override val annotations: MutableList<AstMutableAnnotationExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String, isVariable: Boolean, upperBound: AstMutableTypeReference, initialValue: AstMutableExpression?, annotations: MutableList<AstMutableAnnotationExpression>, tokens: MutableList<AstCodeToken>): AstMutableLocalVariableDeclaration =
			object : AstMutableLocalVariableDeclaration
			{
				override var name: String = name
				override val isVariable: Boolean = isVariable
				override var upperBound: AstMutableTypeReference = upperBound
				override var initialValue: AstMutableExpression? = initialValue
				override val annotations: MutableList<AstMutableAnnotationExpression> = annotations
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
