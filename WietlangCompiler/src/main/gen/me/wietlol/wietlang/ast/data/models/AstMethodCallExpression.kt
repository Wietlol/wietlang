package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstMethodCallExpressionSerializer

interface AstMethodCallExpression : BitSerializable, AstExpression, Jsonable
{
	override val serializationKey: UUID
		get() = AstMethodCallExpressionSerializer.modelId
	
	override val type: String
		get() = "methodCallExpression"
	val supplierExpression: AstExpression
	val functionName: String
	val arguments: List<AstArgumentExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstMethodCallExpression) return false
		
		if (type != other.type) return false
		if (supplierExpression != other.supplierExpression) return false
		if (functionName != other.functionName) return false
		if (arguments != other.arguments) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(supplierExpression)
			.with(functionName)
			.with(arguments)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"supplierExpression":${supplierExpression.toJson()},"functionName":${functionName.toJson()},"arguments":${arguments.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(supplierExpression: AstExpression, functionName: String, arguments: List<AstArgumentExpression>, tokens: List<AstCodeToken>): AstMethodCallExpression =
			object : AstMethodCallExpression
			{
				override val supplierExpression: AstExpression = supplierExpression
				override val functionName: String = functionName
				override val arguments: List<AstArgumentExpression> = arguments
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
