package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstClassBuilder
{
	var name: String? = null
	var imports: MutableCollection<AstMutableImport> = mutableListOf()
	var members: MutableList<AstMutableClassMember> = mutableListOf()
	var annotations: MutableList<AstMutableAnnotationExpression> = mutableListOf()
	var supertypes: MutableList<AstMutableTypeReference> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstClass =
		AstClass.of(
			name!!,
			imports.toList(),
			members.toList(),
			annotations.toList(),
			supertypes.toList(),
			tokens.toList()
		)
}
