package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstNamePathSerializer : ModelSerializer<AstNamePath, AstNamePath>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstNamePath|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val partsIndex = 1
	private const val tokensIndex = 2
	
	override val modelId: UUID
		get() = UUID.fromString("6a39ef51-8a64-45aa-938b-255b906b8f39")
	override val dataClass: Class<AstNamePath>
		get() = AstNamePath::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstNamePath)
	{
		if (entity.parts.isNotEmpty())
		{
			stream.writeUnsignedVarInt(partsIndex)
			entity.parts.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstNamePath
	{
		val builder = AstNamePathBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				partsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.parts.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
