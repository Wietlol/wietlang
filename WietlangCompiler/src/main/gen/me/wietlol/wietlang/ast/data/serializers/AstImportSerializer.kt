package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstImportSerializer : ModelSerializer<AstImport, AstImport>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstImport|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val pathIndex = 1
	private const val aliasIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("e9ddbf6b-f7e4-4f01-bee7-65140cc9577d")
	override val dataClass: Class<AstImport>
		get() = AstImport::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstImport)
	{
		stream.writeUnsignedVarInt(pathIndex)
		schema.serialize(stream, entity.path)
		
		val alias = entity.alias
		if (alias != null)
		{
			stream.writeUnsignedVarInt(aliasIndex)
			schema.serialize(stream, alias)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstImport
	{
		val builder = AstImportBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				pathIndex -> builder.path = schema.deserialize(stream)
				aliasIndex -> builder.alias = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
