package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstFunctionCallExpressionSerializer

interface AstFunctionCallExpression : BitSerializable, AstExpression, Jsonable
{
	override val serializationKey: UUID
		get() = AstFunctionCallExpressionSerializer.modelId
	
	override val type: String
		get() = "functionCallExpression"
	val functionName: String
	val arguments: List<AstArgumentExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstFunctionCallExpression) return false
		
		if (type != other.type) return false
		if (functionName != other.functionName) return false
		if (arguments != other.arguments) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(functionName)
			.with(arguments)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"functionName":${functionName.toJson()},"arguments":${arguments.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(functionName: String, arguments: List<AstArgumentExpression>, tokens: List<AstCodeToken>): AstFunctionCallExpression =
			object : AstFunctionCallExpression
			{
				override val functionName: String = functionName
				override val arguments: List<AstArgumentExpression> = arguments
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
