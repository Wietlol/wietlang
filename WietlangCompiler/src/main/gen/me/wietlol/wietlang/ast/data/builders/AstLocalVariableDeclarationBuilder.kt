package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstLocalVariableDeclarationBuilder
{
	var name: String? = null
	var isVariable: Boolean? = null
	var upperBound: AstMutableTypeReference? = null
	var initialValue: AstMutableExpression? = null
	var annotations: MutableList<AstMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstLocalVariableDeclaration =
		AstLocalVariableDeclaration.of(
			name!!,
			isVariable!!,
			upperBound!!,
			initialValue,
			annotations.toList(),
			tokens.toList()
		)
}
