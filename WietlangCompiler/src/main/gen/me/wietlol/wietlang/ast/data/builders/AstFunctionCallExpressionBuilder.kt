package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstFunctionCallExpressionBuilder
{
	var functionName: String? = null
	var arguments: MutableList<AstMutableArgumentExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstFunctionCallExpression =
		AstFunctionCallExpression.of(
			functionName!!,
			arguments.toList(),
			tokens.toList()
		)
}
