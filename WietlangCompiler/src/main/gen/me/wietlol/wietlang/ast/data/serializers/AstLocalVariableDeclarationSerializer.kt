package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstLocalVariableDeclarationSerializer : ModelSerializer<AstLocalVariableDeclaration, AstLocalVariableDeclaration>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstLocalVariableDeclaration|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val nameIndex = 1
	private const val isVariableIndex = 2
	private const val upperBoundIndex = 3
	private const val initialValueIndex = 4
	private const val annotationsIndex = 5
	private const val tokensIndex = 6
	
	override val modelId: UUID
		get() = UUID.fromString("5d732e1a-ed04-41dd-821b-ed04c516b1d3")
	override val dataClass: Class<AstLocalVariableDeclaration>
		get() = AstLocalVariableDeclaration::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstLocalVariableDeclaration)
	{
		stream.writeUnsignedVarInt(nameIndex)
		schema.serialize(stream, entity.name)
		
		stream.writeUnsignedVarInt(isVariableIndex)
		schema.serialize(stream, entity.isVariable)
		
		stream.writeUnsignedVarInt(upperBoundIndex)
		schema.serialize(stream, entity.upperBound)
		
		val initialValue = entity.initialValue
		if (initialValue != null)
		{
			stream.writeUnsignedVarInt(initialValueIndex)
			schema.serialize(stream, initialValue)
		}
		
		if (entity.annotations.isNotEmpty())
		{
			stream.writeUnsignedVarInt(annotationsIndex)
			entity.annotations.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstLocalVariableDeclaration
	{
		val builder = AstLocalVariableDeclarationBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				nameIndex -> builder.name = schema.deserialize(stream)
				isVariableIndex -> builder.isVariable = schema.deserialize(stream)
				upperBoundIndex -> builder.upperBound = schema.deserialize(stream)
				initialValueIndex -> builder.initialValue = schema.deserialize(stream)
				annotationsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.annotations.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
