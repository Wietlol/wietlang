package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstAnnotationBuilder
{
	var name: String? = null
	var imports: MutableCollection<AstMutableImport> = mutableListOf()
	var members: MutableList<AstMutableAnnotationMember> = mutableListOf()
	var annotations: MutableList<AstMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstAnnotation =
		AstAnnotation.of(
			name!!,
			imports.toList(),
			members.toList(),
			annotations.toList(),
			tokens.toList()
		)
}
