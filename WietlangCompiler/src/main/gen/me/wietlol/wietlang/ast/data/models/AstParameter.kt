package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstParameterSerializer

interface AstParameter : BitSerializable, AstVariable, Jsonable
{
	override val serializationKey: UUID
		get() = AstParameterSerializer.modelId
	
	override val type: String
		get() = "parameter"
	override val name: String
	override val upperBound: AstTypeReference
	override val annotations: List<AstAnnotationExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstParameter) return false
		
		if (type != other.type) return false
		if (name != other.name) return false
		if (upperBound != other.upperBound) return false
		if (annotations != other.annotations) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(name)
			.with(upperBound)
			.with(annotations)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"name":${name.toJson()},"upperBound":${upperBound.toJson()},"annotations":${annotations.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(name: String, upperBound: AstTypeReference, annotations: List<AstAnnotationExpression>, tokens: List<AstCodeToken>): AstParameter =
			object : AstParameter
			{
				override val name: String = name
				override val upperBound: AstTypeReference = upperBound
				override val annotations: List<AstAnnotationExpression> = annotations
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
