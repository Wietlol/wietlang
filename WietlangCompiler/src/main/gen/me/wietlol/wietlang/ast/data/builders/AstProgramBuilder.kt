package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstProgramBuilder
{
	var rootPackage: AstMutablePackage? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstProgram =
		AstProgram.of(
			rootPackage!!,
			tokens.toList()
		)
}
