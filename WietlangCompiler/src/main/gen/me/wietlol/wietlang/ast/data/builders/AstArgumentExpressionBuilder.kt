package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstArgumentExpressionBuilder
{
	var name: String? = null
	var expression: AstMutableExpression? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstArgumentExpression =
		AstArgumentExpression.of(
			name,
			expression!!,
			tokens.toList()
		)
}
