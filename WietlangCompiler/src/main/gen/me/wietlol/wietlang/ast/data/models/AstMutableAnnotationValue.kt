package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableAnnotationValue : AstAnnotationValue, AstMutableAnnotationMember, AstMutableNamedComponent
{
	override var name: String
	override var upperBound: AstMutableTypeReference
	override val annotations: MutableList<AstMutableAnnotationExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String, upperBound: AstMutableTypeReference, annotations: MutableList<AstMutableAnnotationExpression>, tokens: MutableList<AstCodeToken>): AstMutableAnnotationValue =
			object : AstMutableAnnotationValue
			{
				override var name: String = name
				override var upperBound: AstMutableTypeReference = upperBound
				override val annotations: MutableList<AstMutableAnnotationExpression> = annotations
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
