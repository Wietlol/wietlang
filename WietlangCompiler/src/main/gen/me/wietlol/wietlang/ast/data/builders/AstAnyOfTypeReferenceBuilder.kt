package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstAnyOfTypeReferenceBuilder
{
	var types: MutableList<AstMutableTypeReference> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstAnyOfTypeReference =
		AstAnyOfTypeReference.of(
			types.toList(),
			tokens.toList()
		)
}
