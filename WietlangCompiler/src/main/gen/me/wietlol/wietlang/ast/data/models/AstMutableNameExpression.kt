package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableNameExpression : AstNameExpression, AstMutableExpression
{
	override var path: AstMutableNamePath
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(path: AstMutableNamePath, tokens: MutableList<AstCodeToken>): AstMutableNameExpression =
			object : AstMutableNameExpression
			{
				override var path: AstMutableNamePath = path
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
