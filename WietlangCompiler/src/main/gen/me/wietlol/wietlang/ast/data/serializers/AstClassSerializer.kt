package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstClassSerializer : ModelSerializer<AstClass, AstClass>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstClass|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val nameIndex = 1
	private const val importsIndex = 2
	private const val membersIndex = 3
	private const val annotationsIndex = 4
	private const val supertypesIndex = 5
	private const val tokensIndex = 6
	
	override val modelId: UUID
		get() = UUID.fromString("b78d5876-c1e7-455f-b6a4-06cd8e277e02")
	override val dataClass: Class<AstClass>
		get() = AstClass::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstClass)
	{
		stream.writeUnsignedVarInt(nameIndex)
		schema.serialize(stream, entity.name)
		
		if (entity.imports.isNotEmpty())
		{
			stream.writeUnsignedVarInt(importsIndex)
			entity.imports.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.members.isNotEmpty())
		{
			stream.writeUnsignedVarInt(membersIndex)
			entity.members.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.annotations.isNotEmpty())
		{
			stream.writeUnsignedVarInt(annotationsIndex)
			entity.annotations.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.supertypes.isNotEmpty())
		{
			stream.writeUnsignedVarInt(supertypesIndex)
			entity.supertypes.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstClass
	{
		val builder = AstClassBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				nameIndex -> builder.name = schema.deserialize(stream)
				importsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.imports.add(schema.deserialize(stream, key))
					}
				}
				membersIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.members.add(schema.deserialize(stream, key))
					}
				}
				annotationsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.annotations.add(schema.deserialize(stream, key))
					}
				}
				supertypesIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.supertypes.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
