package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstClassSerializer

interface AstClass : BitSerializable, AstFirstClassComponent, Jsonable
{
	override val serializationKey: UUID
		get() = AstClassSerializer.modelId
	
	override val type: String
		get() = "class"
	override val name: String
	override val imports: Collection<AstImport>
	val members: List<AstClassMember>
	override val annotations: List<AstAnnotationExpression>
	val supertypes: List<AstTypeReference>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstClass) return false
		
		if (type != other.type) return false
		if (name != other.name) return false
		if (imports != other.imports) return false
		if (members != other.members) return false
		if (annotations != other.annotations) return false
		if (supertypes != other.supertypes) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(name)
			.with(imports)
			.with(members)
			.with(annotations)
			.with(supertypes)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"name":${name.toJson()},"imports":${imports.toJson()},"members":${members.toJson()},"annotations":${annotations.toJson()},"supertypes":${supertypes.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(name: String, imports: Collection<AstImport>, members: List<AstClassMember>, annotations: List<AstAnnotationExpression>, supertypes: List<AstTypeReference>, tokens: List<AstCodeToken>): AstClass =
			object : AstClass
			{
				override val name: String = name
				override val imports: Collection<AstImport> = imports
				override val members: List<AstClassMember> = members
				override val annotations: List<AstAnnotationExpression> = annotations
				override val supertypes: List<AstTypeReference> = supertypes
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
