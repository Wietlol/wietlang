package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableMethodCallExpression : AstMethodCallExpression, AstMutableExpression
{
	override var supplierExpression: AstMutableExpression
	override var functionName: String
	override val arguments: MutableList<AstMutableArgumentExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(supplierExpression: AstMutableExpression, functionName: String, arguments: MutableList<AstMutableArgumentExpression>, tokens: MutableList<AstCodeToken>): AstMutableMethodCallExpression =
			object : AstMutableMethodCallExpression
			{
				override var supplierExpression: AstMutableExpression = supplierExpression
				override var functionName: String = functionName
				override val arguments: MutableList<AstMutableArgumentExpression> = arguments
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
