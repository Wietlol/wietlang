package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableAnnotation : AstAnnotation, AstMutableFirstClassComponent
{
	override var name: String
	override val imports: MutableCollection<AstMutableImport>
	override val members: MutableList<AstMutableAnnotationMember>
	override val annotations: MutableList<AstMutableAnnotationExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String, imports: MutableCollection<AstMutableImport>, members: MutableList<AstMutableAnnotationMember>, annotations: MutableList<AstMutableAnnotationExpression>, tokens: MutableList<AstCodeToken>): AstMutableAnnotation =
			object : AstMutableAnnotation
			{
				override var name: String = name
				override val imports: MutableCollection<AstMutableImport> = imports
				override val members: MutableList<AstMutableAnnotationMember> = members
				override val annotations: MutableList<AstMutableAnnotationExpression> = annotations
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
