package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstParameterBuilder
{
	var name: String? = null
	var upperBound: AstMutableTypeReference? = null
	var annotations: MutableList<AstMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstParameter =
		AstParameter.of(
			name!!,
			upperBound!!,
			annotations.toList(),
			tokens.toList()
		)
}
