package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstReturnExpressionSerializer

interface AstReturnExpression : BitSerializable, AstExpression, Jsonable
{
	override val serializationKey: UUID
		get() = AstReturnExpressionSerializer.modelId
	
	override val type: String
		get() = "returnExpression"
	val valueExpression: AstExpression?
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstReturnExpression) return false
		
		if (type != other.type) return false
		if (valueExpression != other.valueExpression) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(valueExpression)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"valueExpression":${valueExpression.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(valueExpression: AstExpression?, tokens: List<AstCodeToken>): AstReturnExpression =
			object : AstReturnExpression
			{
				override val valueExpression: AstExpression? = valueExpression
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
