package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableNamePath : AstNamePath, AstNode
{
	override val parts: MutableList<String>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(parts: MutableList<String>, tokens: MutableList<AstCodeToken>): AstMutableNamePath =
			object : AstMutableNamePath
			{
				override val parts: MutableList<String> = parts
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
