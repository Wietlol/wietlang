package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableClassMethod : AstClassMethod, AstMutableClassMember, AstMutableNamedComponent
{
	override var name: String
	override var returnType: AstMutableTypeReference?
	override val body: MutableList<AstMutableExpression>?
	override val parameters: MutableList<AstMutableParameter>
	override val annotations: MutableList<AstMutableAnnotationExpression>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String, returnType: AstMutableTypeReference?, body: MutableList<AstMutableExpression>?, parameters: MutableList<AstMutableParameter>, annotations: MutableList<AstMutableAnnotationExpression>, tokens: MutableList<AstCodeToken>): AstMutableClassMethod =
			object : AstMutableClassMethod
			{
				override var name: String = name
				override var returnType: AstMutableTypeReference? = returnType
				override val body: MutableList<AstMutableExpression>? = body
				override val parameters: MutableList<AstMutableParameter> = parameters
				override val annotations: MutableList<AstMutableAnnotationExpression> = annotations
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
