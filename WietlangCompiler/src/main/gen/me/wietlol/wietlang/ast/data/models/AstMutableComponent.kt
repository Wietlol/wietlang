package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableComponent : AstComponent, AstNode
{
}
