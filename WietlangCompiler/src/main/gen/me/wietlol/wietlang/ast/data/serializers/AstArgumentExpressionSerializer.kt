package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstArgumentExpressionSerializer : ModelSerializer<AstArgumentExpression, AstArgumentExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstArgumentExpression|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val nameIndex = 1
	private const val expressionIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("2263b2b4-5999-4dc7-92b8-14a067fd0a79")
	override val dataClass: Class<AstArgumentExpression>
		get() = AstArgumentExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstArgumentExpression)
	{
		val name = entity.name
		if (name != null)
		{
			stream.writeUnsignedVarInt(nameIndex)
			schema.serialize(stream, name)
		}
		
		stream.writeUnsignedVarInt(expressionIndex)
		schema.serialize(stream, entity.expression)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstArgumentExpression
	{
		val builder = AstArgumentExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				nameIndex -> builder.name = schema.deserialize(stream)
				expressionIndex -> builder.expression = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
