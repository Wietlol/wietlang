package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.Jsonable
import java.util.*

interface AstNode : BitSerializable, Jsonable
{
	val type: String
	val tokens: List<AstCodeToken>
}
