package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableArgumentExpression : AstArgumentExpression, AstNode
{
	override var name: String?
	override var expression: AstMutableExpression
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(name: String?, expression: AstMutableExpression, tokens: MutableList<AstCodeToken>): AstMutableArgumentExpression =
			object : AstMutableArgumentExpression
			{
				override var name: String? = name
				override var expression: AstMutableExpression = expression
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
