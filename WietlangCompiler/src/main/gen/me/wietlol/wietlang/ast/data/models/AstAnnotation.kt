package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstAnnotationSerializer

interface AstAnnotation : BitSerializable, AstFirstClassComponent, Jsonable
{
	override val serializationKey: UUID
		get() = AstAnnotationSerializer.modelId
	
	override val type: String
		get() = "annotation"
	override val name: String
	override val imports: Collection<AstImport>
	val members: List<AstAnnotationMember>
	override val annotations: List<AstAnnotationExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstAnnotation) return false
		
		if (type != other.type) return false
		if (name != other.name) return false
		if (imports != other.imports) return false
		if (members != other.members) return false
		if (annotations != other.annotations) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(name)
			.with(imports)
			.with(members)
			.with(annotations)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"name":${name.toJson()},"imports":${imports.toJson()},"members":${members.toJson()},"annotations":${annotations.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(name: String, imports: Collection<AstImport>, members: List<AstAnnotationMember>, annotations: List<AstAnnotationExpression>, tokens: List<AstCodeToken>): AstAnnotation =
			object : AstAnnotation
			{
				override val name: String = name
				override val imports: Collection<AstImport> = imports
				override val members: List<AstAnnotationMember> = members
				override val annotations: List<AstAnnotationExpression> = annotations
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
