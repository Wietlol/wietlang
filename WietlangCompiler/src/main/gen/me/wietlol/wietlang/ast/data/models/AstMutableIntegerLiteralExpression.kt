package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableIntegerLiteralExpression : AstIntegerLiteralExpression, AstMutableNumberLiteralExpression
{
	override var value: Int
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(value: Int, tokens: MutableList<AstCodeToken>): AstMutableIntegerLiteralExpression =
			object : AstMutableIntegerLiteralExpression
			{
				override var value: Int = value
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
