package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstReturnExpressionBuilder
{
	var valueExpression: AstMutableExpression? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstReturnExpression =
		AstReturnExpression.of(
			valueExpression,
			tokens.toList()
		)
}
