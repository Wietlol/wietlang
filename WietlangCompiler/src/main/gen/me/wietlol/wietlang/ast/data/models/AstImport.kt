package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstImportSerializer

interface AstImport : BitSerializable, AstNode, Jsonable
{
	override val serializationKey: UUID
		get() = AstImportSerializer.modelId
	
	override val type: String
		get() = "import"
	val path: AstNamePath
	val alias: String?
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstImport) return false
		
		if (type != other.type) return false
		if (path != other.path) return false
		if (alias != other.alias) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(path)
			.with(alias)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"path":${path.toJson()},"alias":${alias.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(path: AstNamePath, alias: String?, tokens: List<AstCodeToken>): AstImport =
			object : AstImport
			{
				override val path: AstNamePath = path
				override val alias: String? = alias
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
