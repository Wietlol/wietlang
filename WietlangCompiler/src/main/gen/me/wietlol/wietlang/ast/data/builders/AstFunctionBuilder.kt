package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstFunctionBuilder
{
	var name: String? = null
	var imports: MutableCollection<AstMutableImport> = mutableListOf()
	var returnType: AstMutableTypeReference? = null
	var body: MutableList<AstMutableExpression>? = null
	var parameters: MutableList<AstMutableParameter> = mutableListOf()
	var annotations: MutableList<AstMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstFunction =
		AstFunction.of(
			name!!,
			imports.toList(),
			returnType,
			body?.toList(),
			parameters.toList(),
			annotations.toList(),
			tokens.toList()
		)
}
