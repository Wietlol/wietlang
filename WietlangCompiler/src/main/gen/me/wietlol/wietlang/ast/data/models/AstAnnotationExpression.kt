package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstAnnotationExpressionSerializer

interface AstAnnotationExpression : BitSerializable, AstNode, Jsonable
{
	override val serializationKey: UUID
		get() = AstAnnotationExpressionSerializer.modelId
	
	override val type: String
		get() = "annotationExpression"
	val namePath: AstNamePath
	val arguments: List<AstArgumentExpression>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstAnnotationExpression) return false
		
		if (type != other.type) return false
		if (namePath != other.namePath) return false
		if (arguments != other.arguments) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(namePath)
			.with(arguments)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"namePath":${namePath.toJson()},"arguments":${arguments.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(namePath: AstNamePath, arguments: List<AstArgumentExpression>, tokens: List<AstCodeToken>): AstAnnotationExpression =
			object : AstAnnotationExpression
			{
				override val namePath: AstNamePath = namePath
				override val arguments: List<AstArgumentExpression> = arguments
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
