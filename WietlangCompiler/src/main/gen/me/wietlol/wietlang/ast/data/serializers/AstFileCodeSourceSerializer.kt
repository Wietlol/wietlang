package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstFileCodeSourceSerializer : ModelSerializer<AstFileCodeSource, AstFileCodeSource>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstFileCodeSource|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val filePathIndex = 1
	
	override val modelId: UUID
		get() = UUID.fromString("df09acbb-e242-4320-bfab-c504f758b9c5")
	override val dataClass: Class<AstFileCodeSource>
		get() = AstFileCodeSource::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstFileCodeSource)
	{
		stream.writeUnsignedVarInt(filePathIndex)
		schema.serialize(stream, entity.filePath)
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstFileCodeSource
	{
		val builder = AstFileCodeSourceBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				filePathIndex -> builder.filePath = schema.deserialize(stream)
			}
		}
	}
}
