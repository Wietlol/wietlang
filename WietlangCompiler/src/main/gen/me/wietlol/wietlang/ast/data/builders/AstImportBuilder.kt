package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstImportBuilder
{
	var path: AstMutableNamePath? = null
	var alias: String? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstImport =
		AstImport.of(
			path!!,
			alias,
			tokens.toList()
		)
}
