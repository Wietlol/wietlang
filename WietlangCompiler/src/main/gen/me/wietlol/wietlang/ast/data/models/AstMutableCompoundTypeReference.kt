package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableCompoundTypeReference : AstCompoundTypeReference, AstMutableTypeReference
{
	override val types: MutableList<AstMutableTypeReference>
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(types: MutableList<AstMutableTypeReference>, tokens: MutableList<AstCodeToken>): AstMutableCompoundTypeReference =
			object : AstMutableCompoundTypeReference
			{
				override val types: MutableList<AstMutableTypeReference> = types
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
