package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstCompoundTypeReferenceBuilder
{
	var types: MutableList<AstMutableTypeReference> = mutableListOf()
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstCompoundTypeReference =
		AstCompoundTypeReference.of(
			types.toList(),
			tokens.toList()
		)
}
