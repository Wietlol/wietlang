package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableImport : AstImport, AstNode
{
	override var path: AstMutableNamePath
	override var alias: String?
	override val tokens: MutableList<AstCodeToken>
	
	companion object
	{
		fun of(path: AstMutableNamePath, alias: String?, tokens: MutableList<AstCodeToken>): AstMutableImport =
			object : AstMutableImport
			{
				override var path: AstMutableNamePath = path
				override var alias: String? = alias
				override val tokens: MutableList<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
