package me.wietlol.wietlang.ast.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.ast.data.models.*
import me.wietlol.wietlang.ast.data.builders.*

object AstPackageSerializer : ModelSerializer<AstPackage, AstPackage>
{
	init
	{
		val registryKey = CommonModelRegistryKey("AstPackage|WietlangAst|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val nameIndex = 1
	private const val importsIndex = 2
	private const val membersIndex = 3
	private const val annotationsIndex = 4
	private const val tokensIndex = 8
	
	override val modelId: UUID
		get() = UUID.fromString("a49d1b90-102f-4138-a8d7-13d16da05a96")
	override val dataClass: Class<AstPackage>
		get() = AstPackage::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: AstPackage)
	{
		stream.writeUnsignedVarInt(nameIndex)
		schema.serialize(stream, entity.name)
		
		if (entity.imports.isNotEmpty())
		{
			stream.writeUnsignedVarInt(importsIndex)
			entity.imports.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.members.isNotEmpty())
		{
			stream.writeUnsignedVarInt(membersIndex)
			entity.members.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.annotations.isNotEmpty())
		{
			stream.writeUnsignedVarInt(annotationsIndex)
			entity.annotations.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): AstPackage
	{
		val builder = AstPackageBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				nameIndex -> builder.name = schema.deserialize(stream)
				importsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.imports.add(schema.deserialize(stream, key))
					}
				}
				membersIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.members.add(schema.deserialize(stream, key))
					}
				}
				annotationsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.annotations.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
