package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstArgumentExpressionSerializer

interface AstArgumentExpression : BitSerializable, AstNode, Jsonable
{
	override val serializationKey: UUID
		get() = AstArgumentExpressionSerializer.modelId
	
	override val type: String
		get() = "argumentExpression"
	val name: String?
	val expression: AstExpression
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstArgumentExpression) return false
		
		if (type != other.type) return false
		if (name != other.name) return false
		if (expression != other.expression) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(name)
			.with(expression)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"name":${name.toJson()},"expression":${expression.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(name: String?, expression: AstExpression, tokens: List<AstCodeToken>): AstArgumentExpression =
			object : AstArgumentExpression
			{
				override val name: String? = name
				override val expression: AstExpression = expression
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
