package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstAnyOfTypeReferenceSerializer

interface AstAnyOfTypeReference : BitSerializable, AstTypeReference, Jsonable
{
	override val serializationKey: UUID
		get() = AstAnyOfTypeReferenceSerializer.modelId
	
	override val type: String
		get() = "anyOfTypeReference"
	val types: List<AstTypeReference>
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstAnyOfTypeReference) return false
		
		if (type != other.type) return false
		if (types != other.types) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(types)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"types":${types.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(types: List<AstTypeReference>, tokens: List<AstCodeToken>): AstAnyOfTypeReference =
			object : AstAnyOfTypeReference
			{
				override val types: List<AstTypeReference> = types
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
