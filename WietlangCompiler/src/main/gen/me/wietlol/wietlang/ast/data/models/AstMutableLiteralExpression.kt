package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableLiteralExpression : AstLiteralExpression, AstMutableExpression
{
}
