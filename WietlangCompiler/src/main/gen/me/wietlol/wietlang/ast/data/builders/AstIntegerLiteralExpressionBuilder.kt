package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstIntegerLiteralExpressionBuilder
{
	var value: Int? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstIntegerLiteralExpression =
		AstIntegerLiteralExpression.of(
			value!!,
			tokens.toList()
		)
}
