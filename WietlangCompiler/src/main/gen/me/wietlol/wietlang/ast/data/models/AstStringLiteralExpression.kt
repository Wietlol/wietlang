package me.wietlol.wietlang.ast.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.ast.data.serializers.AstStringLiteralExpressionSerializer

interface AstStringLiteralExpression : BitSerializable, AstLiteralExpression, Jsonable
{
	override val serializationKey: UUID
		get() = AstStringLiteralExpressionSerializer.modelId
	
	override val type: String
		get() = "stringLiteralExpression"
	override val value: String
	override val tokens: List<AstCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is AstStringLiteralExpression) return false
		
		if (type != other.type) return false
		if (value != other.value) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(value)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"value":${value.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(value: String, tokens: List<AstCodeToken>): AstStringLiteralExpression =
			object : AstStringLiteralExpression
			{
				override val value: String = value
				override val tokens: List<AstCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
