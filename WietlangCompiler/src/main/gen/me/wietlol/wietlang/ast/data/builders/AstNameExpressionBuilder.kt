package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstNameExpressionBuilder
{
	var path: AstMutableNamePath? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstNameExpression =
		AstNameExpression.of(
			path!!,
			tokens.toList()
		)
}
