package me.wietlol.wietlang.ast.data.builders

import me.wietlol.wietlang.ast.data.models.*
import java.util.*

class AstStringLiteralExpressionBuilder
{
	var value: String? = null
	var tokens: MutableList<AstCodeToken> = mutableListOf()
	
	fun build(): AstStringLiteralExpression =
		AstStringLiteralExpression.of(
			value!!,
			tokens.toList()
		)
}
