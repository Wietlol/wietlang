package me.wietlol.wietlang.ast.data.models

import java.util.*

interface AstMutableFirstClassComponent : AstFirstClassComponent, AstMutableNamedComponent
{
}
