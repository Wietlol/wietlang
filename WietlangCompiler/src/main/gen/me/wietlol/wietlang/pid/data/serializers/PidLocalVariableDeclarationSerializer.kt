package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidLocalVariableDeclarationSerializer : ModelSerializer<PidLocalVariableDeclaration, PidLocalVariableDeclaration>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidLocalVariableDeclaration|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val idIndex = 1
	private const val annotationsIndex = 2
	private const val nameIndex = 3
	private const val isVariableIndex = 4
	private const val upperBoundIndex = 5
	private const val initialValueIndex = 6
	private const val tokensIndex = 7
	
	override val modelId: UUID
		get() = UUID.fromString("789358e4-376f-4727-b8d6-9525053aed23")
	override val dataClass: Class<PidLocalVariableDeclaration>
		get() = PidLocalVariableDeclaration::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidLocalVariableDeclaration)
	{
		stream.writeUnsignedVarInt(idIndex)
		schema.serialize(stream, entity.id)
		
		if (entity.annotations.isNotEmpty())
		{
			stream.writeUnsignedVarInt(annotationsIndex)
			entity.annotations.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(nameIndex)
		schema.serialize(stream, entity.name)
		
		stream.writeUnsignedVarInt(isVariableIndex)
		schema.serialize(stream, entity.isVariable)
		
		stream.writeUnsignedVarInt(upperBoundIndex)
		schema.serialize(stream, entity.upperBound)
		
		val initialValue = entity.initialValue
		if (initialValue != null)
		{
			stream.writeUnsignedVarInt(initialValueIndex)
			schema.serialize(stream, initialValue)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidLocalVariableDeclaration
	{
		val builder = PidLocalVariableDeclarationBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				idIndex -> builder.id = schema.deserialize(stream)
				annotationsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.annotations.add(schema.deserialize(stream, key))
					}
				}
				nameIndex -> builder.name = schema.deserialize(stream)
				isVariableIndex -> builder.isVariable = schema.deserialize(stream)
				upperBoundIndex -> builder.upperBound = schema.deserialize(stream)
				initialValueIndex -> builder.initialValue = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
