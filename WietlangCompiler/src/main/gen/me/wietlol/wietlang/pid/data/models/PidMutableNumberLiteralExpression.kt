package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableNumberLiteralExpression : PidNumberLiteralExpression, PidMutableLiteralExpression
{
}
