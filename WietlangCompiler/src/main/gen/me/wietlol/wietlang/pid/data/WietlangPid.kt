package me.wietlol.wietlang.pid.data

import me.wietlol.wietlang.pid.data.serializers.*

object WietlangPid
{
	init
	{
		initialize()
	}
	
	fun init()
	{
		// loaded by classloader
	}
	
	private fun initialize()
	{
		Class.forName(PidCodeTokenSerializer::class.java.name)
		Class.forName(PidFileCodeSourceSerializer::class.java.name)
		Class.forName(PidNamePathSerializer::class.java.name)
		Class.forName(PidImportSerializer::class.java.name)
		Class.forName(PidProgramSerializer::class.java.name)
		Class.forName(PidAnnotationSerializer::class.java.name)
		Class.forName(PidClassMethodSerializer::class.java.name)
		Class.forName(PidAnnotationValueSerializer::class.java.name)
		Class.forName(PidClassSerializer::class.java.name)
		Class.forName(PidFunctionSerializer::class.java.name)
		Class.forName(PidPackageSerializer::class.java.name)
		Class.forName(PidAnnotationExpressionSerializer::class.java.name)
		Class.forName(PidDirectTypeReferenceSerializer::class.java.name)
		Class.forName(PidCompoundTypeReferenceSerializer::class.java.name)
		Class.forName(PidAnyOfTypeReferenceSerializer::class.java.name)
		Class.forName(PidAllOfTypeReferenceSerializer::class.java.name)
		Class.forName(PidParameterSerializer::class.java.name)
		Class.forName(PidArgumentExpressionSerializer::class.java.name)
		Class.forName(PidFunctionCallExpressionSerializer::class.java.name)
		Class.forName(PidMethodCallExpressionSerializer::class.java.name)
		Class.forName(PidVariableLoadExpressionSerializer::class.java.name)
		Class.forName(PidReturnExpressionSerializer::class.java.name)
		Class.forName(PidIntegerLiteralExpressionSerializer::class.java.name)
		Class.forName(PidStringLiteralExpressionSerializer::class.java.name)
		Class.forName(PidLocalVariableDeclarationSerializer::class.java.name)
	}
}
