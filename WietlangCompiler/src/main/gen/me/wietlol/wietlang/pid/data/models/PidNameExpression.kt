package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidNameExpressionSerializer

interface PidNameExpression : BitSerializable, PidExpression, Jsonable
{
	override val serializationKey: UUID
		get() = PidNameExpressionSerializer.modelId
	
	override val type: String
		get() = "nameExpression"
	val variable: PidVariable
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidNameExpression) return false
		
		if (type != other.type) return false
		if (variable != other.variable) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(variable)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"variable":${variable.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(variable: PidVariable, tokens: List<PidCodeToken>): PidNameExpression =
			object : PidNameExpression
			{
				override val variable: PidVariable = variable
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
