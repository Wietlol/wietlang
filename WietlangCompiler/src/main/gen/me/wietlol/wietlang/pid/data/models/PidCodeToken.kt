package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidCodeTokenSerializer

interface PidCodeToken : BitSerializable, Jsonable
{
	override val serializationKey: UUID
		get() = PidCodeTokenSerializer.modelId
	
	val text: String
	val type: String
	val line: Int
	val column: Int
	val source: PidCodeSource
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidCodeToken) return false
		
		if (text != other.text) return false
		if (type != other.type) return false
		if (line != other.line) return false
		if (column != other.column) return false
		if (source != other.source) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(text)
			.with(type)
			.with(line)
			.with(column)
			.with(source)
	
	override fun toJson(): String =
		"""{"text":${text.toJson()},"type":${type.toJson()},"line":${line.toJson()},"column":${column.toJson()},"source":${source.toJson()}}"""
	
	companion object
	{
		fun of(text: String, type: String, line: Int, column: Int, source: PidCodeSource): PidCodeToken =
			object : PidCodeToken
			{
				override val text: String = text
				override val type: String = type
				override val line: Int = line
				override val column: Int = column
				override val source: PidCodeSource = source
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
