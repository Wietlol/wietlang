package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidProgramSerializer

interface PidProgram : BitSerializable, PidNode, Jsonable
{
	override val serializationKey: UUID
		get() = PidProgramSerializer.modelId
	
	override val type: String
		get() = "program"
	val rootPackage: PidPackage
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidProgram) return false
		
		if (type != other.type) return false
		if (rootPackage != other.rootPackage) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(rootPackage)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"rootPackage":${rootPackage.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(rootPackage: PidPackage, tokens: List<PidCodeToken>): PidProgram =
			object : PidProgram
			{
				override val rootPackage: PidPackage = rootPackage
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
