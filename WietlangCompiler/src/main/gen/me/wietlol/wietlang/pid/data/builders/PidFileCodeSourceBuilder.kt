package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidFileCodeSourceBuilder
{
	var filePath: String? = null
	
	fun build(): PidFileCodeSource =
		PidFileCodeSource.of(
			filePath!!
		)
}
