package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidIntegerLiteralExpressionBuilder
{
	var value: Int? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidIntegerLiteralExpression =
		PidIntegerLiteralExpression.of(
			value!!,
			tokens.toList()
		)
}
