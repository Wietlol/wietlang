package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableVariable : PidVariable, PidMutableNamedComponent
{
}
