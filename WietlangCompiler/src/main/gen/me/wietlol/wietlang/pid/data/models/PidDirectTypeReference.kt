package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidDirectTypeReferenceSerializer

interface PidDirectTypeReference : BitSerializable, PidTypeReference, Jsonable
{
	override val serializationKey: UUID
		get() = PidDirectTypeReferenceSerializer.modelId
	
	override val type: String
		get() = "directTypeReference"
	val referencedTypeId: UUID
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidDirectTypeReference) return false
		
		if (type != other.type) return false
		if (referencedTypeId != other.referencedTypeId) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(referencedTypeId)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"referencedTypeId":${referencedTypeId.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(referencedTypeId: UUID, tokens: List<PidCodeToken>): PidDirectTypeReference =
			object : PidDirectTypeReference
			{
				override val referencedTypeId: UUID = referencedTypeId
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
