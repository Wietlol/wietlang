package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableAnyOfTypeReference : PidAnyOfTypeReference, PidMutableTypeReference
{
	override val types: MutableList<PidMutableTypeReference>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(types: MutableList<PidMutableTypeReference>, tokens: MutableList<PidCodeToken>): PidMutableAnyOfTypeReference =
			object : PidMutableAnyOfTypeReference
			{
				override val types: MutableList<PidMutableTypeReference> = types
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
