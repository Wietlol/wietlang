package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableNameExpression : PidNameExpression, PidMutableExpression
{
	override var variable: PidMutableVariable
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(variable: PidMutableVariable, tokens: MutableList<PidCodeToken>): PidMutableNameExpression =
			object : PidMutableNameExpression
			{
				override var variable: PidMutableVariable = variable
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
