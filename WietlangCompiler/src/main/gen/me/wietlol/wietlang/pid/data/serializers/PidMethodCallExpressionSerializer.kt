package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidMethodCallExpressionSerializer : ModelSerializer<PidMethodCallExpression, PidMethodCallExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidMethodCallExpression|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val supplierExpressionIndex = 1
	private const val argumentsIndex = 2
	private const val methodIdIndex = 3
	private const val tokensIndex = 4
	
	override val modelId: UUID
		get() = UUID.fromString("48eb5657-6035-4b85-be25-e746ca066b01")
	override val dataClass: Class<PidMethodCallExpression>
		get() = PidMethodCallExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidMethodCallExpression)
	{
		stream.writeUnsignedVarInt(supplierExpressionIndex)
		schema.serialize(stream, entity.supplierExpression)
		
		if (entity.arguments.isNotEmpty())
		{
			stream.writeUnsignedVarInt(argumentsIndex)
			entity.arguments.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(methodIdIndex)
		schema.serialize(stream, entity.methodId)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidMethodCallExpression
	{
		val builder = PidMethodCallExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				supplierExpressionIndex -> builder.supplierExpression = schema.deserialize(stream)
				argumentsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.arguments.add(schema.deserialize(stream, key))
					}
				}
				methodIdIndex -> builder.methodId = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
