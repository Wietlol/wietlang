package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidCodeTokenBuilder
{
	var text: String? = null
	var type: String? = null
	var line: Int? = null
	var column: Int? = null
	var source: PidCodeSource? = null
	
	fun build(): PidCodeToken =
		PidCodeToken.of(
			text!!,
			type!!,
			line!!,
			column!!,
			source!!
		)
}
