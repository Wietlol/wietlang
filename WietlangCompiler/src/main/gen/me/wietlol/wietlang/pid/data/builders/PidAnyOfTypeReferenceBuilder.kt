package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidAnyOfTypeReferenceBuilder
{
	var types: MutableList<PidMutableTypeReference> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidAnyOfTypeReference =
		PidAnyOfTypeReference.of(
			types.toList(),
			tokens.toList()
		)
}
