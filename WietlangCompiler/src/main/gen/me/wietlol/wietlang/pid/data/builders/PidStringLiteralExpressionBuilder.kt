package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidStringLiteralExpressionBuilder
{
	var value: String? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidStringLiteralExpression =
		PidStringLiteralExpression.of(
			value!!,
			tokens.toList()
		)
}
