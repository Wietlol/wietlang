package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidFileCodeSourceSerializer : ModelSerializer<PidFileCodeSource, PidFileCodeSource>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidFileCodeSource|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val filePathIndex = 1
	
	override val modelId: UUID
		get() = UUID.fromString("93fc128d-00b7-4bed-b633-074ca6e3fe15")
	override val dataClass: Class<PidFileCodeSource>
		get() = PidFileCodeSource::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidFileCodeSource)
	{
		stream.writeUnsignedVarInt(filePathIndex)
		schema.serialize(stream, entity.filePath)
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidFileCodeSource
	{
		val builder = PidFileCodeSourceBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				filePathIndex -> builder.filePath = schema.deserialize(stream)
			}
		}
	}
}
