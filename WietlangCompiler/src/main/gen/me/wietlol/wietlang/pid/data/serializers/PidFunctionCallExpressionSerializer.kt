package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidFunctionCallExpressionSerializer : ModelSerializer<PidFunctionCallExpression, PidFunctionCallExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidFunctionCallExpression|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val argumentsIndex = 1
	private const val functionIdIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("d829f80b-cab3-4808-b1c0-74e6edfec809")
	override val dataClass: Class<PidFunctionCallExpression>
		get() = PidFunctionCallExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidFunctionCallExpression)
	{
		if (entity.arguments.isNotEmpty())
		{
			stream.writeUnsignedVarInt(argumentsIndex)
			entity.arguments.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(functionIdIndex)
		schema.serialize(stream, entity.functionId)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidFunctionCallExpression
	{
		val builder = PidFunctionCallExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				argumentsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.arguments.add(schema.deserialize(stream, key))
					}
				}
				functionIdIndex -> builder.functionId = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
