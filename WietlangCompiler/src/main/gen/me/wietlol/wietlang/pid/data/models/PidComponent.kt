package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.Jsonable
import java.util.*

interface PidComponent : BitSerializable, PidNode, PidAnnotatable, Jsonable
{
	val id: UUID
}
