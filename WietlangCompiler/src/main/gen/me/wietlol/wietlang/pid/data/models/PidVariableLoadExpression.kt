package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidVariableLoadExpressionSerializer

interface PidVariableLoadExpression : BitSerializable, PidExpression, Jsonable
{
	override val serializationKey: UUID
		get() = PidVariableLoadExpressionSerializer.modelId
	
	override val type: String
		get() = "nameExpression"
	val variableId: UUID
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidVariableLoadExpression) return false
		
		if (type != other.type) return false
		if (variableId != other.variableId) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(variableId)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"variableId":${variableId.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(variableId: UUID, tokens: List<PidCodeToken>): PidVariableLoadExpression =
			object : PidVariableLoadExpression
			{
				override val variableId: UUID = variableId
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
