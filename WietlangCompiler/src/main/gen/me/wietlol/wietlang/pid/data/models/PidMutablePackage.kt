package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutablePackage : PidPackage, PidMutableFirstClassComponent, PidMutableContainer
{
	override var id: UUID
	override var name: String
	override val members: MutableCollection<PidNode>
	override val annotations: MutableList<PidMutableAnnotationExpression>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(id: UUID, name: String, members: MutableCollection<PidNode>, annotations: MutableList<PidMutableAnnotationExpression>, tokens: MutableList<PidCodeToken>): PidMutablePackage =
			object : PidMutablePackage
			{
				override var id: UUID = id
				override var name: String = name
				override val members: MutableCollection<PidNode> = members
				override val annotations: MutableList<PidMutableAnnotationExpression> = annotations
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
