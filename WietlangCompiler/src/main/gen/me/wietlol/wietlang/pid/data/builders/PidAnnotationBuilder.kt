package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidAnnotationBuilder
{
	var id: UUID? = null
	var name: String? = null
	var members: MutableList<PidMutableAnnotationMember> = mutableListOf()
	var annotations: MutableList<PidMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidAnnotation =
		PidAnnotation.of(
			id!!,
			name!!,
			members.toList(),
			annotations.toList(),
			tokens.toList()
		)
}
