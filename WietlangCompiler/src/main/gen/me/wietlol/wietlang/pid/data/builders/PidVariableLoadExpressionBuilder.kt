package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidVariableLoadExpressionBuilder
{
	var variableId: UUID? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidVariableLoadExpression =
		PidVariableLoadExpression.of(
			variableId!!,
			tokens.toList()
		)
}
