package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidParameterBuilder
{
	var id: UUID? = null
	var name: String? = null
	var upperBound: PidMutableTypeReference? = null
	var annotations: MutableList<PidMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidParameter =
		PidParameter.of(
			id!!,
			name!!,
			upperBound!!,
			annotations.toList(),
			tokens.toList()
		)
}
