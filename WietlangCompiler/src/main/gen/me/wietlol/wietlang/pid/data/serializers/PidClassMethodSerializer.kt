package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidClassMethodSerializer : ModelSerializer<PidClassMethod, PidClassMethod>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidClassMethod|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val idIndex = 1
	private const val nameIndex = 2
	private const val returnTypeIndex = 3
	private const val bodyIndex = 4
	private const val parametersIndex = 5
	private const val annotationsIndex = 6
	private const val tokensIndex = 7
	
	override val modelId: UUID
		get() = UUID.fromString("ceae1140-94b0-4876-a3be-db81cc126ed8")
	override val dataClass: Class<PidClassMethod>
		get() = PidClassMethod::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidClassMethod)
	{
		stream.writeUnsignedVarInt(idIndex)
		schema.serialize(stream, entity.id)
		
		stream.writeUnsignedVarInt(nameIndex)
		schema.serialize(stream, entity.name)
		
		stream.writeUnsignedVarInt(returnTypeIndex)
		schema.serialize(stream, entity.returnType)
		
		val body = entity.body
		if (body != null)
		{
			stream.writeUnsignedVarInt(bodyIndex)
			schema.serialize(stream, body)
		}
		
		if (entity.parameters.isNotEmpty())
		{
			stream.writeUnsignedVarInt(parametersIndex)
			entity.parameters.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.annotations.isNotEmpty())
		{
			stream.writeUnsignedVarInt(annotationsIndex)
			entity.annotations.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidClassMethod
	{
		val builder = PidClassMethodBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				idIndex -> builder.id = schema.deserialize(stream)
				nameIndex -> builder.name = schema.deserialize(stream)
				returnTypeIndex -> builder.returnType = schema.deserialize(stream)
				bodyIndex ->
				{
					builder.body = mutableListOf()
					
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.body!!.add(schema.deserialize(stream, key))
					}
				}
				parametersIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.parameters.add(schema.deserialize(stream, key))
					}
				}
				annotationsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.annotations.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
