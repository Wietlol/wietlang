package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.Jsonable
import java.util.*

interface PidAnnotatable : BitSerializable, Jsonable
{
	val annotations: List<PidAnnotationExpression>
}
