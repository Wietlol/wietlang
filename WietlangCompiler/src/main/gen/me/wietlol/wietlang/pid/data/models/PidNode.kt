package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.Jsonable
import java.util.*

interface PidNode : BitSerializable, Jsonable
{
	val type: String
	val tokens: List<PidCodeToken>
}
