package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableReturnExpression : PidReturnExpression, PidMutableExpression
{
	override var valueExpression: PidMutableExpression?
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(valueExpression: PidMutableExpression?, tokens: MutableList<PidCodeToken>): PidMutableReturnExpression =
			object : PidMutableReturnExpression
			{
				override var valueExpression: PidMutableExpression? = valueExpression
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
