package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidIntegerLiteralExpressionSerializer

interface PidIntegerLiteralExpression : BitSerializable, PidNumberLiteralExpression, Jsonable
{
	override val serializationKey: UUID
		get() = PidIntegerLiteralExpressionSerializer.modelId
	
	override val type: String
		get() = "integerLiteralExpression"
	override val value: Int
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidIntegerLiteralExpression) return false
		
		if (type != other.type) return false
		if (value != other.value) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(value)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"value":${value.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(value: Int, tokens: List<PidCodeToken>): PidIntegerLiteralExpression =
			object : PidIntegerLiteralExpression
			{
				override val value: Int = value
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
