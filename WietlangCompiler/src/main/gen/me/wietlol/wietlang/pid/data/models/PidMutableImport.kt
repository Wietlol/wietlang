package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableImport : PidImport, PidNode
{
	override var componentId: UUID
	override var alias: String?
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(componentId: UUID, alias: String?, tokens: MutableList<PidCodeToken>): PidMutableImport =
			object : PidMutableImport
			{
				override var componentId: UUID = componentId
				override var alias: String? = alias
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
