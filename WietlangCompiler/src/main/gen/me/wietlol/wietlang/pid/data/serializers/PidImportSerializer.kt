package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidImportSerializer : ModelSerializer<PidImport, PidImport>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidImport|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val componentIdIndex = 1
	private const val aliasIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("bce1a0eb-dcf7-4f1e-a31d-a39a667e3079")
	override val dataClass: Class<PidImport>
		get() = PidImport::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidImport)
	{
		stream.writeUnsignedVarInt(componentIdIndex)
		schema.serialize(stream, entity.componentId)
		
		val alias = entity.alias
		if (alias != null)
		{
			stream.writeUnsignedVarInt(aliasIndex)
			schema.serialize(stream, alias)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidImport
	{
		val builder = PidImportBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				componentIdIndex -> builder.componentId = schema.deserialize(stream)
				aliasIndex -> builder.alias = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
