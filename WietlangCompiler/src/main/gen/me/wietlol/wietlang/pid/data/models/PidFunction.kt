package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidFunctionSerializer

interface PidFunction : BitSerializable, PidFirstClassComponent, Jsonable
{
	override val serializationKey: UUID
		get() = PidFunctionSerializer.modelId
	
	override val type: String
		get() = "function"
	override val id: UUID
	override val name: String
	val returnType: PidTypeReference
	val body: List<PidExpression>?
	val parameters: List<PidParameter>
	override val annotations: List<PidAnnotationExpression>
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidFunction) return false
		
		if (type != other.type) return false
		if (id != other.id) return false
		if (name != other.name) return false
		if (returnType != other.returnType) return false
		if (body != other.body) return false
		if (parameters != other.parameters) return false
		if (annotations != other.annotations) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(id)
			.with(name)
			.with(returnType)
			.with(body)
			.with(parameters)
			.with(annotations)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"id":${id.toJson()},"name":${name.toJson()},"returnType":${returnType.toJson()},"body":${body.toJson()},"parameters":${parameters.toJson()},"annotations":${annotations.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(id: UUID, name: String, returnType: PidTypeReference, body: List<PidExpression>?, parameters: List<PidParameter>, annotations: List<PidAnnotationExpression>, tokens: List<PidCodeToken>): PidFunction =
			object : PidFunction
			{
				override val id: UUID = id
				override val name: String = name
				override val returnType: PidTypeReference = returnType
				override val body: List<PidExpression>? = body
				override val parameters: List<PidParameter> = parameters
				override val annotations: List<PidAnnotationExpression> = annotations
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
