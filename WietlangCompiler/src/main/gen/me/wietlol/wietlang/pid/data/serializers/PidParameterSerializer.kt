package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidParameterSerializer : ModelSerializer<PidParameter, PidParameter>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidParameter|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val idIndex = 1
	private const val nameIndex = 2
	private const val upperBoundIndex = 3
	private const val annotationsIndex = 4
	private const val tokensIndex = 5
	
	override val modelId: UUID
		get() = UUID.fromString("2a95656e-0165-4325-aa92-01eb88677ae4")
	override val dataClass: Class<PidParameter>
		get() = PidParameter::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidParameter)
	{
		stream.writeUnsignedVarInt(idIndex)
		schema.serialize(stream, entity.id)
		
		stream.writeUnsignedVarInt(nameIndex)
		schema.serialize(stream, entity.name)
		
		stream.writeUnsignedVarInt(upperBoundIndex)
		schema.serialize(stream, entity.upperBound)
		
		if (entity.annotations.isNotEmpty())
		{
			stream.writeUnsignedVarInt(annotationsIndex)
			entity.annotations.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidParameter
	{
		val builder = PidParameterBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				idIndex -> builder.id = schema.deserialize(stream)
				nameIndex -> builder.name = schema.deserialize(stream)
				upperBoundIndex -> builder.upperBound = schema.deserialize(stream)
				annotationsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.annotations.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
