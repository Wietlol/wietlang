package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidClassMethodBuilder
{
	var id: UUID? = null
	var name: String? = null
	var returnType: PidMutableTypeReference? = null
	var body: MutableList<PidMutableExpression>? = null
	var parameters: MutableList<PidMutableParameter> = mutableListOf()
	var annotations: MutableList<PidMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidClassMethod =
		PidClassMethod.of(
			id!!,
			name!!,
			returnType!!,
			body?.toList(),
			parameters.toList(),
			annotations.toList(),
			tokens.toList()
		)
}
