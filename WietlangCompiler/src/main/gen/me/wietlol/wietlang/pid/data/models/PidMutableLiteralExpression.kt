package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableLiteralExpression : PidLiteralExpression, PidMutableExpression
{
}
