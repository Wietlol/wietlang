package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.Jsonable
import java.util.*

interface PidNumberLiteralExpression : BitSerializable, PidLiteralExpression, Jsonable
{
	override val value: Number
}
