package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidFunctionCallExpressionBuilder
{
	var arguments: MutableList<PidMutableArgumentExpression> = mutableListOf()
	var functionId: UUID? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidFunctionCallExpression =
		PidFunctionCallExpression.of(
			arguments.toList(),
			functionId!!,
			tokens.toList()
		)
}
