package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidAnnotationExpressionBuilder
{
	var annotationId: UUID? = null
	var arguments: MutableList<PidMutableArgumentExpression> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidAnnotationExpression =
		PidAnnotationExpression.of(
			annotationId!!,
			arguments.toList(),
			tokens.toList()
		)
}
