package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidAllOfTypeReferenceSerializer

interface PidAllOfTypeReference : BitSerializable, PidTypeReference, Jsonable
{
	override val serializationKey: UUID
		get() = PidAllOfTypeReferenceSerializer.modelId
	
	override val type: String
		get() = "allOfTypeReference"
	val types: List<PidTypeReference>
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidAllOfTypeReference) return false
		
		if (type != other.type) return false
		if (types != other.types) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(types)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"types":${types.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(types: List<PidTypeReference>, tokens: List<PidCodeToken>): PidAllOfTypeReference =
			object : PidAllOfTypeReference
			{
				override val types: List<PidTypeReference> = types
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
