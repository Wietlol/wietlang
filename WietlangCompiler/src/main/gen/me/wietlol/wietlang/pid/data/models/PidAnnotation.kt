package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidAnnotationSerializer

interface PidAnnotation : BitSerializable, PidType, PidFirstClassComponent, Jsonable
{
	override val serializationKey: UUID
		get() = PidAnnotationSerializer.modelId
	
	override val type: String
		get() = "annotation"
	override val id: UUID
	override val name: String
	val members: List<PidAnnotationMember>
	override val annotations: List<PidAnnotationExpression>
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidAnnotation) return false
		
		if (type != other.type) return false
		if (id != other.id) return false
		if (name != other.name) return false
		if (members != other.members) return false
		if (annotations != other.annotations) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(id)
			.with(name)
			.with(members)
			.with(annotations)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"id":${id.toJson()},"name":${name.toJson()},"members":${members.toJson()},"annotations":${annotations.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(id: UUID, name: String, members: List<PidAnnotationMember>, annotations: List<PidAnnotationExpression>, tokens: List<PidCodeToken>): PidAnnotation =
			object : PidAnnotation
			{
				override val id: UUID = id
				override val name: String = name
				override val members: List<PidAnnotationMember> = members
				override val annotations: List<PidAnnotationExpression> = annotations
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
