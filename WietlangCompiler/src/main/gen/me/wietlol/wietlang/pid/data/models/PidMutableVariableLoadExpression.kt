package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableVariableLoadExpression : PidVariableLoadExpression, PidMutableExpression
{
	override var variableId: UUID
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(variableId: UUID, tokens: MutableList<PidCodeToken>): PidMutableVariableLoadExpression =
			object : PidMutableVariableLoadExpression
			{
				override var variableId: UUID = variableId
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
