package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidDirectTypeReferenceBuilder
{
	var referencedTypeId: UUID? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidDirectTypeReference =
		PidDirectTypeReference.of(
			referencedTypeId!!,
			tokens.toList()
		)
}
