package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidFileCodeSourceSerializer

interface PidFileCodeSource : BitSerializable, PidCodeSource, Jsonable
{
	override val serializationKey: UUID
		get() = PidFileCodeSourceSerializer.modelId
	
	val filePath: String
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidFileCodeSource) return false
		
		if (filePath != other.filePath) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(filePath)
	
	override fun toJson(): String =
		"""{"filePath":${filePath.toJson()}}"""
	
	companion object
	{
		fun of(filePath: String): PidFileCodeSource =
			object : PidFileCodeSource
			{
				override val filePath: String = filePath
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
