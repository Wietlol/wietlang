package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableAnnotationValue : PidAnnotationValue, PidMutableAnnotationMember, PidMutableNamedComponent
{
	override var id: UUID
	override var name: String
	override var upperBound: PidMutableTypeReference
	override val annotations: MutableList<PidMutableAnnotationExpression>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(id: UUID, name: String, upperBound: PidMutableTypeReference, annotations: MutableList<PidMutableAnnotationExpression>, tokens: MutableList<PidCodeToken>): PidMutableAnnotationValue =
			object : PidMutableAnnotationValue
			{
				override var id: UUID = id
				override var name: String = name
				override var upperBound: PidMutableTypeReference = upperBound
				override val annotations: MutableList<PidMutableAnnotationExpression> = annotations
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
