package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidLocalVariableDeclarationBuilder
{
	var id: UUID? = null
	var annotations: MutableList<PidMutableAnnotationExpression> = mutableListOf()
	var name: String? = null
	var isVariable: Boolean? = null
	var upperBound: PidMutableTypeReference? = null
	var initialValue: PidMutableExpression? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidLocalVariableDeclaration =
		PidLocalVariableDeclaration.of(
			id!!,
			annotations.toList(),
			name!!,
			isVariable!!,
			upperBound!!,
			initialValue,
			tokens.toList()
		)
}
