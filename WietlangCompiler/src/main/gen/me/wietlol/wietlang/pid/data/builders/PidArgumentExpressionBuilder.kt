package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidArgumentExpressionBuilder
{
	var targetId: UUID? = null
	var expression: PidMutableExpression? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidArgumentExpression =
		PidArgumentExpression.of(
			targetId!!,
			expression!!,
			tokens.toList()
		)
}
