package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidReturnExpressionSerializer

interface PidReturnExpression : BitSerializable, PidExpression, Jsonable
{
	override val serializationKey: UUID
		get() = PidReturnExpressionSerializer.modelId
	
	override val type: String
		get() = "returnExpression"
	val valueExpression: PidExpression?
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidReturnExpression) return false
		
		if (type != other.type) return false
		if (valueExpression != other.valueExpression) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(valueExpression)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"valueExpression":${valueExpression.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(valueExpression: PidExpression?, tokens: List<PidCodeToken>): PidReturnExpression =
			object : PidReturnExpression
			{
				override val valueExpression: PidExpression? = valueExpression
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
