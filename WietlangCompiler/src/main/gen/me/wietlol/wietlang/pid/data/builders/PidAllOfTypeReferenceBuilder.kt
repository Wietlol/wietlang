package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidAllOfTypeReferenceBuilder
{
	var types: MutableList<PidMutableTypeReference> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidAllOfTypeReference =
		PidAllOfTypeReference.of(
			types.toList(),
			tokens.toList()
		)
}
