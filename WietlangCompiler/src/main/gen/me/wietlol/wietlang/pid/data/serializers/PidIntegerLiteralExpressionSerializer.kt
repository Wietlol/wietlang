package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidIntegerLiteralExpressionSerializer : ModelSerializer<PidIntegerLiteralExpression, PidIntegerLiteralExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidIntegerLiteralExpression|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val valueIndex = 1
	private const val tokensIndex = 2
	
	override val modelId: UUID
		get() = UUID.fromString("be1703a8-2782-4d71-869d-e6a043157ad1")
	override val dataClass: Class<PidIntegerLiteralExpression>
		get() = PidIntegerLiteralExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidIntegerLiteralExpression)
	{
		stream.writeUnsignedVarInt(valueIndex)
		schema.serialize(stream, entity.value)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidIntegerLiteralExpression
	{
		val builder = PidIntegerLiteralExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				valueIndex -> builder.value = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
