package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidNamePathSerializer

interface PidNamePath : BitSerializable, PidNode, Jsonable
{
	override val serializationKey: UUID
		get() = PidNamePathSerializer.modelId
	
	override val type: String
		get() = "namePath"
	val parts: List<String>
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidNamePath) return false
		
		if (type != other.type) return false
		if (parts != other.parts) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(parts)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"parts":${parts.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(parts: List<String>, tokens: List<PidCodeToken>): PidNamePath =
			object : PidNamePath
			{
				override val parts: List<String> = parts
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
