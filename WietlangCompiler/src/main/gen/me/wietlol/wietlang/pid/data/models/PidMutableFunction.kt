package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableFunction : PidFunction, PidMutableFirstClassComponent
{
	override var id: UUID
	override var name: String
	override var returnType: PidMutableTypeReference
	override val body: MutableList<PidMutableExpression>?
	override val parameters: MutableList<PidMutableParameter>
	override val annotations: MutableList<PidMutableAnnotationExpression>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(id: UUID, name: String, returnType: PidMutableTypeReference, body: MutableList<PidMutableExpression>?, parameters: MutableList<PidMutableParameter>, annotations: MutableList<PidMutableAnnotationExpression>, tokens: MutableList<PidCodeToken>): PidMutableFunction =
			object : PidMutableFunction
			{
				override var id: UUID = id
				override var name: String = name
				override var returnType: PidMutableTypeReference = returnType
				override val body: MutableList<PidMutableExpression>? = body
				override val parameters: MutableList<PidMutableParameter> = parameters
				override val annotations: MutableList<PidMutableAnnotationExpression> = annotations
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
