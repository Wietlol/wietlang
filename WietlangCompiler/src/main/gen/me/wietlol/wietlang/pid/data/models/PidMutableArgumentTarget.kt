package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableArgumentTarget : PidArgumentTarget, PidMutableComponent
{
}
