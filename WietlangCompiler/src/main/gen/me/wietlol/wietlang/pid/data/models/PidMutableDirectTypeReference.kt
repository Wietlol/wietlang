package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableDirectTypeReference : PidDirectTypeReference, PidMutableTypeReference
{
	override var referencedTypeId: UUID
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(referencedTypeId: UUID, tokens: MutableList<PidCodeToken>): PidMutableDirectTypeReference =
			object : PidMutableDirectTypeReference
			{
				override var referencedTypeId: UUID = referencedTypeId
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
