package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidNamePathBuilder
{
	var parts: MutableList<String> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidNamePath =
		PidNamePath.of(
			parts.toList(),
			tokens.toList()
		)
}
