package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableComponent : PidComponent, PidNode, PidMutableAnnotatable
{
}
