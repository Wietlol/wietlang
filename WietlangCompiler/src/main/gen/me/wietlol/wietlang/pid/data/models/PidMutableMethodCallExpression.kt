package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableMethodCallExpression : PidMethodCallExpression, PidMutableExpression
{
	override var supplierExpression: PidMutableExpression
	override val arguments: MutableList<PidMutableArgumentExpression>
	override var methodId: UUID
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(supplierExpression: PidMutableExpression, arguments: MutableList<PidMutableArgumentExpression>, methodId: UUID, tokens: MutableList<PidCodeToken>): PidMutableMethodCallExpression =
			object : PidMutableMethodCallExpression
			{
				override var supplierExpression: PidMutableExpression = supplierExpression
				override val arguments: MutableList<PidMutableArgumentExpression> = arguments
				override var methodId: UUID = methodId
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
