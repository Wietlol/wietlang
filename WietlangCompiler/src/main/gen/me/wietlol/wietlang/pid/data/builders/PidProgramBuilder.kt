package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidProgramBuilder
{
	var rootPackage: PidMutablePackage? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidProgram =
		PidProgram.of(
			rootPackage!!,
			tokens.toList()
		)
}
