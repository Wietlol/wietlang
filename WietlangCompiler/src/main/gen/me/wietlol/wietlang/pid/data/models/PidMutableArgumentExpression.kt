package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableArgumentExpression : PidArgumentExpression, PidNode
{
	override var targetId: UUID
	override var expression: PidMutableExpression
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(targetId: UUID, expression: PidMutableExpression, tokens: MutableList<PidCodeToken>): PidMutableArgumentExpression =
			object : PidMutableArgumentExpression
			{
				override var targetId: UUID = targetId
				override var expression: PidMutableExpression = expression
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
