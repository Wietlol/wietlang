package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidImportBuilder
{
	var componentId: UUID? = null
	var alias: String? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidImport =
		PidImport.of(
			componentId!!,
			alias,
			tokens.toList()
		)
}
