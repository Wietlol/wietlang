package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableType : PidType, PidMutableNamedComponent
{
}
