package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidCompoundTypeReferenceBuilder
{
	var types: MutableList<PidMutableTypeReference> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidCompoundTypeReference =
		PidCompoundTypeReference.of(
			types.toList(),
			tokens.toList()
		)
}
