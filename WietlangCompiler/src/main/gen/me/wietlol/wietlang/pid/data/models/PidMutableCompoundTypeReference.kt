package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableCompoundTypeReference : PidCompoundTypeReference, PidMutableTypeReference
{
	override val types: MutableList<PidMutableTypeReference>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(types: MutableList<PidMutableTypeReference>, tokens: MutableList<PidCodeToken>): PidMutableCompoundTypeReference =
			object : PidMutableCompoundTypeReference
			{
				override val types: MutableList<PidMutableTypeReference> = types
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
