package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableAnnotation : PidAnnotation, PidMutableType, PidMutableFirstClassComponent
{
	override var id: UUID
	override var name: String
	override val members: MutableList<PidMutableAnnotationMember>
	override val annotations: MutableList<PidMutableAnnotationExpression>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(id: UUID, name: String, members: MutableList<PidMutableAnnotationMember>, annotations: MutableList<PidMutableAnnotationExpression>, tokens: MutableList<PidCodeToken>): PidMutableAnnotation =
			object : PidMutableAnnotation
			{
				override var id: UUID = id
				override var name: String = name
				override val members: MutableList<PidMutableAnnotationMember> = members
				override val annotations: MutableList<PidMutableAnnotationExpression> = annotations
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
