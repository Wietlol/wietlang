package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidMethodCallExpressionBuilder
{
	var supplierExpression: PidMutableExpression? = null
	var arguments: MutableList<PidMutableArgumentExpression> = mutableListOf()
	var methodId: UUID? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidMethodCallExpression =
		PidMethodCallExpression.of(
			supplierExpression!!,
			arguments.toList(),
			methodId!!,
			tokens.toList()
		)
}
