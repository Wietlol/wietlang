package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableStringLiteralExpression : PidStringLiteralExpression, PidMutableLiteralExpression
{
	override var value: String
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(value: String, tokens: MutableList<PidCodeToken>): PidMutableStringLiteralExpression =
			object : PidMutableStringLiteralExpression
			{
				override var value: String = value
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
