package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableNamedComponent : PidNamedComponent, PidMutableComponent
{
}
