package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidCompoundTypeReferenceSerializer : ModelSerializer<PidCompoundTypeReference, PidCompoundTypeReference>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidCompoundTypeReference|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val typesIndex = 1
	private const val tokensIndex = 2
	
	override val modelId: UUID
		get() = UUID.fromString("2c06670c-35bf-4bd0-a5c7-a78918cd2816")
	override val dataClass: Class<PidCompoundTypeReference>
		get() = PidCompoundTypeReference::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidCompoundTypeReference)
	{
		if (entity.types.isNotEmpty())
		{
			stream.writeUnsignedVarInt(typesIndex)
			entity.types.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidCompoundTypeReference
	{
		val builder = PidCompoundTypeReferenceBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				typesIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.types.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
