package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableLocalVariableDeclaration : PidLocalVariableDeclaration, PidMutableExpression, PidMutableVariable
{
	override var id: UUID
	override val annotations: MutableList<PidMutableAnnotationExpression>
	override var name: String
	override val isVariable: Boolean
	override var upperBound: PidMutableTypeReference
	override var initialValue: PidMutableExpression?
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(id: UUID, annotations: MutableList<PidMutableAnnotationExpression>, name: String, isVariable: Boolean, upperBound: PidMutableTypeReference, initialValue: PidMutableExpression?, tokens: MutableList<PidCodeToken>): PidMutableLocalVariableDeclaration =
			object : PidMutableLocalVariableDeclaration
			{
				override var id: UUID = id
				override val annotations: MutableList<PidMutableAnnotationExpression> = annotations
				override var name: String = name
				override val isVariable: Boolean = isVariable
				override var upperBound: PidMutableTypeReference = upperBound
				override var initialValue: PidMutableExpression? = initialValue
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
