package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidCodeTokenSerializer : ModelSerializer<PidCodeToken, PidCodeToken>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidCodeToken|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val textIndex = 1
	private const val typeIndex = 2
	private const val lineIndex = 3
	private const val columnIndex = 4
	private const val sourceIndex = 5
	
	override val modelId: UUID
		get() = UUID.fromString("9b7376b9-2a0c-4ae7-8ef0-add939aa2833")
	override val dataClass: Class<PidCodeToken>
		get() = PidCodeToken::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidCodeToken)
	{
		stream.writeUnsignedVarInt(textIndex)
		schema.serialize(stream, entity.text)
		
		stream.writeUnsignedVarInt(typeIndex)
		schema.serialize(stream, entity.type)
		
		stream.writeUnsignedVarInt(lineIndex)
		schema.serialize(stream, entity.line)
		
		stream.writeUnsignedVarInt(columnIndex)
		schema.serialize(stream, entity.column)
		
		stream.writeUnsignedVarInt(sourceIndex)
		schema.serialize(stream, entity.source)
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidCodeToken
	{
		val builder = PidCodeTokenBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				textIndex -> builder.text = schema.deserialize(stream)
				typeIndex -> builder.type = schema.deserialize(stream)
				lineIndex -> builder.line = schema.deserialize(stream)
				columnIndex -> builder.column = schema.deserialize(stream)
				sourceIndex -> builder.source = schema.deserialize(stream)
			}
		}
	}
}
