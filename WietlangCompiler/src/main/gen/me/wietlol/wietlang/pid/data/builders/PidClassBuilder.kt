package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidClassBuilder
{
	var id: UUID? = null
	var name: String? = null
	var members: MutableCollection<PidNode> = mutableListOf()
	var annotations: MutableList<PidMutableAnnotationExpression> = mutableListOf()
	var supertypes: MutableList<PidMutableTypeReference> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidClass =
		PidClass.of(
			id!!,
			name!!,
			members.toList(),
			annotations.toList(),
			supertypes.toList(),
			tokens.toList()
		)
}
