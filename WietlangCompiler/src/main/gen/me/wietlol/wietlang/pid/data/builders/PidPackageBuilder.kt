package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidPackageBuilder
{
	var id: UUID? = null
	var name: String? = null
	var members: MutableCollection<PidNode> = mutableListOf()
	var annotations: MutableList<PidMutableAnnotationExpression> = mutableListOf()
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidPackage =
		PidPackage.of(
			id!!,
			name!!,
			members.toList(),
			annotations.toList(),
			tokens.toList()
		)
}
