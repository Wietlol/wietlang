package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidAnnotationExpressionSerializer : ModelSerializer<PidAnnotationExpression, PidAnnotationExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidAnnotationExpression|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val annotationIdIndex = 1
	private const val argumentsIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("00fad29d-07d7-45fe-a01d-4d40651891f1")
	override val dataClass: Class<PidAnnotationExpression>
		get() = PidAnnotationExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidAnnotationExpression)
	{
		stream.writeUnsignedVarInt(annotationIdIndex)
		schema.serialize(stream, entity.annotationId)
		
		if (entity.arguments.isNotEmpty())
		{
			stream.writeUnsignedVarInt(argumentsIndex)
			entity.arguments.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidAnnotationExpression
	{
		val builder = PidAnnotationExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				annotationIdIndex -> builder.annotationId = schema.deserialize(stream)
				argumentsIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.arguments.add(schema.deserialize(stream, key))
					}
				}
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
