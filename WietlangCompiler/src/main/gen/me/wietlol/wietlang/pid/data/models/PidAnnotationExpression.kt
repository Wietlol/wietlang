package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidAnnotationExpressionSerializer

interface PidAnnotationExpression : BitSerializable, PidNode, Jsonable
{
	override val serializationKey: UUID
		get() = PidAnnotationExpressionSerializer.modelId
	
	override val type: String
		get() = "annotationExpression"
	val annotationId: UUID
	val arguments: List<PidArgumentExpression>
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidAnnotationExpression) return false
		
		if (type != other.type) return false
		if (annotationId != other.annotationId) return false
		if (arguments != other.arguments) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(annotationId)
			.with(arguments)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"annotationId":${annotationId.toJson()},"arguments":${arguments.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(annotationId: UUID, arguments: List<PidArgumentExpression>, tokens: List<PidCodeToken>): PidAnnotationExpression =
			object : PidAnnotationExpression
			{
				override val annotationId: UUID = annotationId
				override val arguments: List<PidArgumentExpression> = arguments
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
