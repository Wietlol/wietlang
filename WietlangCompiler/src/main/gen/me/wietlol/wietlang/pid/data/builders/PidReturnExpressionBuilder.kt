package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidReturnExpressionBuilder
{
	var valueExpression: PidMutableExpression? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidReturnExpression =
		PidReturnExpression.of(
			valueExpression,
			tokens.toList()
		)
}
