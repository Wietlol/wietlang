package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableAnnotationExpression : PidAnnotationExpression, PidNode
{
	override var annotationId: UUID
	override val arguments: MutableList<PidMutableArgumentExpression>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(annotationId: UUID, arguments: MutableList<PidMutableArgumentExpression>, tokens: MutableList<PidCodeToken>): PidMutableAnnotationExpression =
			object : PidMutableAnnotationExpression
			{
				override var annotationId: UUID = annotationId
				override val arguments: MutableList<PidMutableArgumentExpression> = arguments
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
