package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidFunctionCallExpressionSerializer

interface PidFunctionCallExpression : BitSerializable, PidExpression, Jsonable
{
	override val serializationKey: UUID
		get() = PidFunctionCallExpressionSerializer.modelId
	
	override val type: String
		get() = "functionCallExpression"
	val arguments: List<PidArgumentExpression>
	val functionId: UUID
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidFunctionCallExpression) return false
		
		if (type != other.type) return false
		if (arguments != other.arguments) return false
		if (functionId != other.functionId) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(arguments)
			.with(functionId)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"arguments":${arguments.toJson()},"functionId":${functionId.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(arguments: List<PidArgumentExpression>, functionId: UUID, tokens: List<PidCodeToken>): PidFunctionCallExpression =
			object : PidFunctionCallExpression
			{
				override val arguments: List<PidArgumentExpression> = arguments
				override val functionId: UUID = functionId
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
