package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidArgumentExpressionSerializer : ModelSerializer<PidArgumentExpression, PidArgumentExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidArgumentExpression|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val targetIdIndex = 1
	private const val expressionIndex = 2
	private const val tokensIndex = 3
	
	override val modelId: UUID
		get() = UUID.fromString("fdb324ce-6d5b-4add-894f-7b3cdc23495e")
	override val dataClass: Class<PidArgumentExpression>
		get() = PidArgumentExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidArgumentExpression)
	{
		stream.writeUnsignedVarInt(targetIdIndex)
		schema.serialize(stream, entity.targetId)
		
		stream.writeUnsignedVarInt(expressionIndex)
		schema.serialize(stream, entity.expression)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidArgumentExpression
	{
		val builder = PidArgumentExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				targetIdIndex -> builder.targetId = schema.deserialize(stream)
				expressionIndex -> builder.expression = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
