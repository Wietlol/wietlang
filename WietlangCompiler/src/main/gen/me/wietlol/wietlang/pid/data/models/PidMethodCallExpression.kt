package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidMethodCallExpressionSerializer

interface PidMethodCallExpression : BitSerializable, PidExpression, Jsonable
{
	override val serializationKey: UUID
		get() = PidMethodCallExpressionSerializer.modelId
	
	override val type: String
		get() = "methodCallExpression"
	val supplierExpression: PidExpression
	val arguments: List<PidArgumentExpression>
	val methodId: UUID
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidMethodCallExpression) return false
		
		if (type != other.type) return false
		if (supplierExpression != other.supplierExpression) return false
		if (arguments != other.arguments) return false
		if (methodId != other.methodId) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(supplierExpression)
			.with(arguments)
			.with(methodId)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"supplierExpression":${supplierExpression.toJson()},"arguments":${arguments.toJson()},"methodId":${methodId.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(supplierExpression: PidExpression, arguments: List<PidArgumentExpression>, methodId: UUID, tokens: List<PidCodeToken>): PidMethodCallExpression =
			object : PidMethodCallExpression
			{
				override val supplierExpression: PidExpression = supplierExpression
				override val arguments: List<PidArgumentExpression> = arguments
				override val methodId: UUID = methodId
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
