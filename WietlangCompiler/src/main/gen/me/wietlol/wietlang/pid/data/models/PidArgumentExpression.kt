package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidArgumentExpressionSerializer

interface PidArgumentExpression : BitSerializable, PidNode, Jsonable
{
	override val serializationKey: UUID
		get() = PidArgumentExpressionSerializer.modelId
	
	override val type: String
		get() = "argumentExpression"
	val targetId: UUID
	val expression: PidExpression
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidArgumentExpression) return false
		
		if (type != other.type) return false
		if (targetId != other.targetId) return false
		if (expression != other.expression) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(targetId)
			.with(expression)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"targetId":${targetId.toJson()},"expression":${expression.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(targetId: UUID, expression: PidExpression, tokens: List<PidCodeToken>): PidArgumentExpression =
			object : PidArgumentExpression
			{
				override val targetId: UUID = targetId
				override val expression: PidExpression = expression
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
