package me.wietlol.wietlang.pid.data.builders

import me.wietlol.wietlang.pid.data.models.*
import java.util.*

class PidNameExpressionBuilder
{
	var variable: PidMutableVariable? = null
	var tokens: MutableList<PidCodeToken> = mutableListOf()
	
	fun build(): PidNameExpression =
		PidNameExpression.of(
			variable!!,
			tokens.toList()
		)
}
