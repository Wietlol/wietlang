package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableClassMember : PidClassMember, PidMutableComponent
{
}
