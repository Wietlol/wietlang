package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableProgram : PidProgram, PidNode
{
	override var rootPackage: PidMutablePackage
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(rootPackage: PidMutablePackage, tokens: MutableList<PidCodeToken>): PidMutableProgram =
			object : PidMutableProgram
			{
				override var rootPackage: PidMutablePackage = rootPackage
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
