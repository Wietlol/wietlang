package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidDirectTypeReferenceSerializer : ModelSerializer<PidDirectTypeReference, PidDirectTypeReference>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidDirectTypeReference|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val referencedTypeIdIndex = 1
	private const val tokensIndex = 2
	
	override val modelId: UUID
		get() = UUID.fromString("138809ab-cf32-48cc-914a-796b09f8aaa6")
	override val dataClass: Class<PidDirectTypeReference>
		get() = PidDirectTypeReference::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidDirectTypeReference)
	{
		stream.writeUnsignedVarInt(referencedTypeIdIndex)
		schema.serialize(stream, entity.referencedTypeId)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidDirectTypeReference
	{
		val builder = PidDirectTypeReferenceBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				referencedTypeIdIndex -> builder.referencedTypeId = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
