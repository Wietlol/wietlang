package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableNamePath : PidNamePath, PidNode
{
	override val parts: MutableList<String>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(parts: MutableList<String>, tokens: MutableList<PidCodeToken>): PidMutableNamePath =
			object : PidMutableNamePath
			{
				override val parts: MutableList<String> = parts
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
