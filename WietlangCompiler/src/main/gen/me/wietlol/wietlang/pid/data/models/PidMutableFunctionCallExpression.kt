package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableFunctionCallExpression : PidFunctionCallExpression, PidMutableExpression
{
	override val arguments: MutableList<PidMutableArgumentExpression>
	override var functionId: UUID
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(arguments: MutableList<PidMutableArgumentExpression>, functionId: UUID, tokens: MutableList<PidCodeToken>): PidMutableFunctionCallExpression =
			object : PidMutableFunctionCallExpression
			{
				override val arguments: MutableList<PidMutableArgumentExpression> = arguments
				override var functionId: UUID = functionId
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
