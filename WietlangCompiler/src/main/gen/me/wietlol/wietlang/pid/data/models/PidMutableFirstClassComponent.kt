package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableFirstClassComponent : PidFirstClassComponent, PidMutableNamedComponent
{
}
