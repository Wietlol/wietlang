package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidLocalVariableDeclarationSerializer

interface PidLocalVariableDeclaration : BitSerializable, PidExpression, PidVariable, Jsonable
{
	override val serializationKey: UUID
		get() = PidLocalVariableDeclarationSerializer.modelId
	
	override val type: String
		get() = "variableDeclaration"
	override val id: UUID
	override val annotations: List<PidAnnotationExpression>
	override val name: String
	val isVariable: Boolean
	override val upperBound: PidTypeReference
	val initialValue: PidExpression?
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidLocalVariableDeclaration) return false
		
		if (type != other.type) return false
		if (id != other.id) return false
		if (annotations != other.annotations) return false
		if (name != other.name) return false
		if (isVariable != other.isVariable) return false
		if (upperBound != other.upperBound) return false
		if (initialValue != other.initialValue) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(id)
			.with(annotations)
			.with(name)
			.with(isVariable)
			.with(upperBound)
			.with(initialValue)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"id":${id.toJson()},"annotations":${annotations.toJson()},"name":${name.toJson()},"isVariable":${isVariable.toJson()},"upperBound":${upperBound.toJson()},"initialValue":${initialValue.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(id: UUID, annotations: List<PidAnnotationExpression>, name: String, isVariable: Boolean, upperBound: PidTypeReference, initialValue: PidExpression?, tokens: List<PidCodeToken>): PidLocalVariableDeclaration =
			object : PidLocalVariableDeclaration
			{
				override val id: UUID = id
				override val annotations: List<PidAnnotationExpression> = annotations
				override val name: String = name
				override val isVariable: Boolean = isVariable
				override val upperBound: PidTypeReference = upperBound
				override val initialValue: PidExpression? = initialValue
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
