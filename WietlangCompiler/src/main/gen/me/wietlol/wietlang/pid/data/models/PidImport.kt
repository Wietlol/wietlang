package me.wietlol.wietlang.pid.data.models

import me.wietlol.bitblock.api.serialization.BitSerializable
import me.wietlol.common.emptyHashCode
import me.wietlol.common.Jsonable
import me.wietlol.common.toJson
import me.wietlol.common.with
import java.util.*
import me.wietlol.wietlang.pid.data.serializers.PidImportSerializer

interface PidImport : BitSerializable, PidNode, Jsonable
{
	override val serializationKey: UUID
		get() = PidImportSerializer.modelId
	
	override val type: String
		get() = "import"
	val componentId: UUID
	val alias: String?
	override val tokens: List<PidCodeToken>
	
	fun isEqualTo(other: Any?): Boolean
	{
		if (this === other) return true
		if (other == null) return false
		if (other !is PidImport) return false
		
		if (type != other.type) return false
		if (componentId != other.componentId) return false
		if (alias != other.alias) return false
		if (tokens != other.tokens) return false
		
		return true
	}
	
	fun computeHashCode(): Int =
		emptyHashCode
			.with(type)
			.with(componentId)
			.with(alias)
			.with(tokens)
	
	override fun toJson(): String =
		"""{"type":${type.toJson()},"componentId":${componentId.toJson()},"alias":${alias.toJson()},"tokens":${tokens.toJson()}}"""
	
	companion object
	{
		fun of(componentId: UUID, alias: String?, tokens: List<PidCodeToken>): PidImport =
			object : PidImport
			{
				override val componentId: UUID = componentId
				override val alias: String? = alias
				override val tokens: List<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
