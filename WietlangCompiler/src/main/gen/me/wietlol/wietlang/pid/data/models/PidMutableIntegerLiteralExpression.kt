package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableIntegerLiteralExpression : PidIntegerLiteralExpression, PidMutableNumberLiteralExpression
{
	override var value: Int
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(value: Int, tokens: MutableList<PidCodeToken>): PidMutableIntegerLiteralExpression =
			object : PidMutableIntegerLiteralExpression
			{
				override var value: Int = value
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
