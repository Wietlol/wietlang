package me.wietlol.wietlang.pid.data.serializers

import me.wietlol.bitblock.api.serialization.ModelSerializer
import me.wietlol.bitblock.api.serialization.Schema
import me.wietlol.bitblock.api.serialization.deserialize
import me.wietlol.bitblock.core.BitBlock
import me.wietlol.bitblock.core.registry.CommonModelRegistryKey
import me.wietlol.common.readUnsignedVarInt
import me.wietlol.common.writeUnsignedVarInt
import java.io.InputStream
import java.io.OutputStream
import java.util.*

import me.wietlol.wietlang.pid.data.models.*
import me.wietlol.wietlang.pid.data.builders.*

object PidNameExpressionSerializer : ModelSerializer<PidNameExpression, PidNameExpression>
{
	init
	{
		val registryKey = CommonModelRegistryKey("PidNameExpression|WietlangPid|Wietlol|1.0")
		BitBlock.modelRegistry[registryKey] = this
	}
	
	private const val endOfObject = 0
	private const val variableIndex = 1
	private const val tokensIndex = 2
	
	override val modelId: UUID
		get() = UUID.fromString("bdc6141c-f09b-4f1b-b247-12ec7c3ce817")
	override val dataClass: Class<PidNameExpression>
		get() = PidNameExpression::class.java
	
	override fun serialize(stream: OutputStream, schema: Schema, entity: PidNameExpression)
	{
		stream.writeUnsignedVarInt(variableIndex)
		schema.serialize(stream, entity.variable)
		
		if (entity.tokens.isNotEmpty())
		{
			stream.writeUnsignedVarInt(tokensIndex)
			entity.tokens.forEach {
				schema.serialize(stream, it)
			}
			stream.writeUnsignedVarInt(endOfObject)
		}
		
		stream.writeUnsignedVarInt(endOfObject)
	}
	
	override fun deserialize(stream: InputStream, schema: Schema): PidNameExpression
	{
		val builder = PidNameExpressionBuilder()
		
		while (true)
		{
			when (stream.readUnsignedVarInt())
			{
				endOfObject -> return builder.build()
				variableIndex -> builder.variable = schema.deserialize(stream)
				tokensIndex ->
				{
					while (true)
					{
						val key = stream.readUnsignedVarInt()
						if (key == 0)
							break
						
						builder.tokens.add(schema.deserialize(stream, key))
					}
				}
			}
		}
	}
}
