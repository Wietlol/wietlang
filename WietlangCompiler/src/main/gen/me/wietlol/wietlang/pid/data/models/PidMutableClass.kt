package me.wietlol.wietlang.pid.data.models

import java.util.*

interface PidMutableClass : PidClass, PidMutableType, PidMutableFirstClassComponent, PidMutableContainer
{
	override var id: UUID
	override var name: String
	override val members: MutableCollection<PidNode>
	override val annotations: MutableList<PidMutableAnnotationExpression>
	override val supertypes: MutableList<PidMutableTypeReference>
	override val tokens: MutableList<PidCodeToken>
	
	companion object
	{
		fun of(id: UUID, name: String, members: MutableCollection<PidNode>, annotations: MutableList<PidMutableAnnotationExpression>, supertypes: MutableList<PidMutableTypeReference>, tokens: MutableList<PidCodeToken>): PidMutableClass =
			object : PidMutableClass
			{
				override var id: UUID = id
				override var name: String = name
				override val members: MutableCollection<PidNode> = members
				override val annotations: MutableList<PidMutableAnnotationExpression> = annotations
				override val supertypes: MutableList<PidMutableTypeReference> = supertypes
				override val tokens: MutableList<PidCodeToken> = tokens
				
				override fun equals(other: Any?): Boolean =
					isEqualTo(other)
				
				override fun hashCode(): Int =
					computeHashCode()
				
				override fun toString(): String =
					toJson()
			}
	}
}
