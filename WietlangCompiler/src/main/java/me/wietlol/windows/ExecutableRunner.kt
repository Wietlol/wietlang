package me.wietlol.windows

import java.nio.file.Files

object ExecutableRunner
{
	fun run(executable: ByteArray): Int
	{
		val tempDirectory = Files.createTempDirectory("windows-runner").toFile()
		
		tempDirectory
			.resolve("executable.exe")
			.also { it.createNewFile() }
			.also { it.writeBytes(executable) }
		
		val processBuilder = ProcessBuilder()
			.command(
				"cmd.exe",
				"/c",
				"executable.exe"
			)
			.directory(tempDirectory)
			.inheritIO()
		
		val process = processBuilder.start()
		
		return process.waitFor()
			.also { tempDirectory.delete() }
			.also {
				if (it != 0)
					throw Exception("Program failed with exit code $it.")
			}
	}
}