package me.wietlol.wietlang.compiler

import me.wietlol.wietlang.ast.data.models.AstCodeToken
import me.wietlol.wietlang.ast.data.models.AstFileCodeSource
import me.wietlol.wietlang.pid.data.models.PidCodeToken
import me.wietlol.wietlang.pid.data.models.PidFileCodeSource

class CompilationException : Exception
{
	constructor(message: String, source: PidCodeToken?) : super(generateMessage(message, source))
	constructor(message: String, source: AstCodeToken?) : super(generateMessage(message, source))
	
	companion object
	{
		private fun generateMessage(message: String, source: PidCodeToken?): String =
			if (source == null)
				"$message."
			else
				"$message at ${source.source.let { it as PidFileCodeSource }.filePath} (${source.line}:${source.column})."
		
		private fun generateMessage(message: String, source: AstCodeToken?): String =
			if (source == null)
				"$message."
			else
				"$message at ${source.source.let { it as AstFileCodeSource }.filePath} (${source.line}:${source.column})."
	}
}