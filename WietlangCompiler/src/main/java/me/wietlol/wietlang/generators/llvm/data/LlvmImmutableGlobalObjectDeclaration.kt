package me.wietlol.wietlang.generators.llvm.data

data class LlvmImmutableGlobalObjectDeclaration(
	override val variable: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<*>>>,
	override val values: List<LlvmExpression<*>>
) : LlvmGlobalObjectDeclaration
{
	init
	{
		if (variable.isGlobal.not())
			TODO("stop being an idiot")
		
		if (values.size != variable.type.reference.referencedType.fields.size)
			TODO("stop being an idiot")
	}
}