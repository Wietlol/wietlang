package me.wietlol.wietlang.generators.llvm.data

data class LlvmFunctionTypeReference<out T : LlvmTypeReference>(
	val parameters: List<LlvmTypeReference>,
	val returnType: T
) : LlvmTypeReference
{
	override fun toIr(): String =
		"${returnType.toIr()}(${parameters.joinToString { it.toIr() }})"
}