package me.wietlol.wietlang.generators.llvm.data

interface LlvmNode
{
	fun toIr(): String
}