package me.wietlol.wietlang.generators.llvm.data

interface LlvmTypeDeclaration : LlvmNode
{
	val name: String
	val fields: List<LlvmTypeReference>
}