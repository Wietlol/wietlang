package me.wietlol.wietlang.generators.llvm

import me.wietlol.wietlang.generators.llvm.data.LlvmClass
import me.wietlol.wietlang.generators.llvm.data.LlvmExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmFunctionDefinition
import me.wietlol.wietlang.generators.llvm.data.LlvmFunctionTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerLiteralExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmMutableGlobalObjectDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmNamedTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmNullExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmPointerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmReturnExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmStructDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmVariable
import me.wietlol.wietlang.generators.llvm.data.LlvmVariableExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmVirtualTable
import java.util.concurrent.atomic.AtomicInteger

class LlvmClassBuilder
{
	private val typeIdentityCounter = AtomicInteger(1)
	
	private val classDeclarationMembers = mutableListOf<LlvmTypeReference>()
	private val classObjectMembers = mutableListOf<LlvmExpression<*>>()
	val `class` = run {
		val classType: LlvmStructDeclaration
		val vTableFunction: LlvmFunctionDefinition
		val objectVariable: LlvmVariable<*>
		
		LlvmClass(
			LlvmStructDeclaration(
				"Class",
				classDeclarationMembers
			)
				.also { classDeclarationMembers.add(LlvmPointerTypeReference(LlvmNamedTypeReference(it))) }
				.also { classDeclarationMembers.add(LlvmIntegerTypeReference(32)) }
				.also {
					classDeclarationMembers.add(
						LlvmPointerTypeReference(
							LlvmFunctionTypeReference(
								listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(it))),
								LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
							)
						)
					)
				}
				.also { classType = it },
			LlvmVirtualTable(
				LlvmStructDeclaration(
					"Class_VTable",
					listOf(
						LlvmIntegerTypeReference(8)
					)
				),
				listOf(),
				LlvmFunctionDefinition(
					LlvmVariable("Class_VTableLoader", true, LlvmPointerTypeReference(
						LlvmFunctionTypeReference(
							listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(classType))),
							LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
						)
					)),
					listOf(
						LlvmReturnExpression(LlvmNullExpression(LlvmPointerTypeReference(LlvmIntegerTypeReference(8))))
					) // todo
				)
					.also { vTableFunction = it }
			),
			LlvmMutableGlobalObjectDeclaration(
				LlvmVariable("Class_Object", true, LlvmPointerTypeReference(LlvmNamedTypeReference(classType)))
					.also { objectVariable = it },
				listOf(
					LlvmVariableExpression(objectVariable),
					LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(32), 0),
					LlvmVariableExpression(vTableFunction.variable)
				)
			)
		)
	}
//		.also { classDeclarationMembers.add(LlvmPointerTypeReference(LlvmNamedTypeReference(it.typeDeclaration))) }
//		.also { classDeclarationMembers.add(LlvmIntegerTypeReference(32)) }
//		.also {
//			classDeclarationMembers.add(
//				LlvmPointerTypeReference(
//					LlvmFunctionTypeReference(
//						listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(it.typeDeclaration))),
//						LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
//					)
//				)
//			)
//		}
	
	fun build(name: String, fields: List<LlvmTypeReference>): LlvmClass
	{
		val vTableFunction: LlvmFunctionDefinition
		return LlvmClass(
			LlvmStructDeclaration(
				name,
				listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(`class`.typeDeclaration)))
					+ fields
			),
			LlvmVirtualTable(
				LlvmStructDeclaration(
					"${name}_VTable",
					listOf(
						LlvmIntegerTypeReference(8)
					)
				),
				listOf(),
				LlvmFunctionDefinition(
					LlvmVariable("${name}_VTableLoader", true, LlvmPointerTypeReference(
						LlvmFunctionTypeReference(
							listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(`class`.typeDeclaration))),
							LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
						)
					)),
					listOf(
						LlvmReturnExpression(LlvmNullExpression(LlvmPointerTypeReference(LlvmIntegerTypeReference(8))))
					)
				)
					.also { vTableFunction = it }
			),
			LlvmMutableGlobalObjectDeclaration(
				LlvmVariable("${name}_Object", true, LlvmPointerTypeReference(LlvmNamedTypeReference(`class`.typeDeclaration))),
				listOf(
					LlvmVariableExpression(`class`.typeObject.variable),
					LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(32), typeIdentityCounter.getAndAdd(1)),
					LlvmVariableExpression(vTableFunction.variable)
				)
			)
		)
	}
}