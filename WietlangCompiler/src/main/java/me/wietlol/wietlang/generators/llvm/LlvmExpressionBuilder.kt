package me.wietlol.wietlang.generators.llvm

import me.wietlol.wietlang.generators.llvm.data.LlvmAllocateExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmBitCastExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmExtractValueExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmFunction
import me.wietlol.wietlang.generators.llvm.data.LlvmFunctionCallExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmFunctionDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmFunctionDefinition
import me.wietlol.wietlang.generators.llvm.data.LlvmFunctionTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmGetElementPointerExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmGlobalObjectDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmImmutableGlobalObjectDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmInsertValueExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerLiteralExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmLoadExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmLocalVariableDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmNamedTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmPointerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmReturnExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmReturnVoidExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmStoreExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmStringConstantDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmTypeDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmUndefinedExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmVariable
import me.wietlol.wietlang.generators.llvm.data.LlvmVariableExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmVoidTypeReference
import me.wietlol.wietlang.pid.data.models.PidAnnotatable
import me.wietlol.wietlang.pid.data.models.PidClass
import me.wietlol.wietlang.pid.data.models.PidClassMethod
import me.wietlol.wietlang.pid.data.models.PidCompoundTypeReference
import me.wietlol.wietlang.pid.data.models.PidDirectTypeReference
import me.wietlol.wietlang.pid.data.models.PidExpression
import me.wietlol.wietlang.pid.data.models.PidFunction
import me.wietlol.wietlang.pid.data.models.PidFunctionCallExpression
import me.wietlol.wietlang.pid.data.models.PidLocalVariableDeclaration
import me.wietlol.wietlang.pid.data.models.PidMutableMethodCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutableVariableLoadExpression
import me.wietlol.wietlang.pid.data.models.PidParameter
import me.wietlol.wietlang.pid.data.models.PidReturnExpression
import me.wietlol.wietlang.pid.data.models.PidStringLiteralExpression
import me.wietlol.wietlang.pid.data.models.PidType
import me.wietlol.wietlang.pid.data.models.PidTypeReference
import me.wietlol.wietlang.pid.linker.PidProgramLinker
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

internal class LlvmExpressionBuilder(
	private val programBuilder: LlvmProgramBuilder,
	linker: PidProgramLinker
) : PidProgramLinker by linker
{
	internal val stringConstants: MutableMap<String, LlvmStringConstantDeclaration> = HashMap()
	internal val stringGlobalObjects: MutableMap<String, LlvmGlobalObjectDeclaration> = HashMap()
	private val variableLinker: MutableMap<UUID, LlvmVariable<*>> = HashMap()
	internal val functions: MutableCollection<LlvmFunction> = ArrayList()
	private lateinit var entryPoint: LlvmFunction
	
	internal val main: LlvmFunction by lazy {
		LlvmFunctionDefinition(
			LlvmVariable(
				"main",
				true,
				LlvmPointerTypeReference(
					LlvmFunctionTypeReference(
						listOf(),
						LlvmIntegerTypeReference(32)
					)
				)
			),
			if (::entryPoint.isInitialized)
			{
				if (entryPoint.variable.type.reference.returnType == LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration)))
				{
					val exitCodeObject: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<LlvmTypeDeclaration>>>
					val exitCodePointer: LlvmVariable<LlvmPointerTypeReference<LlvmIntegerTypeReference>>
					val exitCode: LlvmVariable<LlvmIntegerTypeReference>
					listOf(
						LlvmLocalVariableDeclaration(
							LlvmVariable("v0", false, LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration)))
								.also { exitCodeObject = it },
							LlvmFunctionCallExpression.of(LlvmVariableExpression(entryPoint.variable), listOf())
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v1", false, LlvmPointerTypeReference(LlvmIntegerTypeReference(32)))
								.also { exitCodePointer = it },
							LlvmGetElementPointerExpression(
								LlvmVariableExpression(exitCodeObject),
								listOf(0, 1),
								LlvmIntegerTypeReference::class.java
							)
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v2", false, LlvmIntegerTypeReference(32))
								.also { exitCode = it },
							LlvmLoadExpression(LlvmVariableExpression(exitCodePointer))
						),
						LlvmReturnExpression(LlvmVariableExpression(exitCode))
					)
				}
				else
				{
					listOf(
						LlvmFunctionCallExpression.of(LlvmVariableExpression(entryPoint.variable), listOf()),
						LlvmReturnExpression(
							LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(32), 0)
						)
					)
				}
			}
			else
				listOf(
					LlvmReturnExpression(
						LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(32), 0)
					)
				)
		)
	}
	
	private fun PidFunction.convertBody(): List<LlvmExpression<*>>
	{
		val counter = AtomicInteger()
		
		return body!!
			.flatMap { it.convert(counter).expressions }
	}
	
	private fun PidClassMethod.convertBody(): List<LlvmExpression<*>>
	{
		val counter = AtomicInteger()
		
		return body
			?.flatMap { it.convert(counter).expressions }
			?: emptyList()
	}
	
	private fun PidExpression.convert(counter: AtomicInteger): LlvmExpressionSet<*> =
		when (this)
		{
			is PidFunctionCallExpression -> convert(counter)
			is PidStringLiteralExpression -> convert(counter)
			is PidReturnExpression -> convert(counter)
			is PidLocalVariableDeclaration -> convert(counter)
			is PidMutableVariableLoadExpression -> convert(counter)
			is PidMutableMethodCallExpression -> convert(counter)
			else -> throw IllegalArgumentException("Invalid type for expression of class: $type ${javaClass.name}")
		}
	
	@Suppress("UNCHECKED_CAST")
	private fun PidFunctionCallExpression.convert(counter: AtomicInteger): LlvmExpressionSet<*>
	{
		val llvmArguments = arguments.map { it.expression.convert(counter) }
		
		val llvmFunction = programBuilder.produceFunction(function)
		
		if (llvmFunction.type.returnType is LlvmVoidTypeReference)
		{
			return LlvmExpressionSet<LlvmVoidTypeReference>(
				llvmArguments
					.flatMap { it.expressions }
					.plus(
						LlvmFunctionCallExpression.of(
							LlvmVariableExpression(llvmFunction.variable) as LlvmExpression<LlvmPointerTypeReference<LlvmFunctionTypeReference<LlvmVoidTypeReference>>>,
							llvmArguments.map { LlvmVariableExpression(it.result!!) }
						)
					),
				null
			)
		}
		else
		{
			val result = LlvmVariable(
				counter.nextVariable(),
				false,
				llvmFunction.type.returnType
			)
			
			return LlvmExpressionSet(
				llvmArguments
					.flatMap { it.expressions }
					.plus(
						LlvmLocalVariableDeclaration(
							result,
							LlvmFunctionCallExpression.of(
								LlvmVariableExpression(llvmFunction.variable),
								llvmArguments.map { LlvmVariableExpression(it.result!!) },
								llvmFunction.type.returnType.javaClass
							)
						)
					),
				result
			)
		}
	}
	
	private fun PidStringLiteralExpression.convert(counter: AtomicInteger): LlvmExpressionSet<*>
	{
		val globalObject: LlvmGlobalObjectDeclaration = stringGlobalObjects[value]
			?: run {
				val stringConstant = LlvmStringConstantDeclaration.of(
					"c_${stringConstants.size}",
					value
				)
					.also { stringConstants[value] = it }
				
				LlvmImmutableGlobalObjectDeclaration(
					LlvmVariable(
						"c${stringConstants.size}",
						true,
						LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.stringDeclaration.typeDeclaration))
					),
					listOf(
						LlvmVariableExpression(programBuilder.stringDeclaration.typeObject.variable),
						LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(32), value.length), // todo utf16
						LlvmGetElementPointerExpression(
							LlvmVariableExpression(stringConstant.variable),
							listOf(0, 0),
							LlvmIntegerTypeReference::class.java,
							false
						)
					)
				)
					.also { stringGlobalObjects[value] = it }
			}
		
		return LlvmExpressionSet(
			emptyList(),
			globalObject.variable
		)
	}
	
	private fun PidReturnExpression.convert(counter: AtomicInteger): LlvmExpressionSet<*>
	{
		val valueExpression = valueExpression
			?: return LlvmExpressionSet<LlvmVoidTypeReference>(
				listOf(
					LlvmReturnVoidExpression
				),
				null
			)
		
		val x = valueExpression.convert(counter)
		
		return LlvmExpressionSet<LlvmVoidTypeReference>(
			x.expressions.plus(
				LlvmReturnExpression(
					LlvmVariableExpression(x.result!!)
				)
			),
			null
		)
	}
	
	private fun PidLocalVariableDeclaration.convert(counter: AtomicInteger): LlvmExpressionSet<*>
	{
		val initialValue = initialValue!! // todo handle late-inits
		val initialExpression = initialValue.convert(counter)
		
		return LlvmExpressionSet(
			initialExpression.expressions,
			initialExpression.result
				.also { variableLinker[id] = it!! }
		)
	}
	
	private fun PidMutableVariableLoadExpression.convert(counter: AtomicInteger): LlvmExpressionSet<*> =
		LlvmExpressionSet(
			listOf(),
			variableLinker[variableId]!!
		)
	
	
	@Suppress("UNCHECKED_CAST")
	private fun PidMutableMethodCallExpression.convert(counter: AtomicInteger): LlvmExpressionSet<*>
	{
		// todo make loading from vtable context based rather than call based to optimize performance on multiple method calls on the same object provided by the same virtual table
		val supplier = supplierExpression.convert(counter) as LlvmExpressionSet<LlvmPointerTypeReference<*>>
		val arguments = arguments.map { it.expression.convert(counter) }
		
		val parentType = programBuilder.produceTypeDeclaration(method.parentComponent as PidClass)
		val virtualMethodIndex = 1 // todo find out which index should be used
		val virtualMethod = parentType.virtualTable.typeDeclaration.fields[virtualMethodIndex] as LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>
		
		val classObjectPointerPointer: LlvmVariable<LlvmPointerTypeReference<LlvmPointerTypeReference<LlvmNamedTypeReference<LlvmTypeDeclaration>>>>
		val classObjectPointer: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<LlvmTypeDeclaration>>>
		val classObject: LlvmVariable<LlvmNamedTypeReference<LlvmTypeDeclaration>>
		val virtualTableLoader: LlvmVariable<LlvmPointerTypeReference<LlvmFunctionTypeReference<LlvmPointerTypeReference<LlvmIntegerTypeReference>>>>
		val virtualTableStub: LlvmVariable<LlvmPointerTypeReference<LlvmIntegerTypeReference>>
		val virtualTable: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<LlvmTypeDeclaration>>>
		val methodPointer: LlvmVariable<LlvmPointerTypeReference<LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>>>
		val method: LlvmVariable<LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>>
		val result: LlvmVariable<*>
		
		return LlvmExpressionSet(
			supplier.expressions
				+ arguments.flatMap { it.expressions }
				+ listOf(
				LlvmLocalVariableDeclaration(
					LlvmVariable(counter.nextVariable(), false, LlvmPointerTypeReference(LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.classDeclaration.typeDeclaration))))
						.also { classObjectPointerPointer = it },
					LlvmGetElementPointerExpression(
						LlvmVariableExpression(supplier.result!!),
						listOf(0, 0),
						LlvmPointerTypeReference::class.java
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(counter.nextVariable(), false, LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.classDeclaration.typeDeclaration)))
						.also { classObjectPointer = it },
					LlvmLoadExpression(
						LlvmVariableExpression(classObjectPointerPointer)
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(counter.nextVariable(), false, LlvmNamedTypeReference(programBuilder.classDeclaration.typeDeclaration))
						.also { classObject = it },
					LlvmLoadExpression(
						LlvmVariableExpression(classObjectPointer)
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(
						counter.nextVariable(),
						false,
						LlvmPointerTypeReference(
							LlvmFunctionTypeReference(
								listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.classDeclaration.typeDeclaration))),
								LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
							)
						)
					)
						.also { virtualTableLoader = it },
					LlvmExtractValueExpression(
						LlvmVariableExpression(classObject),
						2,
						LlvmPointerTypeReference::class.java
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(
						counter.nextVariable(),
						false,
						LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
					)
						.also { virtualTableStub = it },
					LlvmFunctionCallExpression.of(
						LlvmVariableExpression(virtualTableLoader),
						listOf(LlvmVariableExpression(parentType.typeObject.variable))
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(
						counter.nextVariable(),
						false,
						LlvmPointerTypeReference(LlvmNamedTypeReference(parentType.virtualTable.typeDeclaration))
					)
						.also { virtualTable = it },
					LlvmBitCastExpression(
						LlvmVariableExpression(virtualTableStub),
						LlvmPointerTypeReference(LlvmNamedTypeReference(parentType.virtualTable.typeDeclaration))
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(
						counter.nextVariable(),
						false,
						LlvmPointerTypeReference(virtualMethod)
					)
						.also { methodPointer = it },
					LlvmGetElementPointerExpression(
						LlvmVariableExpression(virtualTable),
						listOf(0, virtualMethodIndex),
						LlvmPointerTypeReference::class.java
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(
						counter.nextVariable(),
						false,
						virtualMethod
					)
						.also { method = it },
					LlvmLoadExpression(
						LlvmVariableExpression(methodPointer)
					)
				),
				LlvmLocalVariableDeclaration(
					LlvmVariable(
						counter.nextVariable(),
						false,
						virtualMethod.reference.returnType
					)
						.also { result = it },
					LlvmFunctionCallExpression.of(
						LlvmVariableExpression(method),
						listOf(LlvmVariableExpression(supplier.result)),
						result.type.javaClass
					)
				)
			),
			result
		)
	}
	
	internal fun convertFunction(pidFunction: PidFunction): LlvmFunction =
		pidFunction.convertFunction()
	
	@JvmName("_convertFunction")
	private fun PidFunction.convertFunction(): LlvmFunction
	{
		val nativeName = getNativeName()
		if (nativeName != null)
			return convertNativeFunction(nativeName)
		
		return LlvmFunctionDefinition(
			LlvmVariable(
				"_$name",
				true,
				LlvmPointerTypeReference(
					LlvmFunctionTypeReference(
						parameters.map { LlvmPointerTypeReference(it.toLlvm()) },
						LlvmPointerTypeReference(returnType.toLlvm())
					)
				)
			),
			convertBody()
		)
			.also {
				if (isEntryPoint())
					entryPoint = it
			}
	}
	
	internal fun convertMethod(pidMethod: PidClassMethod): LlvmFunction? =
		pidMethod.convertMethod()
	
	@JvmName("_convertMethod")
	private fun PidClassMethod.convertMethod(): LlvmFunction?
	{
		val nativeName = getNativeName()
		if (nativeName != null)
			return convertNativeMethod(nativeName)
		
		if (isAbstract())
			return null
		
		return LlvmFunctionDefinition(
			LlvmVariable(
				"_$name",
				true,
				LlvmPointerTypeReference(
					LlvmFunctionTypeReference(
						listOf((parentComponent as PidType).asReference().toLlvm()) + parameters.map { it.toLlvm() },
						returnType.toLlvm()
					)
				)
			),
			convertBody()
		)
	}
	
	@Suppress("UNCHECKED_CAST")
	private fun PidFunction.convertNativeFunction(nativeName: String): LlvmFunction =
		when (nativeName)
		{
			"system:std:printLine" -> LlvmFunctionDefinition(
				LlvmVariable("_$name", true, LlvmPointerTypeReference(
					LlvmFunctionTypeReference(
						parameters.map { it.toLlvm() },
						returnType.toLlvm()
					)
				)),
				run {
					val v0: LlvmVariable<LlvmNamedTypeReference<LlvmTypeDeclaration>>
					val v1: LlvmVariable<LlvmPointerTypeReference<LlvmIntegerTypeReference>>
					
					listOf(
						// todo refactor to getelementptr
						LlvmLocalVariableDeclaration(
							LlvmVariable("v0", false, LlvmNamedTypeReference(programBuilder.stringDeclaration.typeDeclaration))
								.also { v0 = it },
							LlvmLoadExpression(
								LlvmVariableExpression(LlvmVariable("0", false, LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.stringDeclaration.typeDeclaration))))
							)
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v1", false, LlvmPointerTypeReference(LlvmIntegerTypeReference(8)))
								.also { v1 = it },
							LlvmExtractValueExpression(
								LlvmVariableExpression(v0),
								2,
								LlvmPointerTypeReference::class.java
							)
						),
						LlvmFunctionCallExpression.of(
							LlvmFunctionDeclaration(
								LlvmVariable(
									"puts",
									true,
									LlvmPointerTypeReference(
										LlvmFunctionTypeReference(
											listOf(
												LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
											),
											LlvmIntegerTypeReference(32)
										)
									)
								)
							)
								.also { functions.add(it) }
								.let { it.variable }
								.let { LlvmVariableExpression(it) as LlvmExpression<LlvmPointerTypeReference<LlvmFunctionTypeReference<LlvmIntegerTypeReference>>> },
							listOf(
								LlvmVariableExpression(v1)
							)
						),
						LlvmReturnVoidExpression
					)
				}
			)
			else -> TODO("wot?")
		}
	
	private fun PidClassMethod.convertNativeMethod(nativeName: String): LlvmFunction =
		when (nativeName)
		{
			"system:text:string:getLength" -> LlvmFunctionDefinition(
				LlvmVariable(
					"_$name",
					true,
					LlvmPointerTypeReference(
						LlvmFunctionTypeReference(
							listOf(LlvmPointerTypeReference((parentComponent as PidType).asReference().toLlvm()))
								+ parameters.map { it.toLlvm() },
							LlvmPointerTypeReference(returnType.toLlvm())
						)
					)
				),
				run {
					val v0: LlvmVariable<LlvmPointerTypeReference<LlvmIntegerTypeReference>>
					val v1: LlvmVariable<LlvmIntegerTypeReference>
					val v2: LlvmVariable<LlvmNamedTypeReference<LlvmTypeDeclaration>>
					val v3: LlvmVariable<LlvmNamedTypeReference<LlvmTypeDeclaration>>
					val v4: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<LlvmTypeDeclaration>>>
					
					listOf(
						LlvmLocalVariableDeclaration(
							LlvmVariable("v0", false, LlvmPointerTypeReference(LlvmIntegerTypeReference(32)))
								.also { v0 = it },
							LlvmGetElementPointerExpression(
								LlvmVariableExpression(LlvmVariable("0", false, LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.stringDeclaration.typeDeclaration)))),
								listOf(
									0,
									1
								),
								LlvmIntegerTypeReference::class.java
							)
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v1", false, LlvmIntegerTypeReference(32))
								.also { v1 = it },
							LlvmLoadExpression(
								LlvmVariableExpression(v0)
							)
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v2", false, LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration))
								.also { v2 = it },
							LlvmInsertValueExpression(
								LlvmUndefinedExpression(LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration)),
								LlvmVariableExpression(programBuilder.integerDeclaration.typeObject.variable),
								0
							)
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v3", false, LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration))
								.also { v3 = it },
							LlvmInsertValueExpression(
								LlvmVariableExpression(v2),
								LlvmVariableExpression(v1),
								1
							)
						),
						LlvmLocalVariableDeclaration(
							LlvmVariable("v4", false, LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration)))
								.also { v4 = it },
							LlvmAllocateExpression(
								LlvmNamedTypeReference(programBuilder.integerDeclaration.typeDeclaration)
							)
						),
						LlvmStoreExpression(
							LlvmVariableExpression(v3),
							LlvmVariableExpression(v4)
						),
						LlvmReturnExpression(
							LlvmVariableExpression(v4)
						)
					)
				}
			)
			else -> TODO("wot?")
		}
	
	private fun PidParameter.toLlvm(): LlvmTypeReference =
		LlvmPointerTypeReference(upperBound.toLlvm())
	
	private fun PidTypeReference.toLlvm(): LlvmTypeReference =
		when (this)
		{
			is PidDirectTypeReference -> toLlvm()
			is PidCompoundTypeReference -> toLlvm()
			else -> TODO("wot?")
		}
	
	private fun PidDirectTypeReference.toLlvm(): LlvmTypeReference =
		LlvmNamedTypeReference(
			programBuilder.produceTypeDeclaration(referencedType).typeDeclaration
		)
	
	private fun PidCompoundTypeReference.toLlvm(): LlvmTypeReference =
		when
		{
			types.isEmpty() -> LlvmVoidTypeReference
			types.size == 1 -> types.single().toLlvm()
			else -> TODO()
		}
	
	internal fun getSignature(pidClassMethod: PidClassMethod): LlvmFunctionTypeReference<*> =
		pidClassMethod.getSignature()
	
	@JvmName("_getSignature")
	private fun PidClassMethod.getSignature(): LlvmFunctionTypeReference<*>
	{
		return LlvmFunctionTypeReference(
			listOf(LlvmPointerTypeReference((parentComponent as PidType).asReference().toLlvm())) + parameters.map { LlvmPointerTypeReference(it.toLlvm()) }
				.map { LlvmPointerTypeReference(it) },
			LlvmPointerTypeReference(returnType.toLlvm())
		)
	}
	
	private fun PidAnnotatable.getNativeName(): String? =
		annotations
			.singleOrNull { it.annotation.name == "Native" } // todo get direct reference to native annotation from metadata rather than based on name
			?.arguments
			?.first()
			?.let { it.expression as PidStringLiteralExpression }
			?.let { it.value }
	
	private fun PidFunction.isEntryPoint(): Boolean =
		annotations
			.any { it.annotation.name == "EntryPoint" } // todo get direct reference to native annotation from metadata rather than based on name
	
	private fun AtomicInteger.nextVariable(): String =
		"v${addAndGet(1)}"
}