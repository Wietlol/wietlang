package me.wietlol.wietlang.generators.llvm.data

data class LlvmVirtualTable(
	var typeDeclaration: LlvmTypeDeclaration,
	var tables: List<LlvmGlobalObjectDeclaration>,
	var loaderFunction: LlvmFunctionDefinition
)