//package me.wietlol.wietlang.generators.llvm
//
//import com.common.recursiveMapPreOrder
//import me.wietlol.wietlang.pid.data.models.PidAnnotatable
//import me.wietlol.wietlang.pid.data.models.PidArgumentExpression
//import me.wietlol.wietlang.pid.data.models.PidArgumentTarget
//import me.wietlol.wietlang.pid.data.models.PidClass
//import me.wietlol.wietlang.pid.data.models.PidClassMethod
//import me.wietlol.wietlang.pid.data.models.PidComponent
//import me.wietlol.wietlang.pid.data.models.PidContainer
//import me.wietlol.wietlang.pid.data.models.PidExpression
//import me.wietlol.wietlang.pid.data.models.PidFunction
//import me.wietlol.wietlang.pid.data.models.PidFunctionCallExpression
//import me.wietlol.wietlang.pid.data.models.PidParameter
//import me.wietlol.wietlang.pid.data.models.PidProgram
//import me.wietlol.wietlang.pid.data.models.PidStringLiteralExpression
//import me.wietlol.wietlang.pid.data.models.PidType
//import me.wietlol.wietlang.pid.data.models.PidTypeReference
//import me.wietlol.wietlang.pid.linker.PidProgramLinker
//import java.io.PrintWriter
//import java.util.concurrent.atomic.AtomicInteger
//import java.util.function.Supplier
//
//class LlvmProgramWriter(
//	private val writer: PrintWriter,
//	val program: PidProgram,
//	linker: PidProgramLinker
//) : PidProgramLinker by linker
//{
//	private val functionValueCounterMap: MutableMap<PidFunction, () -> Int> = HashMap()
//	private val PidFunction.valueCounter: () -> Int
//		get() =
//			functionValueCounterMap.computeIfAbsent(this) {
//				val integer = AtomicInteger();
//				{ integer.addAndGet(1) }
//			}
//
//	fun writeProgram()
//	{
//		writer.writeHeader()
//	}
//
//	private fun PrintWriter.writeHeader()
//	{
//		println()
//		println("target triple = \"x86_64-pc-windows-msvc19.15.26726\"")
//		println()
//		println("%Class = type { i8*(%Class*)* }")
//		println()
//
//		val classes = program
//			.rootPackage
//			.let { sequenceOf(it) }
//			.recursiveMapPreOrder<PidContainer> { it.members.asSequence().filterIsInstance<PidContainer>() }
//			.filterIsInstance<PidClass>()
//			.toList()
//
//		classes.forEach { `class` ->
//			if (`class`.isNative())
//			{
//				println("%${`class`.name}_vTable = type {") // todo new virtual methods
//				println("\ti8" + if (`class`.members.isEmpty()) "" else ",")
//				`class`.members
//					.asSequence()
//					.filterIsInstance<PidClassMethod>()
//					.filter { true } // todo filter out overrides
//					.filter { true } // todo filter out private
//					.map { "\ti32(%${`class`.name}*)*" } // todo return type and arguments
//					.joinToString(",\r\n")
//					.also { if (it.isNotBlank()) println(it) }
//				println("}")
//
//				println("%${`class`.name} = type { %Class*, i32, i8* }") // todo fields
//			}
//			else
//			{
//				println("%${`class`.name}_vTable = type { ... }") // todo new virtual methods
//				println("%${`class`.name} = type { %Class*, ... }") // todo fields
//			}
//			println()
//		}
//
//		classes.forEach { `class` ->
//			println("@${`class`.name}_Class = global %Class {")
//			println("\ti8*(%Class*)* @${`class`.name}_loadVTable")
//			println("}")
//			println()
//
//			println("@${`class`.name}_${`class`.name}_vTable_data = global %${`class`.name}_vTable {")
//			println("\ti8 0" + if (`class`.members.isEmpty()) "" else ",")
//			`class`.members
//				.asSequence()
//				.filterIsInstance<PidClassMethod>()
//				.filter { true } // todo filter out overrides
//				.filter { true } // todo filter out private
//				.map { "\ti32(%${`class`.name}*)* @${`class`.name}_${it.name}" } // todo return type and arguments
//				.joinToString(",\r\n")
//				.also { if (it.isNotBlank()) println(it) }
//			println("}")
//			println()
//
//			// todo foreach supertype, generate vTable data
////			println("@${it.name}_${super.name}_vTable_data = global %${super.name}_vTable {")
////			println("    i32(%string*)* @string_getLength")
////			println("}")
////			println()
//		}
//
//
//		val functions = program
//			.rootPackage
//			.let { sequenceOf(it) }
//			.recursiveMapPreOrder<PidComponent> {
//				if (it is PidContainer)
//					it.members.asSequence().filterIsInstance<PidComponent>()
//				else
//					emptySequence()
//			}
//			.filterIsInstance<PidFunction>()
//			.toList()
//
//		functions.forEach {
//			if (it.isNative())
//			{
//				if (it.name == "printLine")
//				{
//					println("define void @${it.name}(%NativeString*) {")
//					println("\t%value = load %NativeString, %NativeString* %0")
//					println("\t%chars = extractvalue %NativeString %value, 2")
//					println("\tcall i32 @puts(i8* %chars)")
//					println("\tret void")
//					println("}")
//					println("; todo find out how to print null characters")
//					println("; todo possibly by @putchar or @putc")
//					println("declare i32 @puts(i8* nocapture) nounwind")
//					println()
//				}
//			}
//			else
//			{
//				val body = it.body
//				if (body != null)
//				{
//					println("define void @${it.name}() {")
//					body.forEach { expr ->
//						printLlvmIr(expr, it.valueCounter)
//					}
//					println("\tret i32 0")
//					println("}")
//					println()
//
//				}
//
//				// todo
//			}
//		}
//	}
//
//	private fun PidAnnotatable.isNative(): Boolean =
//		annotations
//			.filter { it.annotation.name == "Native" } // todo get direct reference to native annotation rather than based on name
//			.any()
//
//	private fun PrintWriter.printLlvmIr(expression: PidExpression, valueCounter: () -> Int)
//	{
//		println("\t${toLlvmIr(expression, valueCounter)}")
//	}
//
//	private fun toLlvmIr(expression: PidExpression, valueCounter: () -> Int): CharSequence =
//		when (expression)
//		{
//			is PidFunctionCallExpression -> "\tcall void @${expression.function.name}(${expression.arguments.joinToString(", ") { toLlvmIr(it, valueCounter) }})"
//			is PidStringLiteralExpression -> {
//				val elementPointer = valueCounter()
//				val struct1 = valueCounter()
//				val struct2 = valueCounter()
//				val struct3 = valueCounter()
//				val variable = valueCounter()
//				val stringTypeName = "NativeString"
//
//				"\t%t$elementPointer = getelementptr [1 x i8], [1 x i8]* @const, i64 0, i64 0\r\n" +
//					"\t%t$struct1 = insertvalue %$stringTypeName undef, i32 1, 0 ; todo pointer to class\r\n" +
//					"\t%t$struct2 = insertvalue %$stringTypeName %t$struct1, i32 1, 1\r\n" +
//					"\t%t$struct3 = insertvalue %$stringTypeName %t$struct2, i8* %t$elementPointer, 2\r\n" +
//					"\t%l$variable = alloca %$stringTypeName\r\n" +
//					"\tstore %$stringTypeName %t$struct3, %$stringTypeName* %l$variable"
//			}
////			; stack immediate value
////			%t1 = getelementptr [14 x i8], [14 x i8]* @.c1, i64 0, i64 0
////			%t2 = insertvalue %struct.string undef, i32 14, 0
////			%l1 = insertvalue %struct.string %t2, i8* %t1, 1
////			; stack pointer value
////			%l2 = alloca %struct.string
////			store %struct.string %l1, %struct.string* %l2
//			else -> throw IllegalArgumentException("Unknown type for expression: '${expression.type}'.")
//		}
//
//	private fun toLlvmIr(expression: PidArgumentExpression, valueCounter: () -> Int): CharSequence =
//		"${toLlvmIr(expression.target, valueCounter)} ${toLlvmIr(expression.expression, valueCounter)}"
//
//	private fun toLlvmIr(target: PidArgumentTarget, valueCounter: () -> Int): CharSequence =
//		when (target)
//		{
//			is PidParameter -> "i64"
//			else -> throw IllegalArgumentException("Unknown type for expression: '${target.type}'.")
//		}
//}