package me.wietlol.wietlang.generators.llvm.data

data class LlvmFunctionCallExpression<T : LlvmTypeReference> private constructor(
	val target: LlvmExpression<LlvmPointerTypeReference<LlvmFunctionTypeReference<T>>>,
	val arguments: List<LlvmExpression<*>>,
	val targetType: Class<T>
) : LlvmExpression<T>
{
	init
	{
		if (arguments.size != target.resultType.reference.parameters.size)
			TODO("stop being an idiot")
	}
	
	override val resultType: T =
		targetType.cast(target.resultType.reference.returnType)
	
	override fun toIr(): String =
		"call ${resultType.toIr()} ${target.toIr()}(${arguments.joinToString { "${it.resultType.toIr()} ${it.toIr()}" }})"
	
	companion object
	{
		inline fun <reified T : LlvmTypeReference> of(target: LlvmExpression<LlvmPointerTypeReference<LlvmFunctionTypeReference<T>>>, arguments: List<LlvmExpression<*>>) =
			of(
				target,
				arguments,
				T::class.java
			)
		
		fun <T : LlvmTypeReference> of(target: LlvmExpression<LlvmPointerTypeReference<LlvmFunctionTypeReference<T>>>, arguments: List<LlvmExpression<*>>, targetType: Class<T>) =
			LlvmFunctionCallExpression(
				target,
				arguments
					.zip(target.resultType.reference.parameters)
					.map { (arg, param) ->
						if (arg.resultType == param)
							arg
						else
							LlvmBitCastExpression(arg, param, false)
					},
				targetType
			)
	}
}