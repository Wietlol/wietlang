package me.wietlol.wietlang.generators.llvm.data

data class LlvmStringConstantDeclaration private constructor(
	val variable: LlvmVariable<LlvmPointerTypeReference<LlvmArrayTypeReference<LlvmIntegerTypeReference>>>,
	val text: String
) : LlvmNode
{
	init
	{
		if (variable.isGlobal.not())
			TODO("stop being an idiot")
		
		if (text.contains('\u0000'))
			TODO("we dont support null characters yet")
	}
	
	override fun toIr(): String =
		"${variable.toIr()} = private unnamed_addr constant ${variable.type.reference.toIr()} c\"$text\\00\""
	
	companion object
	{
		fun of(name: String, text: String): LlvmStringConstantDeclaration =
			LlvmStringConstantDeclaration(
				LlvmVariable(name, true, LlvmPointerTypeReference(LlvmArrayTypeReference(text.length + 1, LlvmIntegerTypeReference(8)))),
				text
			)
	}
}
