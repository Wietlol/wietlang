package me.wietlol.wietlang.generators.llvm.data

data class LlvmUndefinedExpression<T : LlvmTypeReference>(
	override val resultType: T
) : LlvmExpression<T>
{
	override fun toIr(): String =
		"undef"
}