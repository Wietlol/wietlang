package me.wietlol.wietlang.generators.llvm.data

data class LlvmVariableExpression<T : LlvmTypeReference>(
	val variable: LlvmVariable<T>
) : LlvmExpression<T>
{
	override val resultType: T
		get() = variable.type
	
	override fun toIr(): String =
		variable.toIr()
}