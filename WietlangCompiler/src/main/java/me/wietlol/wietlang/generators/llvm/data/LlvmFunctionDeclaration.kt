package me.wietlol.wietlang.generators.llvm.data

class LlvmFunctionDeclaration(
	override val variable: LlvmVariable<LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>>
) : LlvmFunction
{
	override fun toIr(): String =
		"declare ${type.returnType.toIr()} ${variable.toIr()}(${type.parameters.joinToString { it.toIr() }})"
}