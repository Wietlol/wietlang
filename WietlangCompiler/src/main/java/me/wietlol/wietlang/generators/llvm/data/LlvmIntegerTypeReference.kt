package me.wietlol.wietlang.generators.llvm.data

data class LlvmIntegerTypeReference(
	val bits: Int
) : LlvmTypeReference
{
	override fun toIr(): String =
		"i$bits"
}