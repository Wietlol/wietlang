package me.wietlol.wietlang.generators.llvm.data

data class LlvmLocalVariableDeclaration<T : LlvmTypeReference>(
	val variable: LlvmVariable<T>,
	val expression: LlvmExpression<T>
) : LlvmExpression<T>
{
	override val resultType: T
		get() = variable.type
	
	override fun toIr(): String =
		"${variable.toIr()} = ${expression.toIr()}"
}