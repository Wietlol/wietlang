package me.wietlol.wietlang.generators.llvm.data

interface LlvmFunction : LlvmNode
{
	val type: LlvmFunctionTypeReference<*>
		get() = variable.type.reference
	
	val variable: LlvmVariable<LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>>
}