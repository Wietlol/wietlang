package me.wietlol.wietlang.generators.llvm.data

object LlvmVoidTypeReference : LlvmTypeReference
{
	override fun toIr(): String
		= "void"
}