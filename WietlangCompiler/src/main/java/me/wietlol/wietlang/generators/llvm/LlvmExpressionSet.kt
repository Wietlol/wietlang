package me.wietlol.wietlang.generators.llvm

import me.wietlol.wietlang.generators.llvm.data.LlvmExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmVariable

class LlvmExpressionSet<T : LlvmTypeReference>(
	val expressions: List<LlvmExpression<*>>,
	val result: LlvmVariable<T>?
)
{
	fun toIr(): String =
		expressions.joinToString(", ") { it.toIr() }
}