package me.wietlol.wietlang.generators.llvm.data

import me.wietlol.wietlang.generators.llvm.LlvmProgramBuilder

class LlvmClass(
	val typeDeclaration: LlvmTypeDeclaration,
	val virtualTable: LlvmVirtualTable,
	val typeObject: LlvmMutableGlobalObjectDeclaration
)
{
	val identity: Int
		get() = typeObject
			.values[1]
			.let { it as LlvmIntegerLiteralExpression }
			.value
	
	var virtualTableIsSet = false
	internal fun provideVirtualTable(
		programBuilder: LlvmProgramBuilder,
		methodSignatures: Sequence<LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>>,
		virtualTables: Sequence<Pair<LlvmClass, LlvmGlobalObjectDeclaration>>
	): LlvmClass
	{
		if (virtualTableIsSet.not())
		{
			virtualTableIsSet = true
			
			virtualTable.typeDeclaration = LlvmStructDeclaration(
				"${typeDeclaration.name}_VTable",
				listOf(LlvmIntegerTypeReference(8))
					.plus(methodSignatures)
			)
			virtualTable.tables = virtualTables.map { it.second }.toList()
			virtualTable.loaderFunction =
				LlvmFunctionDefinition(
					LlvmVariable("${typeDeclaration.name}_VTableLoader", true, LlvmPointerTypeReference(
						LlvmFunctionTypeReference(
							listOf(LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.classDeclaration.typeDeclaration))),
							LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
						)
					)),
					run {
						val identityPointer: LlvmVariable<LlvmPointerTypeReference<LlvmIntegerTypeReference>>
						val identity: LlvmVariable<LlvmIntegerTypeReference>
						
						listOf(
							LlvmLocalVariableDeclaration(
								LlvmVariable("v0", false, LlvmPointerTypeReference(LlvmIntegerTypeReference(32)))
									.also { identityPointer = it },
								LlvmGetElementPointerExpression(
									LlvmVariableExpression(LlvmVariable("0", false, LlvmPointerTypeReference(LlvmNamedTypeReference(programBuilder.classDeclaration.typeDeclaration)))),
									listOf(0, 1),
									LlvmIntegerTypeReference(32).javaClass
								)
							),
							LlvmLocalVariableDeclaration(
								LlvmVariable("v1", false, LlvmIntegerTypeReference(32))
									.also { identity = it },
								LlvmLoadExpression(
									LlvmVariableExpression(identityPointer)
								)
							),
							LlvmSwitchExpression(
								LlvmVariableExpression(identity),
								virtualTables
									.associate { (`class`, classObject) ->
										Pair(
											LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(32), `class`.identity),
											LlvmSwitchNode(
												"l${`class`.identity}",
												listOf(
													LlvmReturnExpression(
														LlvmBitCastExpression(
															LlvmVariableExpression(classObject.variable),
															LlvmPointerTypeReference(LlvmIntegerTypeReference(8)),
															false
														)
													)
												)
											)
										)
									},
								LlvmSwitchNode(
									"none",
									listOf(
										LlvmReturnExpression(LlvmNullExpression(LlvmPointerTypeReference(LlvmIntegerTypeReference(8))))
									)
								)
							)
						)
					}
				)
			
			typeObject.values = typeObject.values
				.dropLast(1)
				.plus(LlvmVariableExpression(virtualTable.loaderFunction.variable))
		}
		return this
	}


//	define i8* @NativeString_vTableLoader(%Class*) {
//		%v0 = getelementptr %Class, %Class* %0, i32 0, i32 1
//		%v1 = load i32, i32* %v0
//		switch i32 %v1, label %none [
//		i32 2, label %l2
//		i32 1, label %l1
//		]
//		l2:
//		ret i8* bitcast (%String_vTable* @NativeString_String_vTable_data to i8*)
//		l1:
//		ret i8* bitcast (%NativeString_vTable* @NativeString_NativeString_vTable_data to i8*)
//		none:
//		ret i8* null
//	}
}