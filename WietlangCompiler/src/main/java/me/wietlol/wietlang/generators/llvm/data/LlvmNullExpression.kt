package me.wietlol.wietlang.generators.llvm.data

data class LlvmNullExpression<T : LlvmTypeReference>(
	override val resultType: T
) : LlvmExpression<T>
{
	override fun toIr(): String =
		"null"
}