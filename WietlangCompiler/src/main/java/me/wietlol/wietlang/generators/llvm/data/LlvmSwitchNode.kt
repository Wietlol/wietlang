package me.wietlol.wietlang.generators.llvm.data

data class LlvmSwitchNode(
	val labelName: String,
	val expressions: List<LlvmExpression<*>>
)