package me.wietlol.wietlang.generators.llvm.data

data class LlvmPointerTypeReference<out T : LlvmTypeReference>(
	val reference: T
) : LlvmTypeReference
{
	override fun toIr(): String =
		"${reference.toIr()}*"
}