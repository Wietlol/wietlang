package me.wietlol.wietlang.generators.llvm.data

data class LlvmExtractValueExpression<T : LlvmTypeReference>(
	val sourceExpression: LlvmExpression<LlvmNamedTypeReference<*>>,
	val index: Int,
	val targetType: Class<T>
) : LlvmExpression<T>
{
	override val resultType: T =
		targetType.cast(sourceExpression.resultType.referencedType.fields[index])
	
	override fun toIr(): String =
		"extractvalue ${sourceExpression.resultType.toIr()} ${sourceExpression.toIr()}, $index"
}