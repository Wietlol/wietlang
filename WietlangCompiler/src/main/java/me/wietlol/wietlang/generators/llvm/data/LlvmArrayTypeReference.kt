package me.wietlol.wietlang.generators.llvm.data

data class LlvmArrayTypeReference<T : LlvmTypeReference>(
	val size: Int,
	val type: T
) : LlvmTypeReference
{
	override fun toIr(): String =
		"[$size x ${type.toIr()}]"
}