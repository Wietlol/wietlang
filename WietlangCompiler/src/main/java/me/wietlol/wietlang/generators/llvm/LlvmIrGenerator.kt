package me.wietlol.wietlang.generators.llvm

import me.wietlol.wietlang.generators.llvm.data.LlvmProgram
import me.wietlol.wietlang.pid.data.models.PidProgram
import me.wietlol.wietlang.pid.linker.PidProgramLinker

class LlvmIrGenerator(
	private val linker: PidProgramLinker
)
{
	fun generate(program: PidProgram): LlvmProgram =
		LlvmProgramBuilder(program, linker).build()
}