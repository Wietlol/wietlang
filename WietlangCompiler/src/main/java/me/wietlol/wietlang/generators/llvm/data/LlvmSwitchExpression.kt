package me.wietlol.wietlang.generators.llvm.data

data class LlvmSwitchExpression(
	val value: LlvmExpression<LlvmIntegerTypeReference>,
	val nodes: Map<LlvmIntegerLiteralExpression, LlvmSwitchNode>,
	val defaultNode: LlvmSwitchNode
) : LlvmExpression<LlvmVoidTypeReference>
{
	override val resultType: LlvmVoidTypeReference
		get() = LlvmVoidTypeReference
	
	override fun toIr(): String =
		StringBuilder().apply {
			append("switch ${value.resultType.toIr()} ${value.toIr()}, label %${defaultNode.labelName} [ ${nodes.asSequence().joinToString(" ") { (key, value) -> "${key.type.toIr()} ${key.value}, label %${value.labelName}" }} ]\r\n")
			nodes
				.values
				.plus(defaultNode)
				.forEach { node ->
					append("\t${node.labelName}:\r\n")
					node.expressions.forEach {
						append("\t${it.toIr()}\r\n")
					}
				}
		}.toString()
}

//switch i32 %v1, label %none [
//    i32 2, label %l2
//    i32 1, label %l1
//]
//l2:
//ret i8* bitcast (%String_vTable* @NativeString_String_vTable_data to i8*)
//l1:
//ret i8* bitcast (%NativeString_vTable* @NativeString_NativeString_vTable_data to i8*)
//none:
//ret i8* null