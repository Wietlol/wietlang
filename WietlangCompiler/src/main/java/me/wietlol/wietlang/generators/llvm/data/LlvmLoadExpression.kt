package me.wietlol.wietlang.generators.llvm.data

data class LlvmLoadExpression<T : LlvmTypeReference>(
	val sourceExpression: LlvmExpression<LlvmPointerTypeReference<T>>
) : LlvmExpression<T>
{
	override val resultType: T
		get() = sourceExpression.resultType.reference
	
	override fun toIr(): String =
		"load ${resultType.toIr()}, ${sourceExpression.resultType.toIr()} ${sourceExpression.toIr()}"
}