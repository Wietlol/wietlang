package me.wietlol.wietlang.generators.llvm.data

data class LlvmReturnExpression<T : LlvmTypeReference>(
	val value: LlvmExpression<T>
) : LlvmExpression<LlvmVoidTypeReference>
{
	override val resultType: LlvmVoidTypeReference
		get() = LlvmVoidTypeReference
	
	override fun toIr(): String =
		"ret ${value.resultType.toIr()} ${value.toIr()}"
}