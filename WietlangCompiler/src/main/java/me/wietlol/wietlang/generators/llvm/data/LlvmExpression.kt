package me.wietlol.wietlang.generators.llvm.data

interface LlvmExpression<T : LlvmTypeReference> : LlvmNode
{
	val resultType: T
}