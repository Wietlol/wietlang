package me.wietlol.wietlang.generators.llvm.data

data class LlvmVariable<out T : LlvmTypeReference>(
	val name: String,
	val isGlobal: Boolean,
	val type: T
) : LlvmNode
{
	override fun toIr(): String =
		if (isGlobal)
			"@$name"
		else
			"%$name"
}