package me.wietlol.wietlang.generators.llvm.data

data class LlvmStoreExpression<T : LlvmTypeReference>(
	val value: LlvmExpression<T>,
	val target: LlvmExpression<LlvmPointerTypeReference<T>>
) : LlvmExpression<LlvmVoidTypeReference>
{
	override val resultType: LlvmVoidTypeReference
		get() = LlvmVoidTypeReference
	
	override fun toIr(): String =
		"store ${value.resultType.toIr()} ${value.toIr()}, ${target.resultType.toIr()} ${target.toIr()}"
}