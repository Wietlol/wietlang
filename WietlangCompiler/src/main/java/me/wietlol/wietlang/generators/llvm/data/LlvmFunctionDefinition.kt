package me.wietlol.wietlang.generators.llvm.data

class LlvmFunctionDefinition(
	override val variable: LlvmVariable<LlvmPointerTypeReference<LlvmFunctionTypeReference<*>>>,
	val body: List<LlvmExpression<*>>
) : LlvmFunction
{
	init
	{
		if (body.isEmpty())
			TODO("stop being an idiot")
	}
	
	override val type: LlvmFunctionTypeReference<*> = variable.type.reference
	
	override fun toIr(): String =
		StringBuilder().apply {
			append("define ${type.returnType.toIr()} ${variable.toIr()}(${type.parameters.joinToString { it.toIr() }}) {\r\n")
			body.forEach {
				append("\t${it.toIr()}\r\n")
			}
			append("}")
		}.toString()
}
