package me.wietlol.wietlang.generators.llvm.data

interface LlvmGlobalObjectDeclaration : LlvmNode
{
	val variable: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<*>>>
	val values: List<LlvmExpression<*>>
	
	override fun toIr(): String =
		StringBuilder().apply {
			append("${variable.toIr()} = global ${variable.type.reference.toIr()} {\r\n")
			append("\t${values.joinToString(",\r\n\t") { "${it.resultType.toIr()} ${it.toIr()}" }}\r\n")
			append("}")
		}.toString()
}