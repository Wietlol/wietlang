package me.wietlol.wietlang.generators.llvm.data

data class LlvmMutableGlobalObjectDeclaration(
	override var variable: LlvmVariable<LlvmPointerTypeReference<LlvmNamedTypeReference<*>>>,
	override var values: List<LlvmExpression<*>>
) : LlvmGlobalObjectDeclaration
{
	init
	{
		if (variable.isGlobal.not())
			TODO("stop being an idiot")
		
		if (values.size != variable.type.reference.referencedType.fields.size)
			TODO("stop being an idiot")
	}
}