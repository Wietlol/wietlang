package me.wietlol.wietlang.generators.llvm.data

data class LlvmProgram(
	val stringConstants: List<LlvmStringConstantDeclaration>,
	val objectConstants: List<LlvmGlobalObjectDeclaration>,
	val classes: List<LlvmClass>,
	val functions: List<LlvmFunction>,
	val headers: List<String> = listOf(
		"target triple = \"x86_64-pc-windows-msvc19.15.26726\""
	)
)
{
	private val ln = "\r\n"
	fun toIr(): String =
		StringBuilder().apply {
			
			headers.forEach {
				append(it + ln)
			}
			append(ln)
			
			stringConstants.forEach {
				append(it.toIr() + ln)
			}
			append(ln)
			
			classes
				.asSequence()
				.sortedBy { it.identity }
				.map { it.typeDeclaration }
				.forEach {
					append(it.toIr() + ln)
				}
			append(ln)
			
			classes
				.asSequence()
				.sortedBy { it.identity }
				.map { it.virtualTable.typeDeclaration }
				.forEach {
					append(it.toIr() + ln)
				}
			append(ln)
			
			classes
				.asSequence()
				.sortedBy { it.identity }
				.flatMap { it.virtualTable.tables.asSequence() }
				.forEach {
					append(it.toIr() + ln)
				}
			append(ln)
			
			classes
				.asSequence()
				.sortedBy { it.identity }
				.map { it.virtualTable.loaderFunction }
				.forEach {
					append(it.toIr() + ln)
				}
			append(ln)
			
			classes
				.asSequence()
				.sortedBy { it.identity }
				.map { it.typeObject }
				.forEach {
					append(it.toIr() + ln)
				}
			append(ln)
			
			objectConstants.forEach {
				append(it.toIr() + ln)
			}
			append(ln)
			
			functions.forEach {
				append(it.toIr() + ln + ln)
			}
			append(ln)
			
		}.toString()
}