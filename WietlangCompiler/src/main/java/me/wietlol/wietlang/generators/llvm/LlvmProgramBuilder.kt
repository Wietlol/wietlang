package me.wietlol.wietlang.generators.llvm

import me.wietlol.common.prepend
import me.wietlol.common.recursiveMapPreOrder
import me.wietlol.wietlang.generators.llvm.data.LlvmBitCastExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmClass
import me.wietlol.wietlang.generators.llvm.data.LlvmFunction
import me.wietlol.wietlang.generators.llvm.data.LlvmGlobalObjectDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmImmutableGlobalObjectDeclaration
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerLiteralExpression
import me.wietlol.wietlang.generators.llvm.data.LlvmIntegerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmNamedTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmPointerTypeReference
import me.wietlol.wietlang.generators.llvm.data.LlvmProgram
import me.wietlol.wietlang.generators.llvm.data.LlvmVariable
import me.wietlol.wietlang.generators.llvm.data.LlvmVariableExpression
import me.wietlol.wietlang.pid.data.models.PidAnnotatable
import me.wietlol.wietlang.pid.data.models.PidAnnotation
import me.wietlol.wietlang.pid.data.models.PidAnnotationValue
import me.wietlol.wietlang.pid.data.models.PidClass
import me.wietlol.wietlang.pid.data.models.PidClassMethod
import me.wietlol.wietlang.pid.data.models.PidComponent
import me.wietlol.wietlang.pid.data.models.PidContainer
import me.wietlol.wietlang.pid.data.models.PidFunction
import me.wietlol.wietlang.pid.data.models.PidPackage
import me.wietlol.wietlang.pid.data.models.PidProgram
import me.wietlol.wietlang.pid.data.models.PidStringLiteralExpression
import me.wietlol.wietlang.pid.data.models.PidType
import me.wietlol.wietlang.pid.linker.PidProgramLinker
import java.util.*

internal class LlvmProgramBuilder(
	val program: PidProgram,
	linker: PidProgramLinker
) : PidProgramLinker by linker
{
	private val types: MutableMap<PidClass, LlvmClass> = IdentityHashMap()
	private val functionMap: MutableMap<PidFunction, LlvmFunction> = IdentityHashMap()
	private val methodMap: MutableMap<PidClassMethod, LlvmFunction> = IdentityHashMap()
	private val classBuilder = LlvmClassBuilder()
	private val expressionBuilder = LlvmExpressionBuilder(this, linker)
	
	fun build(): LlvmProgram =
		buildInternal()
			.run {
				LlvmProgram(
					expressionBuilder.stringConstants.values.toList(),
					expressionBuilder.stringGlobalObjects.values.toList(),
					types.values.toList(),
					functionMap
						.values
						.plus(methodMap.values)
						.plus(expressionBuilder.functions)
						.plus(expressionBuilder.main)
				)
			}
	
	private fun buildInternal()
	{
		fun doNothing()
		{
		}
		program
			.rootPackage
			.let { sequenceOf(it) }
			.recursiveMapPreOrder<PidComponent> {
				if (it is PidContainer)
					it.members.asSequence().filterIsInstance<PidComponent>()
				else
					emptySequence()
			}
			.forEach {
				when (it)
				{
					is PidAnnotation -> doNothing()
					is PidAnnotationValue -> doNothing()
					is PidClass -> produceTypeDeclaration(it)
					is PidClassMethod -> produceMethod(it)
					is PidFunction -> produceFunction(it)
					is PidPackage -> doNothing()
					else -> TODO("wot?")
				}
			}
	}
	
	internal val classDeclaration = classBuilder.`class`
	internal val integerDeclaration = classBuilder.build(
		"NativeInteger",
		listOf(
			LlvmIntegerTypeReference(32)
		)
	)
	
	internal val stringDeclaration = classBuilder.build(
		"NativeString",
		listOf(
			LlvmIntegerTypeReference(32),
			LlvmPointerTypeReference(LlvmIntegerTypeReference(8))
		)
	)
	
	private fun PidClass.convertClass(): LlvmClass
	{
		val nativeName = getNativeName()
		if (nativeName != null)
			return convertNativeClass(nativeName)
		
		return classBuilder.build(
			name,
			listOf() // todo list all fields
		)
	}
	
	private fun PidClass.convertNativeClass(nativeName: String): LlvmClass =
		when (nativeName)
		{
			"system:text:string" -> stringDeclaration
			"system:math:integer" -> integerDeclaration
			"system:lang:class" -> classDeclaration
			else -> TODO("wot?")
		}
	
	internal fun produceTypeDeclaration(type: PidType): LlvmClass =
		when (type)
		{
			is PidClass -> produceTypeDeclaration(type)
			else -> TODO()
		}
	
	private fun produceTypeDeclaration(type: PidClass): LlvmClass =
		types.computeIfAbsent(type) {
			it.convertClass()
		}
			.also { typeClass ->
				typeClass.provideVirtualTable(
					this,
					sequenceOf(this)
						.flatMap { type.members.asSequence() }
						.filterIsInstance<PidClassMethod>()
						.map { LlvmPointerTypeReference(expressionBuilder.getSignature(it)) },
					type
						.supertypes
						.asSequence()
						.map { it.referencedType as PidClass }
						.map { type to produceTypeDeclaration(it) }
						.let { seq ->
							if (!type.isAbstract())
								seq.plus(type to typeClass)
							else
								seq
						}
						.map { it.second to generateVirtualTable(it.first, it.second) }
				)
			}

	private fun generateVirtualTable(`class`: PidClass, type: LlvmClass): LlvmGlobalObjectDeclaration =
		LlvmImmutableGlobalObjectDeclaration(
			LlvmVariable("${`class`.name}_${type.typeDeclaration.name}_vTable_data", true, LlvmPointerTypeReference(LlvmNamedTypeReference(type.virtualTable.typeDeclaration))),
			`class`
				.members
				.asSequence()
				.filterIsInstance<PidClassMethod>()
				.filter { it.isAbstract().not() }
				.map { produceMethod(it) }
				.filterNotNull()
				.map { it.variable }
				.map { LlvmVariableExpression(it) }
				.mapIndexed { index, it ->
					if (it.resultType != type.virtualTable.typeDeclaration.fields[index + 1])
						LlvmBitCastExpression(
							it,
							type.virtualTable.typeDeclaration.fields[index + 1],
							false
						)
					else
						it
				}
				.prepend(LlvmIntegerLiteralExpression(LlvmIntegerTypeReference(8), 0))
				.toList()
		)
	
	internal fun produceFunction(pidFunction: PidFunction): LlvmFunction =
		functionMap[pidFunction]
			?: expressionBuilder.convertFunction(pidFunction)
				.also { functionMap[pidFunction] = it }
	
	internal fun produceMethod(pidFunction: PidClassMethod): LlvmFunction? =
		methodMap[pidFunction]
			?: expressionBuilder.convertMethod(pidFunction)
				?.also { methodMap[pidFunction] = it }
	
	private fun PidAnnotatable.getNativeName(): String? =
		annotations
			.singleOrNull { it.annotation.name == "Native" } // todo get direct reference to native annotation from metadata rather than based on name
			?.arguments
			?.first()
			?.let { it.expression as PidStringLiteralExpression }
			?.let { it.value }
}