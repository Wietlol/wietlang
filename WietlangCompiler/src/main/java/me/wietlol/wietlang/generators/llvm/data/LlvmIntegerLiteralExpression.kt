package me.wietlol.wietlang.generators.llvm.data

data class LlvmIntegerLiteralExpression(
	val type: LlvmIntegerTypeReference,
	val value: Int
) : LlvmExpression<LlvmIntegerTypeReference>
{
	override val resultType: LlvmIntegerTypeReference
		get() = type
	
	override fun toIr(): String =
		value.toString()
}