package me.wietlol.wietlang.generators.llvm.data

object LlvmReturnVoidExpression : LlvmExpression<LlvmVoidTypeReference>
{
	override val resultType: LlvmVoidTypeReference
		get() = LlvmVoidTypeReference
	
	override fun toIr(): String =
		"ret void"
}