package me.wietlol.wietlang.generators.llvm.data

data class LlvmGetElementPointerExpression<S : LlvmTypeReference, T : LlvmTypeReference>(
	val sourceExpression: LlvmExpression<LlvmPointerTypeReference<S>>,
	val indices: List<Int>,
	val targetType: Class<T>,
	val isStatement: Boolean = true
) : LlvmExpression<LlvmPointerTypeReference<T>>
{
	init
	{
		if (indices.isEmpty())
			TODO("stop being an idiot")
	}
	
	override val resultType: LlvmPointerTypeReference<T> =
		LlvmPointerTypeReference(targetType.cast(evaluateResultType(sourceExpression.resultType, indices)))
	
	private fun evaluateResultType(target: LlvmTypeReference, indices: List<Int>): LlvmTypeReference
	{
		if (indices.isEmpty())
			return target
		
		return when (target)
		{
			is LlvmPointerTypeReference<*> -> evaluateResultType(target.reference, indices.drop(1))
			is LlvmNamedTypeReference<*> -> evaluateResultType(target.referencedType.fields[indices.first()], indices.drop(1))
			is LlvmArrayTypeReference<*> -> evaluateResultType(target.type, indices.drop(1))
			else -> TODO("not implemented")
		}
	}
	
	override fun toIr(): String =
		if (isStatement)
			"getelementptr ${sourceExpression.resultType.reference.toIr()}, ${sourceExpression.resultType.toIr()} ${sourceExpression.toIr()}, ${indices.joinToString { "i32 $it" }}"
		else
			"getelementptr (${sourceExpression.resultType.reference.toIr()}, ${sourceExpression.resultType.toIr()} ${sourceExpression.toIr()}, ${indices.joinToString { "i32 $it" }})"
}