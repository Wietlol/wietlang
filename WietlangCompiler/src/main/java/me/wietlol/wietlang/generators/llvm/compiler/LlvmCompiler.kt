package me.wietlol.wietlang.generators.llvm.compiler

import me.wietlol.wietlang.generators.llvm.data.LlvmProgram
import java.nio.file.Files

object LlvmCompiler
{
	fun compile(program: LlvmProgram): ByteArray
	{
		val tempDirectory = Files.createTempDirectory("llvm-compiler").toFile()
		
		try
		{
			tempDirectory
				.resolve("program.ll")
				.also { it.createNewFile() }
				.also { it.writeText(program.toIr()) }
			
			val processBuilder = ProcessBuilder()
				.command(
					"cmd.exe",
					"/c",
					"clang.exe program.ll"
				)
				.directory(tempDirectory)
				.inheritIO()
			
			val process = processBuilder.start()
			process.waitFor()
				.also {
					if (it != 0)
						throw Exception("Program failed with exit code $it.")
				}
			
			return tempDirectory
				.resolve("a.exe")
				.let { it.readBytes() }
		}
		finally
		{
			tempDirectory.delete()
		}
	}
}