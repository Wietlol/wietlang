package me.wietlol.wietlang.generators.llvm.data

data class LlvmBitCastExpression<T : LlvmTypeReference>(
	val sourceExpression: LlvmExpression<*>,
	val targetType: T,
	val isStatement: Boolean = true
) : LlvmExpression<T>
{
	override val resultType: T
		get() = targetType
	
	override fun toIr(): String =
		if (isStatement)
			"bitcast ${sourceExpression.resultType.toIr()} ${sourceExpression.toIr()} to ${targetType.toIr()}"
		else
			"bitcast (${sourceExpression.resultType.toIr()} ${sourceExpression.toIr()} to ${targetType.toIr()})"
}