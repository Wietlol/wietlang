package me.wietlol.wietlang.generators.llvm.data

data class LlvmAllocateExpression<T : LlvmTypeReference>(
	val type: T
) : LlvmExpression<LlvmPointerTypeReference<T>>
{
	override val resultType: LlvmPointerTypeReference<T>
		get() = LlvmPointerTypeReference(type)
	
	override fun toIr(): String =
		"alloca ${type.toIr()}"
}