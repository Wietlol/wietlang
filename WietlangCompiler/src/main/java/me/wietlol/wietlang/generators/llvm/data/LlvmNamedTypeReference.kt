package me.wietlol.wietlang.generators.llvm.data

data class LlvmNamedTypeReference<out T : LlvmTypeDeclaration>(
	val referencedType: T
) : LlvmTypeReference
{
	override fun toIr(): String =
		"%${referencedType.name}"
}