package me.wietlol.wietlang.generators.llvm.data

data class LlvmInsertValueExpression<T : LlvmTypeReference>(
	val source: LlvmExpression<T>,
	val value: LlvmExpression<*>,
	val index: Int
) : LlvmExpression<T>
{
	override val resultType: T
		get() = source.resultType
	
	override fun toIr(): String =
		"insertvalue ${source.resultType.toIr()} ${source.toIr()}, ${value.resultType.toIr()} ${value.toIr()}, $index"
}