package me.wietlol.wietlang.generators.llvm.data

data class LlvmStructDeclaration(
	override val name: String,
	override val fields: List<LlvmTypeReference>
) : LlvmTypeDeclaration
{
	override fun toIr(): String =
		"%$name = type { ${fields.joinToString { it.toIr() }} }"
}