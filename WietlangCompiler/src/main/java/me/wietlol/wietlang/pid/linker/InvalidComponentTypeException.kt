package me.wietlol.wietlang.pid.linker

class InvalidComponentTypeException : Exception
{
	val type: String
	
	constructor(type: String) : super()
	{
		this.type = type
	}
	
	constructor(message: String, type: String) : super(message)
	{
		this.type = type
	}
	
	constructor(message: String, cause: Throwable, type: String) : super(message, cause)
	{
		this.type = type
	}
	
	constructor(cause: Throwable, type: String) : super(cause)
	{
		this.type = type
	}
	
	constructor(message: String, cause: Throwable, enableSuppression: Boolean, writableStackTrace: Boolean, type: String) : super(message, cause, enableSuppression, writableStackTrace)
	{
		this.type = type
	}
}