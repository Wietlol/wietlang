package me.wietlol.wietlang.pid.linker

class MissingNativeException : Exception
{
	val key: String
	
	constructor(key: String) : super()
	{
		this.key = key
	}
	
	constructor(message: String?, key: String) : super(message)
	{
		this.key = key
	}
	
	constructor(message: String, cause: Throwable, key: String) : super(message, cause)
	{
		this.key = key
	}
	
	constructor(cause: Throwable, key: String) : super(cause)
	{
		this.key = key
	}
	
	constructor(message: String, cause: Throwable, enableSuppression: Boolean, writableStackTrace: Boolean, key: String) : super(message, cause, enableSuppression, writableStackTrace)
	{
		this.key = key
	}
}