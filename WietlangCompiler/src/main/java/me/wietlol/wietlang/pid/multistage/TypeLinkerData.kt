package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.ast.data.models.AstImport
import me.wietlol.wietlang.compiler.CompilationException
import me.wietlol.wietlang.pid.data.models.PidMutableComponent
import me.wietlol.wietlang.pid.data.models.PidMutableFirstClassComponent
import me.wietlol.wietlang.pid.data.models.PidMutableNamedComponent
import me.wietlol.wietlang.pid.data.models.PidMutableType
import me.wietlol.wietlang.pid.data.models.PidPackage
import java.util.*
import kotlin.collections.HashMap

internal class TypeLinkerData(
	rootPackage: PidPackage,
	mapper: PidToAstMapper,
	private val resolver: TypeLinkerResolver
) : PidToAstMapper by mapper
{
	internal val rootPackage: PidPackage = rootPackage
	internal var currentPackage: PidPackage = rootPackage
	//	internal val variableStack: Deque<Map<String, PidVariable>> = ArrayDeque()
	internal val componentStack: Deque<MutableMap<String, PidMutableNamedComponent>> = ArrayDeque()
	internal val currentTypeStack: Deque<PidMutableType> = ArrayDeque()
	
	fun setCurrentPackage(`package`: PidPackage)
	{
		currentPackage = `package`
	}
	
	fun pushComponentStack()
	{
		componentStack.push(HashMap())
	}
	
	fun popComponentStack()
	{
		componentStack.pop()
	}

//	fun pushVariableStack()
//	{
//		variableStack.push(HashMap())
//	}
	
	fun pushCurrentTypeStack(pidType: PidMutableType)
	{
		currentTypeStack.push(pidType)
		addToComponentStack(pidType)
		processImports(pidType)
	}
	
	fun popCurrentTypeStack()
	{
		currentTypeStack.pop()
	}
	
	fun addToComponentStack(component: PidMutableNamedComponent, name: String = component.name)
	{
		componentStack.peekLast()[name] = component
	}
	
	fun processImports(pidComponent: PidMutableComponent)
	{
		if (pidComponent is PidMutableFirstClassComponent)
		{
			val astComponent = pidComponent.astComponent
			val imports = astComponent.imports
			imports.forEach {
				processImport(it)
			}
		}
	}
	
	private fun processImport(astImport: AstImport)
	{
		val component: PidMutableNamedComponent = resolver.findComponent(astImport.path, this)
			?: throw CompilationException("Unresolved reference: ${astImport.path.parts.joinToString(".")}.", astImport.tokens.first())
		
		// non-asterisk import:
		addToComponentStack(component, astImport.alias ?: component.name)


//			if (jImport.isStatic())
//			{
//				if (jImport.isAsterisk())
//				{
//					val path = jImport.getPath().replace("\\.\\*", "")
//
//					val typeDeclaration = sourceCode.findType(path)
//					if (typeDeclaration != null)
//					{
//						// add all static variables to variable stack
//						addToVariableStack(typeDeclaration!!
//							.getMembers()
//							.stream()
//							.filter({ jBodyDeclaration ->
//								if (jBodyDeclaration is JFieldDeclaration)
//									return@typeDeclaration
//										.getMembers()
//										.stream()
//										.filter(jBodyDeclaration as JFieldDeclaration).isStatic()
//								jBodyDeclaration is JEnumConstantDeclaration
//							})
//							.map({ jBodyDeclaration -> jBodyDeclaration as JVariable })
//							.collect(Collectors.toList<T>()))
//
//						// add all static methods to variable stack
//						// TODO
//					}
//					else
//						throw JCompilingException("Cannot import type of \"null\".")
//				}
//				else
//				{
//					val fullPath = jImport.getPath()
//					val path = fullPath.replace("\\.[^.]+$".toRegex(), "")
//					val name = fullPath.substring(fullPath.lastIndexOf(".") + 1, fullPath.length)
//
//					val typeDeclaration = sourceCode.findType(path)
//					if (typeDeclaration != null)
//					{
//						addToVariableStack(typeDeclaration!!
//							.getMembers()
//							.stream()
//							.filter({ jBodyDeclaration ->
//								if (jBodyDeclaration is JFieldDeclaration)
//									return@typeDeclaration
//										.getMembers()
//										.stream()
//										.filter(jBodyDeclaration as JFieldDeclaration).isStatic()
//								jBodyDeclaration is JEnumConstantDeclaration
//							})
//							.map({ jBodyDeclaration -> jBodyDeclaration as JVariable })
//							.filter({ jVariable -> jVariable.getName().equals(name) })
//							.collect(Collectors.toList<T>()))
//					}
//					else
//						throw JCompilingException("Cannot import type of \"null\".")
//				}
//			}
//			else if (jImport.isAsterisk())
//			{
//				val container = findPackage(jImport.getPath())
//
//				if (container != null)
//					container!!.getTypes().forEach(???({ this.addToComponentStack() }))
//				else
//				throw JCompilingException("Cannot resolve import $jImport")
//			}
//			else
//			{
//				addToComponentStack(findType(jImport.getPath()))
//			}
	}

//	fun pushCurrentTypeStack(jType: JTypeDeclaration)
//	{
//		currentTypeStack.add(jType)
//		addToComponentStack(jType)
//
//		jType.getImports().forEach { jImport ->
//		}
//
//	    member types
//		jType.getTypes().forEach(???({ this.addToComponentStack() }))
//	}
}