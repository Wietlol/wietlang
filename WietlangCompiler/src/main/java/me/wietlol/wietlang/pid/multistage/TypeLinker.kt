package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.compiler.CompilationException
import me.wietlol.wietlang.pid.data.models.PidAnnotation
import me.wietlol.wietlang.pid.data.models.PidComponent
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotation
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationExpression
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationMember
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationValue
import me.wietlol.wietlang.pid.data.models.PidMutableClass
import me.wietlol.wietlang.pid.data.models.PidMutableClassMember
import me.wietlol.wietlang.pid.data.models.PidMutableClassMethod
import me.wietlol.wietlang.pid.data.models.PidMutableCompoundTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableDirectTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableFirstClassComponent
import me.wietlol.wietlang.pid.data.models.PidMutableFunction
import me.wietlol.wietlang.pid.data.models.PidMutablePackage
import me.wietlol.wietlang.pid.data.models.PidMutableParameter
import me.wietlol.wietlang.pid.data.models.PidMutableProgram
import me.wietlol.wietlang.pid.data.models.PidMutableTypeReference
import me.wietlol.wietlang.pid.data.models.PidPackage
import me.wietlol.wietlang.pid.data.models.PidType
import me.wietlol.wietlang.pid.linker.PidProgramLinker
import me.wietlol.wietlang.utility.invalidType

internal class TypeLinker(
	private val pidProgram: PidMutableProgram,
	mapper: PidToAstMapper,
	linker: PidProgramLinker
) : PidToAstMapper by mapper, PidProgramLinker by linker
{
	private val data: TypeLinkerData
	private val resolver: TypeLinkerResolver = TypeLinkerResolver(linker)
	
	init
	{
		data = TypeLinkerData(
			pidProgram.rootPackage,
			this,
			resolver
		)
	}
	
	fun process()
	{
		pidProgram.rootPackage.process()
	}
	
	private fun PidMutableFirstClassComponent.process()
	{
		when (this)
		{
			is PidMutablePackage -> process()
			is PidMutableClass -> process()
			is PidMutableFunction -> process()
			is PidMutableAnnotation -> process()
			else -> throw IllegalArgumentException(invalidType("package member", type, this))
		}
	}
	
	private fun PidMutableClassMember.process()
	{
		when (this)
		{
			is PidMutableClassMethod -> process()
			else -> throw IllegalArgumentException(invalidType("class member", type, this))
		}
	}
	
	private fun PidMutablePackage.process()
	{
		members
			.filterIsInstance<PidMutableFirstClassComponent>()
			.forEach { it.process() }
	}
	
	private fun PidMutableClass.process()
	{
		data.setCurrentPackage(parentPackage())
		
		data.pushComponentStack()
//		data.pushVariableStack()
		data.pushCurrentTypeStack(this)
//		data.addToComponentStack(typeContainer.typeParameters) // todo type arguments
		
		supertypes.forEach { it.process() }
//		pidClass.typeParameters.forEach { process(it) } // todo type arguments
		
		members
			.filterIsInstance<PidMutableClassMember>()
			.forEach { it.process() }
		
		annotations.forEach { it.process() }
		
		data.popCurrentTypeStack()
//		data.popVariableStack()
		data.popComponentStack()
	}
	
	private fun PidMutableFunction.process()
	{
		data.setCurrentPackage(parentPackage())
		// todo if extension method, push current type stack
		// todo add type arguments
		
		data.pushComponentStack()
		data.processImports(this)
		
		parameters.forEach { it.process() }
		returnType.process()
		
		annotations.forEach { it.process() }
		
		data.popComponentStack()
	}
	
	private fun PidMutableAnnotation.process()
	{
		data.pushComponentStack()
		data.pushCurrentTypeStack(this)
		
		members.forEach { it.process() }
		
		annotations.forEach { it.process() }
		
		data.popCurrentTypeStack()
		data.popComponentStack()
	}
	
	private fun PidMutableAnnotationMember.process()
	{
		when (this)
		{
			is PidMutableAnnotationValue -> process()
			else -> throw IllegalArgumentException(invalidType("annotation member", type, this))
		}
	}
	
	private fun PidMutableAnnotationValue.process()
	{
		upperBound.process()
	}
	
	private fun PidMutableParameter.process()
	{
		upperBound.process()
	}
	
	private fun PidMutableClassMethod.process()
	{
		data.pushComponentStack()
		// todo add type arguments
		
		parameters.forEach { it.upperBound.process() }
		returnType.process()
		
		annotations.forEach { it.process() }
		
		data.popComponentStack()
	}
	
	private fun PidMutableTypeReference.process()
	{
		when (this)
		{
			is PidMutableDirectTypeReference -> process()
			is PidMutableCompoundTypeReference -> process()
			else -> throw IllegalArgumentException(invalidType("type reference", type, this))
		}
	}
	
	private fun PidMutableDirectTypeReference.process()
	{
		val path = astComponent.namePath.parts
		val type: PidType = resolver.findComponent(path, data)
			?: throw CompilationException("Unresolved reference: ${astComponent.namePath.parts.joinToString(".")}", tokens.first())
		
		referencedTypeId = type.id
		// todo type arguments
	}
	
	private fun PidMutableCompoundTypeReference.process()
	{
		types.forEach { it.process() }
	}
	
	private fun PidMutableAnnotationExpression.process()
	{
		// annotations have simple processing, no overloads, no nothing
		val path = astComponent.namePath
		val annotation: PidAnnotation = resolver.findComponent(path, data)
			?: throw CompilationException("Unresolved reference: ${astComponent.namePath.parts.joinToString(".")}", tokens.first())
		
		annotationId = annotation.id
	}
	
	private fun PidComponent.parentPackage(): PidPackage =
		parentComponent!!
			.let {
				if (it is PidPackage)
					it
				else
					it.parentPackage()
			}

//	@Nullable
//	fun accept(typeContainer: JInterfaceDeclaration): Void?
//	{
//		val compilerData = JSourceCode.instance.getCompilerData()
//		compilerData.pushComponentStack()
//		compilerData.pushVariableStack()
//		compilerData.pushCurrentTypeStack(typeContainer)
//		compilerData.addToComponentStack(typeContainer.getTypeParameters())
//
//		typeContainer.getExtendedTypes().forEach(???({ this.visitType() }))
//		typeContainer.getTypeParameters().forEach(???({ this.visitDeclaration() }))
//
//		typeContainer.getTypes().forEach(???({ this.visitTypeContainer() }))
//
//		typeContainer.getMembers().forEach(???({ this.visitDeclaration() }))
//
//		compilerData.popCurrentTypeStack()
//		compilerData.popVariableStack()
//		compilerData.popTypeStack()
//		return null
//	}
//
//	@Nullable
//	fun accept(typeContainer: JClassDeclaration): Void?
//	{
//		val compilerData = JSourceCode.instance.getCompilerData()
//		compilerData.pushComponentStack()
//		compilerData.pushVariableStack()
//		compilerData.pushCurrentTypeStack(typeContainer)
//		compilerData.addToComponentStack(typeContainer.getTypeParameters())
//
//		if (typeContainer.getExtendedType() != null)
//			visitType(typeContainer.getExtendedType())
//		typeContainer.getImplementedTypes().forEach(???({ this.visitType() }))
//		typeContainer.getTypeParameters().forEach(???({ this.visitDeclaration() }))
//
//		typeContainer.getTypes().forEach(???({ this.visitTypeContainer() }))
//
//		typeContainer.getMembers().forEach(???({ this.visitDeclaration() }))
//
//		compilerData.popCurrentTypeStack()
//		compilerData.popVariableStack()
//		compilerData.popTypeStack()
//		return null
//	}
//
//	@Nullable
//	fun accept(typeContainer: JGenericDeclaration): Void?
//	{
//		JSourceCode.instance.getCompilerData().addToComponentStack(typeContainer)
//		return null
//	}
//
//	@Nullable
//	fun accept(typeContainer: JEnumDeclaration): Void?
//	{
//		val compilerData = JSourceCode.instance.getCompilerData()
//		compilerData.pushComponentStack()
//		compilerData.pushVariableStack()
//		compilerData.pushCurrentTypeStack(typeContainer)
//
//		typeContainer.getImplementedTypes().forEach(???({ this.visitType() }))
//
//		typeContainer.getTypes().forEach(???({ this.visitTypeContainer() }))
//
//		typeContainer.getMembers().forEach(???({ this.visitDeclaration() }))
//
//		compilerData.popCurrentTypeStack()
//		compilerData.popVariableStack()
//		compilerData.popTypeStack()
//		return null
//	}
//
//	@Nullable
//	fun accept(typeContainer: JAnnotationDeclaration): Void?
//	{
//		val compilerData = JSourceCode.instance.getCompilerData()
//		compilerData.pushComponentStack()
//		compilerData.pushVariableStack()
//		compilerData.pushCurrentTypeStack(typeContainer)
//
//		typeContainer.getMembers().forEach(???({ this.visitDeclaration() }))
//
//		compilerData.popCurrentTypeStack()
//		compilerData.popTypeStack()
//		compilerData.popVariableStack()
//		return null
//	}
//
//	@Nullable
//	fun accept(type: JClassOrInterfaceType): Void?
//	{
//		val typeDeclaration = JSourceCode.instance.getCompilerData().findType(type.getName())
//			?: throw CompilerException("TypeDeclaration " + type.getName() + " cannot be found.")
//
//		type.setTypeDeclaration(typeDeclaration)
//		type.getTypeArguments().forEach(???({ this.visitType() }))
//		return null
//	}
//
//	@Nullable
//	fun accept(type: JArrayType): Void?
//	{
//		visitType(type.getComponentType())
//		return null
//	}
//
//	@Nullable
//	fun accept(type: JPrimitiveType): Void?
//	{
//		visitType(type.getWrapperType())
//		return null
//	}
//
//	@Nullable
//	@Contract(pure = true)
//	fun accept(type: JNullType): Void?
//	{
//		/* nothing to do */
//		return null
//	}
//
//	@Nullable
//	fun accept(type: JWildcardType): Void?
//	{
//		if (type.getExtendedType() != null)
//			visitType(type.getExtendedType())
//		if (type.getSuperType() != null)
//			visitType(type.getSuperType())
//		return null
//	}
//
//	@Nullable
//	@Contract(pure = true)
//	fun accept(type: JVoidType): Void?
//	{
//		/* nothing to do */
//		return null
//	}
//
//	@Nullable
//	fun accept(declaration: JFieldDeclaration): Void?
//	{
//		visitType(declaration.getType())
//		return null
//	}
//
//	@Nullable
//	fun accept(declaration: JMethodDeclaration): Void?
//	{}
//
//	@Nullable
//	fun accept(declaration: JConstructorDeclaration): Void?
//	{
//		declaration.getParameters().forEach(???({ this.visitVariable() }))
//		declaration.getTypeParameters().forEach(???({ this.visitDeclaration() }))
//		declaration.getThrownExceptions().forEach(???({ this.visitType() }))
//		return null
//	}
//
//	@Nullable
//	@Contract(pure = true)
//	fun accept(declaration: JInitializerDeclaration): Void?
//	{
//		/* nothing to do */
//		return null
//	}
//
//	@Nullable
//	fun accept(declaration: JAnnotationMemberDeclaration): Void?
//	{
//		visitType(declaration.getType())
//		return null
//	}
//
//	@Nullable
//	fun accept(declaration: JEnumConstantDeclaration): Void?
//	{
//		visitType(declaration.getType())
//		return null
//	}
//
//	@Nullable
//	fun accept(variable: JLocalVariable): Void?
//	{
//		visitType(variable.getType())
//		return null
//	}
//
//	@Nullable
//	fun accept(variable: JParameter): Void?
//	{
//		visitType(variable.getType())
//		return null
//	}
}