package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.ast.data.models.AstAnnotationExpression
import me.wietlol.wietlang.ast.data.models.AstArgumentExpression
import me.wietlol.wietlang.ast.data.models.AstFirstClassComponent
import me.wietlol.wietlang.ast.data.models.AstFunctionCallExpression
import me.wietlol.wietlang.ast.data.models.AstMethodCallExpression
import me.wietlol.wietlang.ast.data.models.AstNameExpression
import me.wietlol.wietlang.ast.data.models.AstNamedTypeReference
import me.wietlol.wietlang.ast.data.models.AstNode
import me.wietlol.wietlang.pid.data.models.PidAnnotationExpression
import me.wietlol.wietlang.pid.data.models.PidDirectTypeReference
import me.wietlol.wietlang.pid.data.models.PidFirstClassComponent
import me.wietlol.wietlang.pid.data.models.PidMutableArgumentExpression
import me.wietlol.wietlang.pid.data.models.PidMutableFunctionCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutableMethodCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutableVariableLoadExpression
import me.wietlol.wietlang.pid.data.models.PidNode

internal interface PidToAstMapper
{
	val map: MutableMap<PidNode, AstNode>
	
	val PidFirstClassComponent.astComponent: AstFirstClassComponent
		get() = map[this] as AstFirstClassComponent
	
	val PidDirectTypeReference.astComponent: AstNamedTypeReference
		get() = map[this] as AstNamedTypeReference
	
	val PidAnnotationExpression.astComponent: AstAnnotationExpression
		get() = map[this] as AstAnnotationExpression
	
	val PidMutableArgumentExpression.astComponent: AstArgumentExpression
		get() = map[this] as AstArgumentExpression
	
	val PidMutableFunctionCallExpression.astComponent: AstFunctionCallExpression
		get() = map[this] as AstFunctionCallExpression
	
	val PidMutableMethodCallExpression.astComponent: AstMethodCallExpression
		get() = map[this] as AstMethodCallExpression
	
	val PidMutableVariableLoadExpression.astComponent: AstNameExpression
		get() = map[this] as AstNameExpression
}