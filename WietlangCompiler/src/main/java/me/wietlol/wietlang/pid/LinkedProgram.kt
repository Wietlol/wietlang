package me.wietlol.wietlang.pid

import me.wietlol.wietlang.pid.data.models.PidProgram
import me.wietlol.wietlang.pid.linker.PidProgramLinker

data class LinkedProgram(
	val program: PidProgram,
	val linker: PidProgramLinker
)