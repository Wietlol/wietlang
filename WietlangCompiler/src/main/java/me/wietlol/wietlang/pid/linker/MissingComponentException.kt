package me.wietlol.wietlang.pid.linker

import java.util.*

class MissingComponentException : Exception
{
	val id: UUID
	
	constructor(id: UUID) : super()
	{
		this.id = id
	}
	
	constructor(message: String, id: UUID) : super(message)
	{
		this.id = id
	}
	
	constructor(message: String, cause: Throwable, id: UUID) : super(message, cause)
	{
		this.id = id
	}
	
	constructor(cause: Throwable, id: UUID) : super(cause)
	{
		this.id = id
	}
	
	constructor(message: String, cause: Throwable, enableSuppression: Boolean, writableStackTrace: Boolean, id: UUID) : super(message, cause, enableSuppression, writableStackTrace)
	{
		this.id = id
	}
}