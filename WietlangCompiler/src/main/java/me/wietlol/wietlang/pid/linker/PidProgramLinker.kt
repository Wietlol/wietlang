package me.wietlol.wietlang.pid.linker

import me.wietlol.wietlang.pid.data.models.PidAnnotation
import me.wietlol.wietlang.pid.data.models.PidAnnotationExpression
import me.wietlol.wietlang.pid.data.models.PidArgumentExpression
import me.wietlol.wietlang.pid.data.models.PidArgumentTarget
import me.wietlol.wietlang.pid.data.models.PidClass
import me.wietlol.wietlang.pid.data.models.PidClassMethod
import me.wietlol.wietlang.pid.data.models.PidComponent
import me.wietlol.wietlang.pid.data.models.PidDirectTypeReference
import me.wietlol.wietlang.pid.data.models.PidExpression
import me.wietlol.wietlang.pid.data.models.PidFunction
import me.wietlol.wietlang.pid.data.models.PidFunctionCallExpression
import me.wietlol.wietlang.pid.data.models.PidImport
import me.wietlol.wietlang.pid.data.models.PidMethodCallExpression
import me.wietlol.wietlang.pid.data.models.PidNode
import me.wietlol.wietlang.pid.data.models.PidStringLiteralExpression
import me.wietlol.wietlang.pid.data.models.PidType
import me.wietlol.wietlang.pid.data.models.PidTypeReference
import me.wietlol.wietlang.pid.data.models.PidVariable
import me.wietlol.wietlang.pid.data.models.PidVariableLoadExpression
import java.util.*

interface PidProgramLinker
{
	val componentRegistry: Map<UUID, PidComponent>
	val nativeRegistry: Map<String, PidComponent>
	val treeRegistry: Map<PidNode, PidComponent>
	
	val PidComponent.parentComponent: PidComponent?
		get() = treeRegistry[this]
	
	val PidImport.component: PidComponent
		get() = findComponent(componentId, "component")
	
	val PidAnnotationExpression.annotation: PidAnnotation
		get() = findComponent(annotationId, "annotation")
	
	val PidArgumentExpression.target: PidArgumentTarget
		get() = findComponent(targetId, "argument target")
	
	val PidFunctionCallExpression.function: PidFunction
		get() = findComponent(functionId, "function")
	
	val PidMethodCallExpression.method: PidClassMethod
		get() = findComponent(methodId, "method")
	
	val PidVariableLoadExpression.variable: PidVariable
		get() = findComponent(variableId, "variable")
	
	val PidTypeReference.referencedType: PidType
		get() = when (this)
		{
			is PidDirectTypeReference -> referencedType
			else -> throw IllegalArgumentException("Invalid type for type reference of class: ${javaClass.name}.")
		}
	
	val PidDirectTypeReference.referencedType: PidType
		get() = findComponent(referencedTypeId, "type")
	
	val PidExpression.resultType: PidTypeReference
		get() = when (this)
		{
			is PidFunctionCallExpression -> function.returnType
			is PidMethodCallExpression -> method.returnType
			is PidStringLiteralExpression -> findNativeComponent<PidClass>("system:text:string", "class").asReference()
			is PidVariableLoadExpression -> variable.upperBound
			else -> throw IllegalArgumentException("Invalid type for expression of class: ${javaClass.name}.")
		}
	
	fun PidType.asReference(): PidDirectTypeReference =
		PidDirectTypeReference.of(id, emptyList())
	
	fun PidClassMethod.isAbstract(): Boolean =
		body == null
			&& annotations.none { it.annotation.name == "Native" }
	
	fun PidFunction.isAbstract(): Boolean =
		body == null
			&& annotations.none { it.annotation.name == "Native" }
	
	fun PidClass.isAbstract(): Boolean =
		members
			.asSequence()
			.filterIsInstance<PidClassMethod>()
			.any { it.isAbstract() }
	
	companion object
	{
		private inline fun <reified T : PidComponent> PidProgramLinker.findComponent(id: UUID, type: String): T =
			(componentRegistry[id] ?: throw MissingComponentException("Missing component of id '$id'.", id))
				.let { it as? T ?: throw InvalidComponentTypeException("Expected $type, but found ${it.type}.", it.type) }
		
		private inline fun <reified T : PidComponent> PidProgramLinker.findNativeComponent(key: String, type: String): T =
			(nativeRegistry[key] ?: throw MissingNativeException("Missing native component of key '$key'.", key))
				.let { it as? T ?: throw InvalidNativeTypeException("Expected $type, but found ${it.type}.", it.type) }
	}
}