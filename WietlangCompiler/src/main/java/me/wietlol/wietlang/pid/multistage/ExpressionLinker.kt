package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.compiler.CompilationException
import me.wietlol.wietlang.pid.data.models.PidComponent
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotation
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationExpression
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationMember
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationValue
import me.wietlol.wietlang.pid.data.models.PidMutableArgumentExpression
import me.wietlol.wietlang.pid.data.models.PidMutableClass
import me.wietlol.wietlang.pid.data.models.PidMutableClassMember
import me.wietlol.wietlang.pid.data.models.PidMutableClassMethod
import me.wietlol.wietlang.pid.data.models.PidMutableCompoundTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableDirectTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableExpression
import me.wietlol.wietlang.pid.data.models.PidMutableFirstClassComponent
import me.wietlol.wietlang.pid.data.models.PidMutableFunction
import me.wietlol.wietlang.pid.data.models.PidMutableFunctionCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutableLocalVariableDeclaration
import me.wietlol.wietlang.pid.data.models.PidMutableMethodCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutablePackage
import me.wietlol.wietlang.pid.data.models.PidMutableParameter
import me.wietlol.wietlang.pid.data.models.PidMutableProgram
import me.wietlol.wietlang.pid.data.models.PidMutableReturnExpression
import me.wietlol.wietlang.pid.data.models.PidMutableStringLiteralExpression
import me.wietlol.wietlang.pid.data.models.PidMutableTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableVariable
import me.wietlol.wietlang.pid.data.models.PidMutableVariableLoadExpression
import me.wietlol.wietlang.pid.data.models.PidPackage
import me.wietlol.wietlang.pid.data.models.PidType
import me.wietlol.wietlang.pid.linker.PidProgramLinker
import me.wietlol.wietlang.utility.invalidType
import java.util.*

internal class ExpressionLinker(
	private val pidProgram: PidMutableProgram,
	mapper: PidToAstMapper,
	linker: PidProgramLinker
) : PidToAstMapper by mapper, PidProgramLinker by linker
{
	private val data: TypeLinkerData
	private val resolver: TypeLinkerResolver = TypeLinkerResolver(linker)
	
	init
	{
		data = TypeLinkerData(
			pidProgram.rootPackage,
			this,
			resolver
		)
	}
	
	fun process()
	{
		pidProgram.rootPackage.process()
	}
	
	private fun PidMutableFirstClassComponent.process()
	{
		when (this)
		{
			is PidMutablePackage -> process()
			is PidMutableClass -> process()
			is PidMutableFunction -> process()
			is PidMutableAnnotation -> process()
			else -> throw IllegalArgumentException(invalidType("package member", type, this))
		}
	}
	
	private fun PidMutableClassMember.process()
	{
		when (this)
		{
			is PidMutableClassMethod -> process()
			else -> throw IllegalArgumentException(invalidType("class member", type, this))
		}
	}
	
	private fun PidMutablePackage.process()
	{
		members
			.filterIsInstance<PidMutableFirstClassComponent>()
			.forEach { it.process() }
	}
	
	private fun PidMutableClass.process()
	{
		data.setCurrentPackage(parentPackage())
		
		data.pushComponentStack()
//		data.pushVariableStack()
		data.pushCurrentTypeStack(this)
//		data.addToComponentStack(typeContainer.typeParameters) // todo type arguments
		
		supertypes.forEach { it.process() }
//		typeParameters.forEach { process(it) } // todo type arguments
		
		members
			.filterIsInstance<PidMutableClassMember>()
			.forEach { it.process() }
		
		annotations.forEach { it.process() }
		
		data.popCurrentTypeStack()
//		data.popVariableStack()
		data.popComponentStack()
	}
	
	private fun PidMutableFunction.process()
	{
		data.setCurrentPackage(parentPackage())
		// todo if extension method, push current type stack
		// todo add type arguments
		
		data.pushComponentStack()
		data.processImports(this)
		
		parameters.forEach { it.process() }
		returnType.process()
		
		annotations.forEach { it.process() }
		
		data.pushComponentStack()
		body?.forEach { it.process() }
		data.popComponentStack()
		
		data.popComponentStack()
	}
	
	private fun PidMutableAnnotation.process()
	{
		data.pushComponentStack()
		data.pushCurrentTypeStack(this)
		
		members.forEach { it.process() }
		
		annotations.forEach { it.process() }
		
		data.popCurrentTypeStack()
		data.popComponentStack()
	}
	
	private fun PidMutableAnnotationMember.process()
	{
		when (this)
		{
			is PidMutableAnnotationValue -> process()
			else -> throw IllegalArgumentException(invalidType("annotation member", type, this))
		}
	}
	
	private fun PidMutableAnnotationValue.process()
	{
		upperBound.process()
	}
	
	private fun PidMutableParameter.process()
	{
		upperBound.process()
	}
	
	private fun PidMutableClassMethod.process()
	{
		data.pushComponentStack()
		// todo add type arguments
		
		parameters.forEach { it.upperBound.process() }
		returnType.process()
		
		annotations.forEach { it.process() }
		
		data.pushComponentStack()
		body?.forEach { it.process() }
		data.popComponentStack()
		
		data.popComponentStack()
	}
	
	private fun PidMutableTypeReference.process()
	{
		when (this)
		{
			is PidMutableDirectTypeReference -> process()
			is PidMutableCompoundTypeReference -> process()
			else -> throw IllegalArgumentException(invalidType("type reference", type, this))
		}
	}
	
	private fun PidMutableDirectTypeReference.process()
	{
		if (referencedTypeId == UUID(0, 0))
		{
			val path = astComponent.namePath.parts
			val type: PidType = resolver.findComponent(path, data)
				?: throw CompilationException("Unresolved reference: ${astComponent.namePath.parts.joinToString(".")}", tokens.first())
			
			referencedTypeId = type.id
		}
	}
	
	private fun PidMutableCompoundTypeReference.process()
	{
		types.forEach { it.process() }
	}
	
	private fun PidMutableAnnotationExpression.process()
	{
		if (annotationId == UUID(0, 0))
			TODO("ew")
		
	}
	
	private fun PidMutableExpression.process()
	{
		when (this)
		{
			is PidMutableLocalVariableDeclaration -> process()
			is PidMutableFunctionCallExpression -> process()
			is PidMutableReturnExpression -> process()
			is PidMutableStringLiteralExpression -> process()
			is PidMutableVariableLoadExpression -> process()
			is PidMutableMethodCallExpression -> process()
			else -> throw IllegalArgumentException(invalidType("expression", type, this))
		}
	}
	
	private fun PidMutableLocalVariableDeclaration.process()
	{
		data.addToComponentStack(this)
		
		annotations.forEach { it.process() }
		
		upperBound.process()
		
		initialValue?.process()
	}
	
	private fun PidMutableFunctionCallExpression.process()
	{
		arguments.forEach { it.process() }
		
		val function: PidMutableFunction = resolver.findComponent(listOf(astComponent.functionName), data)
			?: throw CompilationException("Unresolved reference: ${astComponent.functionName}", tokens.first())
		
		functionId = function.id
		
		arguments.forEachIndexed { index, it -> it.link(function, index) }
	}
	
	private fun PidMutableArgumentExpression.process()
	{
		expression.process()
	}
	
	private fun PidMutableArgumentExpression.link(function: PidMutableFunction, index: Int)
	{
		val alias = astComponent.name
		
		val target =
			if (alias != null)
				function.parameters.single { it.name == alias }
			else
				function.parameters[index]
		
		targetId = target.id
	}
	
	private fun PidMutableReturnExpression.process()
	{
		valueExpression?.process()
	}
	
	private fun PidMutableStringLiteralExpression.process()
	{
		// nothing to do (except return type?)
	}
	
	private fun PidMutableVariableLoadExpression.process()
	{
		val path = astComponent.path.parts // todo dont use a path, paths are member access (or maybe do, this could be interesting)
		
		val variable: PidMutableVariable = resolver.findComponent(path, data)
			?: throw CompilationException("Unresolved reference: ${path.joinToString(".")}", tokens.first())
		
		variableId = variable.id
	}
	
	private fun PidMutableMethodCallExpression.process()
	{
		supplierExpression.process()
		arguments.forEach { it.process() }
		
		val method = resolver.findMethod(supplierExpression, astComponent.functionName, arguments)
			?: throw CompilationException("Unresolved reference: ${astComponent.functionName}", tokens.first())
		
		methodId = method.id
	}
	
	private fun PidComponent.parentPackage(): PidPackage =
		parentComponent!!
			.let {
				if (it is PidPackage)
					it
				else
					it.parentPackage()
			}
}