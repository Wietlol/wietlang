package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.ast.data.models.AstProgram
import me.wietlol.wietlang.pid.LinkedProgram

class AstToPidConverter
{
	fun convert(astProgram: AstProgram): LinkedProgram
	{
		val mapper = SimplePidToAstMapper()
		
		val builder = ProgramStructureBuilder(mapper)
		val pidProgram = builder.buildProgram(astProgram)
		
		TypeLinker(pidProgram, mapper, builder)
			.process()
		
		ExpressionLinker(pidProgram, mapper, builder)
			.process()
		
		return LinkedProgram(pidProgram, builder)
	}
}