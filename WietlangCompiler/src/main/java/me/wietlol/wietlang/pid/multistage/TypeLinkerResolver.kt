package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.ast.data.models.AstNamePath
import me.wietlol.wietlang.pid.data.models.PidArgumentExpression
import me.wietlol.wietlang.pid.data.models.PidClass
import me.wietlol.wietlang.pid.data.models.PidClassMethod
import me.wietlol.wietlang.pid.data.models.PidComponent
import me.wietlol.wietlang.pid.data.models.PidContainer
import me.wietlol.wietlang.pid.data.models.PidExpression
import me.wietlol.wietlang.pid.data.models.PidNamedComponent
import me.wietlol.wietlang.pid.linker.PidProgramLinker

internal class TypeLinkerResolver(
	linker: PidProgramLinker
) : PidProgramLinker by linker
{
	inline fun <reified T : PidComponent> findComponent(path: AstNamePath, data: TypeLinkerData): T? =
		findComponent(path, data, T::class.java)
	
	private fun <T : PidComponent> findComponent(path: AstNamePath, data: TypeLinkerData, type: Class<T>): T? =
		findComponent(path.parts, data, type)
	
	inline fun <reified T : PidComponent> findComponent(parts: List<String>, data: TypeLinkerData): T? =
		findComponent(parts, data, T::class.java)
	
	private fun <T : PidComponent> findComponent(parts: List<String>, data: TypeLinkerData, type: Class<T>): T?
	{
		val name = parts.first()
		val component = data
			.componentStack
			.reversed()
			.asSequence()
			.map { it[name] }
			.filterNotNull()
			.filterIsInstance(type)
			.singleOrNull()
		
		if (component != null)
			return if (parts.size == 1)
				component
			else
				findComponent(component as PidContainer, parts.drop(1), type)
		
		return findComponent(data.currentPackage, parts, type)
			?: findComponent(data.rootPackage, parts, type)
	}
	
	inline fun <reified T : PidComponent> findComponent(head: PidContainer, parts: List<String>): T? =
		findComponent(head, parts, T::class.java)
	
	private fun <T : PidComponent> findComponent(head: PidContainer, parts: List<String>, type: Class<T>): T?
	{
		val first = parts.first()
		
		val item = head.members
			.filterIsInstance<PidNamedComponent>()
			.singleOrNull { it.name == first } // todo overloads
		
		if (item == null || !type.isInstance(item))
			return null
		
		if (parts.size == 1)
			return type.cast(item)
		
		return findComponent(item as PidContainer, parts.drop(1), type)
	}
	
	fun findMethod(supplierExpression: PidExpression, name: String, arguments: List<PidArgumentExpression>): PidClassMethod? // todo deal with extension methods, which arent methods
	{
		val supplierType = supplierExpression.resultType.referencedType as? PidClass
			?: throw IllegalArgumentException("Cannot call methods on a non-class object yet.")
		
		val matches = supplierType
			.members
			.asSequence()
			.filterIsInstance<PidClassMethod>()
			.filter { it.name == name }
			// todo add more filters and priority queue for arguments
			.toList()
		
		if (matches.isEmpty())
			return null
		if (matches.size > 1)
			throw IllegalArgumentException("Ambiguous reference to method.")
		
		val match = matches.single()
		
		return match
	}
	
//	private fun findType(jPackage: JPackage, pathSegments: Array<String>?, index: Int): JTypeDeclaration?
//	{
//		if (pathSegments == null || index >= pathSegments.size)
//			throw JCompilingException("Cannot find package from path \"" + Arrays.toString(pathSegments) + "\".")
//
//		val optionalPackage = jPackage
//			.getPackages()
//			.stream()
//			.filter({ jInnerPackage -> jInnerPackage.getName().equals(pathSegments[index]) })
//			.findFirst()
//
//		if (optionalPackage.isPresent())
//		{
//			return if (index + 1 == pathSegments.size) null else findType(optionalPackage.get(), pathSegments, index + 1)
//		}
//		else
//		{
//			val optionalType = jPackage
//				.getTypes()
//				.stream()
//				.filter({ jInnerType -> jInnerType.getName().equals(pathSegments[index]) })
//				.findFirst()
//
//			return optionalType.map({ jTypeDeclaration -> if (index + 1 == pathSegments.size) jTypeDeclaration else findType(jTypeDeclaration, pathSegments, index + 1) }).orElse(null)
//		}
//	}
}