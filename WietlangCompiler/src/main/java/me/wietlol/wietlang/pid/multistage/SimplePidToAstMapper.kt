package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.ast.data.models.AstNode
import me.wietlol.wietlang.pid.data.models.PidNode
import java.util.*

internal class SimplePidToAstMapper : PidToAstMapper
{
	override val map: MutableMap<PidNode, AstNode> = IdentityHashMap()
}