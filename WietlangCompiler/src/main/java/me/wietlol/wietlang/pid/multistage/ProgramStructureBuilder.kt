package me.wietlol.wietlang.pid.multistage

import me.wietlol.wietlang.ast.data.models.AstAnnotation
import me.wietlol.wietlang.ast.data.models.AstAnnotationExpression
import me.wietlol.wietlang.ast.data.models.AstAnnotationMember
import me.wietlol.wietlang.ast.data.models.AstAnnotationValue
import me.wietlol.wietlang.ast.data.models.AstArgumentExpression
import me.wietlol.wietlang.ast.data.models.AstClass
import me.wietlol.wietlang.ast.data.models.AstClassMember
import me.wietlol.wietlang.ast.data.models.AstClassMethod
import me.wietlol.wietlang.ast.data.models.AstCodeSource
import me.wietlol.wietlang.ast.data.models.AstCodeToken
import me.wietlol.wietlang.ast.data.models.AstExpression
import me.wietlol.wietlang.ast.data.models.AstFileCodeSource
import me.wietlol.wietlang.ast.data.models.AstFirstClassComponent
import me.wietlol.wietlang.ast.data.models.AstFunction
import me.wietlol.wietlang.ast.data.models.AstFunctionCallExpression
import me.wietlol.wietlang.ast.data.models.AstLocalVariableDeclaration
import me.wietlol.wietlang.ast.data.models.AstMethodCallExpression
import me.wietlol.wietlang.ast.data.models.AstNameExpression
import me.wietlol.wietlang.ast.data.models.AstNamePath
import me.wietlol.wietlang.ast.data.models.AstNamedTypeReference
import me.wietlol.wietlang.ast.data.models.AstPackage
import me.wietlol.wietlang.ast.data.models.AstParameter
import me.wietlol.wietlang.ast.data.models.AstProgram
import me.wietlol.wietlang.ast.data.models.AstReturnExpression
import me.wietlol.wietlang.ast.data.models.AstStringLiteralExpression
import me.wietlol.wietlang.ast.data.models.AstTypeReference
import me.wietlol.wietlang.pid.data.models.PidCodeSource
import me.wietlol.wietlang.pid.data.models.PidCodeToken
import me.wietlol.wietlang.pid.data.models.PidComponent
import me.wietlol.wietlang.pid.data.models.PidFileCodeSource
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotation
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationExpression
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationMember
import me.wietlol.wietlang.pid.data.models.PidMutableAnnotationValue
import me.wietlol.wietlang.pid.data.models.PidMutableArgumentExpression
import me.wietlol.wietlang.pid.data.models.PidMutableClass
import me.wietlol.wietlang.pid.data.models.PidMutableClassMember
import me.wietlol.wietlang.pid.data.models.PidMutableClassMethod
import me.wietlol.wietlang.pid.data.models.PidMutableCompoundTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableDirectTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableExpression
import me.wietlol.wietlang.pid.data.models.PidMutableFirstClassComponent
import me.wietlol.wietlang.pid.data.models.PidMutableFunction
import me.wietlol.wietlang.pid.data.models.PidMutableFunctionCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutableLocalVariableDeclaration
import me.wietlol.wietlang.pid.data.models.PidMutableMethodCallExpression
import me.wietlol.wietlang.pid.data.models.PidMutableNamePath
import me.wietlol.wietlang.pid.data.models.PidMutablePackage
import me.wietlol.wietlang.pid.data.models.PidMutableParameter
import me.wietlol.wietlang.pid.data.models.PidMutableProgram
import me.wietlol.wietlang.pid.data.models.PidMutableReturnExpression
import me.wietlol.wietlang.pid.data.models.PidMutableStringLiteralExpression
import me.wietlol.wietlang.pid.data.models.PidMutableTypeReference
import me.wietlol.wietlang.pid.data.models.PidMutableVariableLoadExpression
import me.wietlol.wietlang.pid.data.models.PidNode
import me.wietlol.wietlang.pid.linker.PidProgramLinker
import java.util.*
import kotlin.collections.HashMap

internal class ProgramStructureBuilder(
	private val mapper: PidToAstMapper
) : PidProgramLinker
{
	override val componentRegistry: MutableMap<UUID, PidComponent> = HashMap()
	override val nativeRegistry: MutableMap<String, PidComponent> = HashMap()
	override val treeRegistry: MutableMap<PidNode, PidComponent> = IdentityHashMap()
	
	fun buildProgram(astProgram: AstProgram): PidMutableProgram =
		astProgram.convert()
	
	private fun AstProgram.convert(): PidMutableProgram =
		PidMutableProgram.of(
			rootPackage.convert(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstFirstClassComponent.convert(): PidMutableFirstClassComponent =
		when (this)
		{
			is AstPackage -> convert()
			is AstFunction -> convert()
			is AstClass -> convert()
			is AstAnnotation -> convert()
			else -> throw IllegalArgumentException("Unknown type for AstFirstClassComponent: '${javaClass.name}'.")
		}
	
	private fun AstPackage.convert(): PidMutablePackage =
		PidMutablePackage.of(
			UUID.randomUUID(),
			name,
			members.map { it.convert() }.toMutableSet(),
			annotations.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
			.apply { members.forEach { treeRegistry[it] = this } }
	
	private fun AstFunction.convert(): PidMutableFunction =
		PidMutableFunction.of(
			UUID.randomUUID(),
			name,
			returnType?.convert() ?: PidMutableCompoundTypeReference.of(mutableListOf(), mutableListOf()),
			body?.map { it.convert() }?.toMutableList()?.let { appendReturn(it) },
			parameters.map { it.convert() }.toMutableList(),
			annotations.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
	
	private fun appendReturn(expressions: MutableList<PidMutableExpression>): MutableList<PidMutableExpression> =
		if (expressions.isEmpty() || expressions.last() !is PidMutableReturnExpression)
			expressions.apply { add(PidMutableReturnExpression.of(null, mutableListOf())) }
		else
			expressions
	
	private fun AstClass.convert(): PidMutableClass =
		PidMutableClass.of(
			UUID.randomUUID(),
			name,
			members.map { it.convert() }.toMutableList(),
			annotations.map { it.convert() }.toMutableList(),
			supertypes.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
			.also { `class` ->
				annotations
					.filter { it.namePath.parts == listOf("Native") }
					.map { it.arguments.first().expression as AstStringLiteralExpression }
					.map { it.value }
					.forEach { nativeRegistry[it] = `class` }
			}
			.apply { members.forEach { treeRegistry[it] = this } }
	
	private fun AstAnnotation.convert(): PidMutableAnnotation =
		PidMutableAnnotation.of(
			UUID.randomUUID(),
			name,
			members.map { it.convert() }.toMutableList(),
			annotations.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
			.apply { members.forEach { treeRegistry[it] = this } }
	
	private fun AstExpression.convert(): PidMutableExpression =
		when (this)
		{
			is AstFunctionCallExpression -> convert()
			is AstMethodCallExpression -> convert()
			is AstStringLiteralExpression -> convert()
			is AstReturnExpression -> convert()
			is AstLocalVariableDeclaration -> convert()
			is AstNameExpression -> convert()
			else -> throw IllegalArgumentException("Unknown type for AstExpression: '${javaClass.name}'.")
		}
	
	private fun AstFunctionCallExpression.convert(): PidMutableFunctionCallExpression =
		PidMutableFunctionCallExpression.of(
			arguments.map { it.convert() }.toMutableList(),
			UUID(0, 0),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstMethodCallExpression.convert(): PidMutableExpression =
		PidMutableMethodCallExpression.of(
			supplierExpression.convert(),
			arguments.map { it.convert() }.toMutableList(),
			UUID(0, 0),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstReturnExpression.convert(): PidMutableReturnExpression =
		PidMutableReturnExpression.of(
			valueExpression?.convert(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstLocalVariableDeclaration.convert(): PidMutableLocalVariableDeclaration =
		PidMutableLocalVariableDeclaration.of(
			UUID.randomUUID(),
			annotations.map { it.convert() }.toMutableList(),
			name,
			isVariable,
			upperBound.convert(),
			initialValue?.convert(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
	
	private fun AstNameExpression.convert(): PidMutableVariableLoadExpression =
		PidMutableVariableLoadExpression.of(
			UUID(0, 0),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }

	private fun AstStringLiteralExpression.convert(): PidMutableStringLiteralExpression =
		PidMutableStringLiteralExpression.of(
			value,
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstAnnotationExpression.convert(): PidMutableAnnotationExpression =
		PidMutableAnnotationExpression.of(
			UUID(0, 0),
			arguments.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstArgumentExpression.convert(): PidMutableArgumentExpression =
		PidMutableArgumentExpression.of(
			UUID(0, 0),
			expression.convert(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstTypeReference.convert(): PidMutableTypeReference =
		when (this)
		{
			is AstNamedTypeReference -> convert()
			else -> throw IllegalArgumentException("Unknown type for AstTypeReference: '${javaClass.name}'.")
		}
	
	private fun AstNamedTypeReference.convert(): PidMutableDirectTypeReference =
		PidMutableDirectTypeReference.of(
			UUID(0, 0),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstParameter.convert(): PidMutableParameter =
		PidMutableParameter.of(
			UUID.randomUUID(),
			name,
			upperBound.convert(),
			annotations.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
	
	private fun AstClassMember.convert(): PidMutableClassMember =
		when (this)
		{
			is AstClassMethod -> convert()
			else -> throw IllegalArgumentException("Unknown type for AstClassMember: '${javaClass.name}'.")
		}
	
	private fun AstClassMethod.convert(): PidMutableClassMethod =
		PidMutableClassMethod.of(
			UUID.randomUUID(),
			name,
			returnType?.convert() ?: PidMutableCompoundTypeReference.of(mutableListOf(), mutableListOf()),
			body?.map { it.convert() }?.toMutableList(),
			parameters.map { it.convert() }.toMutableList(),
			annotations.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
	
	private fun AstAnnotationMember.convert(): PidMutableAnnotationMember =
		when (this)
		{
			is AstAnnotationValue -> convert()
			else -> throw IllegalArgumentException("Unknown type for AstAnnotationMember: '${javaClass.name}'.")
		}
	
	private fun AstAnnotationValue.convert(): PidMutableAnnotationValue =
		PidMutableAnnotationValue.of(
			UUID.randomUUID(),
			name,
			upperBound.convert(),
			annotations.map { it.convert() }.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
			.also { componentRegistry[it.id] = it }
	
	private fun AstNamePath.convert(): PidMutableNamePath =
		PidMutableNamePath.of(
			parts.toMutableList(),
			tokens.map { it.convert() }.toMutableList()
		)
			.also { mapper.map[it] = this }
	
	private fun AstCodeToken.convert(): PidCodeToken =
		PidCodeToken.of(
			text,
			type,
			line,
			column,
			source.convert()
		)
	
	private fun AstCodeSource.convert(): PidCodeSource =
		when (this)
		{
			is AstFileCodeSource -> convert()
			else -> throw IllegalArgumentException("Unknown type for CodeSource: '${javaClass.name}'.")
		}
	
	private fun AstFileCodeSource.convert(): PidFileCodeSource =
		PidFileCodeSource.of(
			filePath
		)
}