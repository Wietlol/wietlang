package me.wietlol.wietlang.ast

import com.common.collections.StackedCollection
import me.wietlol.metacode.lexer.CodeSource
import me.wietlol.metacode.lexer.FileSource
import me.wietlol.metacode.lexer.MetaCodeToken
import me.wietlol.metacode.parser.results.ParserResult
import me.wietlol.wietlang.ast.data.models.AstCodeSource
import me.wietlol.wietlang.ast.data.models.AstCodeToken
import me.wietlol.wietlang.ast.data.models.AstFileCodeSource
import me.wietlol.wietlang.ast.data.models.AstMutableAnnotation
import me.wietlol.wietlang.ast.data.models.AstMutableAnnotationExpression
import me.wietlol.wietlang.ast.data.models.AstMutableAnnotationMember
import me.wietlol.wietlang.ast.data.models.AstMutableAnnotationValue
import me.wietlol.wietlang.ast.data.models.AstMutableArgumentExpression
import me.wietlol.wietlang.ast.data.models.AstMutableClass
import me.wietlol.wietlang.ast.data.models.AstMutableClassMember
import me.wietlol.wietlang.ast.data.models.AstMutableClassMethod
import me.wietlol.wietlang.ast.data.models.AstMutableExpression
import me.wietlol.wietlang.ast.data.models.AstMutableFunction
import me.wietlol.wietlang.ast.data.models.AstMutableFunctionCallExpression
import me.wietlol.wietlang.ast.data.models.AstMutableImport
import me.wietlol.wietlang.ast.data.models.AstMutableIntegerLiteralExpression
import me.wietlol.wietlang.ast.data.models.AstMutableLiteralExpression
import me.wietlol.wietlang.ast.data.models.AstMutableLocalVariableDeclaration
import me.wietlol.wietlang.ast.data.models.AstMutableMethodCallExpression
import me.wietlol.wietlang.ast.data.models.AstMutableNameExpression
import me.wietlol.wietlang.ast.data.models.AstMutableNamePath
import me.wietlol.wietlang.ast.data.models.AstMutableNamedTypeReference
import me.wietlol.wietlang.ast.data.models.AstMutableNumberLiteralExpression
import me.wietlol.wietlang.ast.data.models.AstMutablePackage
import me.wietlol.wietlang.ast.data.models.AstMutableParameter
import me.wietlol.wietlang.ast.data.models.AstMutableProgram
import me.wietlol.wietlang.ast.data.models.AstMutableReturnExpression
import me.wietlol.wietlang.ast.data.models.AstMutableStringLiteralExpression
import me.wietlol.wietlang.ast.data.models.AstMutableTypeReference
import me.wietlol.wietlang.ast.data.models.AstNamePath
import me.wietlol.wietlang.ast.data.models.AstProgram

class AstBuilder
{
	private val program: AstMutableProgram = newEmptyProgram()
	
	private var currentPackage = program.rootPackage
	private var importsStack = StackedCollection<AstMutableImport>(HashSet())
	
	fun add(contextTree: ParserResult)
	{
		currentPackage = program.rootPackage
		importsStack = importsStack.push(HashSet())
		
		contextTree.parseFile()
		
		importsStack = importsStack.pop()!!
	}
	
	fun buildProgram(): AstProgram =
		program
	
	private fun ParserResult.parseFile() =
		members["members"]
			.forEach { it.parseFileMember() }
	
	private fun ParserResult.parseFileMember(): Any? =
		when (option?.name)
		{
			"PackageDeclaration" -> members["value"].single().parsePackageDeclaration()
			"ImportStatement" -> members["value"].single().parseImportStatement().also { importsStack.items.add(it) }
			"ClassDeclaration" -> members["value"].single().parseClassDeclaration().also { currentPackage.members.add(it) }
			"FunctionDeclaration" -> members["value"].single().parseFunctionDeclaration().also { currentPackage.members.add(it) }
			"AnnotationDeclaration" -> members["value"].single().parseAnnotationDeclaration().also { currentPackage.members.add(it) }
			else -> throw IllegalArgumentException("Invalid option for file member: ${option?.name}")
		}
	
	private fun ParserResult.parseImportStatement(): AstMutableImport =
		AstMutableImport.of(
			members["path"].single().parseNamePath(),
			members["alias"].singleOrNull()?.text,
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseClassDeclaration(): AstMutableClass =
		AstMutableClass.of(
			members["name"].single().text,
			importsStack.toMutableSet(),
			members["members"].map { it.parseClassMember() }.toMutableList(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			members["supertypes"].map { it.parseTypeReference() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseClassMember(): AstMutableClassMember =
		when (option?.name)
		{
			"MethodDeclaration" -> members["value"].single().parseClassMethod()
			else -> throw IllegalArgumentException("Invalid option for class member: ${option?.name}")
		}
	
	private fun ParserResult.parseClassMethod(): AstMutableClassMethod =
		AstMutableClassMethod.of(
			members["name"].single().text,
			members["returnType"].singleOrNull()?.parseTypeReference(),
			parseFunctionBody(),
			members["parameters"].map { it.parseParameter() }.toMutableList(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseFunctionDeclaration(): AstMutableFunction =
		AstMutableFunction.of(
			members["name"].single().text,
			importsStack.toMutableSet(),
			members["returnType"].singleOrNull()?.parseTypeReference(),
			parseFunctionBody(),
			members["parameters"].map { it.parseParameter() }.toMutableList(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseAnnotationDeclaration(): AstMutableAnnotation =
		AstMutableAnnotation.of(
			members["name"].single().text,
			importsStack.toMutableSet(),
			members["members"].map { it.parseAnnotationMember() }.toMutableList(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseAnnotationMember(): AstMutableAnnotationMember =
		when (option?.name)
		{
			"ValueDeclaration" -> members["value"].single().parseAnnotationValue()
			else -> throw IllegalArgumentException("Invalid option for annotation member: ${option?.name}")
		}
	
	private fun ParserResult.parseAnnotationValue(): AstMutableAnnotationValue =
		AstMutableAnnotationValue.of(
			members["name"].single().text,
			members["upperBound"].single().parseTypeReference(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun AstMutablePackage.createPackage(path: AstNamePath): AstMutablePackage = createPackage(path.parts)
	
	private fun AstMutablePackage.createPackage(path: List<String>): AstMutablePackage
	{
		val name = path.first()
		
		val childPackage = this.members
			.asSequence()
			.filter { it.name == name }
			.singleOrNull()
		
		return if (childPackage != null)
		{
			when
			{
				childPackage !is AstMutablePackage -> throw IllegalStateException("Cannot load package '${path.joinToString(".")}' found '${childPackage.type}' instead.")
				path.size == 1 -> childPackage
				else -> childPackage.createPackage(path.drop(1))
			}
		}
		else
		{
			if (path.size == 1)
				newEmptyPackage(name)
					.also { members.add(it) }
			else
				newEmptyPackage(name)
					.also { members.add(it) }
					.createPackage(path.drop(1))
		}
	}
	
	private fun AstMutablePackage.findPackage(path: AstNamePath): AstMutablePackage? = findPackage(path.parts)
	
	private fun AstMutablePackage.findPackage(path: List<String>): AstMutablePackage?
	{
		val name = path.first()
		
		val childPackage = this.members
			.asSequence()
			.filter { it.name == name }
			.singleOrNull()
			?: return null
		
		return when
		{
			childPackage !is AstMutablePackage -> throw IllegalStateException("Cannot load package '${path.joinToString(".")}' found '${childPackage.type}' instead.")
			path.size == 1 -> childPackage
			else -> childPackage.findPackage(path.drop(1))
		}
	}
	
	private fun createPackage(path: AstNamePath): AstMutablePackage
	{
		// search for existing package in
		// - current
		val currChild = currentPackage.findPackage(path)
		if (currChild != null)
			return currChild
		// - root
		if (currentPackage != program.rootPackage)
		{
			val rootChild = program.rootPackage.findPackage(path)
			if (rootChild != null)
				return rootChild
		}
		
		// create new package in current
		return currentPackage.createPackage(path)
	}
	
	private fun ParserResult.parsePackageDeclaration()
	{
		val newPackage = createPackage(members["name"].single().parseNamePath())
		
		if (members["content"].isEmpty())
		{
			currentPackage = newPackage
		}
		else
		{
			val oldPackage = currentPackage
			currentPackage = oldPackage
			importsStack = importsStack.push(HashSet())
			
			members["members"]
				.forEach { it.parseFileMember() }
			
			importsStack = importsStack.pop()!!
			currentPackage = oldPackage
		}
	}
	
	private fun ParserResult.parseFunctionBody(): MutableList<AstMutableExpression>? =
		when
		{
			members["bodyExpressions"].isNotEmpty() ->
				members["bodyExpressions"]
					.map { it.parseExpression() }
					.toMutableList()
			members["returnExpression"].isNotEmpty() -> null
			else -> null
		}
	
	private fun ParserResult.parseExpression(): AstMutableExpression =
		when (option?.name)
		{
			"NameExpression" -> parseNameExpression()
			"ReturnExpression" -> parseReturnExpression()
			"FunctionCallExpression" -> parseFunctionCallExpression()
			"MethodCallExpression" -> parseMethodCallExpression()
			"LiteralExpression" -> members["value"].single().parseLiteralExpression()
			"LocalValueDeclaration" -> parseLocalValueDeclaration()
			else -> throw IllegalArgumentException("Invalid option for expression: ${option?.name}")
		}
	
	private fun ParserResult.parseNameExpression(): AstMutableNameExpression =
		AstMutableNameExpression.of(
			members["name"].single().parseNamePath(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseReturnExpression(): AstMutableReturnExpression =
		AstMutableReturnExpression.of(
			members["expression"].singleOrNull()?.parseExpression(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseFunctionCallExpression(): AstMutableFunctionCallExpression =
		AstMutableFunctionCallExpression.of(
			members["name"].single().text,
			members["arguments"].map { it.parseArgument() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseMethodCallExpression(): AstMutableMethodCallExpression =
		AstMutableMethodCallExpression.of(
			members["supplier"].single().parseExpression(),
			members["name"].single().text,
			members["arguments"].map { it.parseArgument() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseArgument(): AstMutableArgumentExpression =
		AstMutableArgumentExpression.of(
			members["name"].singleOrNull()?.text,
			members["expression"].single().parseExpression(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseLiteralExpression(): AstMutableLiteralExpression =
		when (option?.name)
		{
			"NumberLiteral" -> members["value"].single().parseNumberLiteralExpression()
			"StringLiteral" -> members["value"].single().parseStringLiteralExpression()
			else -> throw IllegalArgumentException("Invalid option for literal expression: ${option?.name}")
		}
	
	private fun ParserResult.parseNumberLiteralExpression(): AstMutableNumberLiteralExpression =
		when (option?.name)
		{
			"IntegerLiteral" -> parseIntegerLiteralExpression()
			else -> throw IllegalArgumentException("Invalid option for number literal expression: ${option?.name}")
		}
	
	private fun ParserResult.parseStringLiteralExpression(): AstMutableStringLiteralExpression =
		AstMutableStringLiteralExpression.of(
			members["value"].single().text.let { WietlangStringParser.parse(it) },
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseIntegerLiteralExpression(): AstMutableIntegerLiteralExpression =
		AstMutableIntegerLiteralExpression.of(
			members["value"].single().text.let { WietlangIntegerParser.parse(it) },
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseLocalValueDeclaration(): AstMutableLocalVariableDeclaration =
		AstMutableLocalVariableDeclaration.of(
			members["name"].single().text,
			false,
			members["upperBound"].single().parseTypeReference(),
			members["initialValue"].singleOrNull()?.parseExpression(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseParameter(): AstMutableParameter =
		AstMutableParameter.of(
			members["name"].single().text,
			members["type"].single().parseTypeReference(),
			members["annotations"].map { it.parseAnnotationExpression() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseTypeReference(): AstMutableTypeReference =
		when (option?.name)
		{
			"NamedTypeReference" -> parseNamedTypeReference()
			else -> throw IllegalArgumentException("Invalid option for type reference: ${option?.name}")
		}
	
	private fun ParserResult.parseNamedTypeReference(): AstMutableNamedTypeReference =
		AstMutableNamedTypeReference.of(
			members["name"].single().parseNamePath(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseAnnotationExpression(): AstMutableAnnotationExpression =
		AstMutableAnnotationExpression.of(
			members["name"].single().parseNamePath(),
			members["arguments"].map { it.parseArgument() }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun ParserResult.parseNamePath(): AstMutableNamePath =
		AstMutableNamePath.of(
			members["parts"].map { it.text }.toMutableList(),
			tokens.map { it.parseToken() }.toMutableList()
		)
	
	private fun MetaCodeToken.parseToken(): AstCodeToken =
		AstCodeToken.of(
			text,
			type,
			line,
			column,
			source.parse()
		)
	
	private fun CodeSource.parse(): AstCodeSource =
		when (this)
		{
			is FileSource -> parse()
			else -> throw IllegalArgumentException("Unknown type for CodeSource: '${javaClass.name}'.")
		}
	
	private fun FileSource.parse(): AstFileCodeSource =
		AstFileCodeSource.of(
			filePath
		)
	
	companion object
	{
		private fun newEmptyProgram(): AstMutableProgram =
			AstMutableProgram.of(
				newEmptyPackage(""),
				mutableListOf()
			)
		
		private fun newEmptyPackage(name: String): AstMutablePackage =
			AstMutablePackage.of(
				name,
				HashSet(),
				mutableListOf(),
				mutableListOf(),
				mutableListOf()
			)
	}
}