package me.wietlol.wietlang.ast

object WietlangIntegerParser
{
	fun parse(code: String): Int =
		code.replace("_", "").toInt()
}