package me.wietlol.wietlang.utility

import me.wietlol.metacode.parser.results.ParserResult

fun invalidType(category: String, type: String, instance: Any) =
	"Invalid type for $category: $type ${instance.javaClass.name}"

fun invalidOption(category: String, result: ParserResult) =
	"Invalid option for $category: ${result.option?.name ?: "null"}"