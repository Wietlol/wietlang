
grammar Wietlang;

file
	:	(members=member)* EOF
	;

member
	:	value=packageDeclaration		#lPackageDeclaration
	|	value=importStatement			#lImportStatement
	|	value=classDeclaration			#lClassDeclaration
	|	value=functionDeclaration		#lFunctionDeclaration
	|	value=annotationDeclaration		#lAnnotationDeclaration
	;
	
packageDeclaration
	:	(annotations+=annotation)* 'package' (name=namePath)
		(content= '{' (members+=member)* '}')?
	;

importStatement
	:	'import' (path=namePath) ('as' alias=Identifier)?
	;

classDeclaration
	:	(annotations+=annotation)* 'class' (name=Identifier)
		('extends' (supertypes+=typeReference) (',' supertypes+=typeReference)* ','?)?
		('{' (members+=classMember)* '}')?
	;
	
functionDeclaration
	:	(annotations=annotation)*
		'function'
		(name=Identifier)
		'(' (parameters+=parameter (',' parameters+=parameter)* ','?)? ')'
		('-' '>' ((returnTypes+=typeReference) | '(' (returnTypes+=typeReference) (',' returnTypes+=typeReference)* ','? ')'))?
		('{' (bodyExpressions+=expression)* '}')?
	;

annotationDeclaration
	: 'annotation' (name=Identifier)
		('{' (members=annotationMember)* '}' )?
	;

classMember
	: value=constructorDeclaration	#lConstructorDeclaration
	| value=classMethodDeclaration	#lClassMethodDeclaration
	;

annotationMember
	: value=annotationValueDeclaration	#lValueDeclaration
	;

annotationValueDeclaration
	: 'value' (name=Identifier) ':' (upperBound=typeReference)
	;

constructorDeclaration
	: (annotations+=annotation)* 'constructor' '(' ')'
	;

classMethodDeclaration
	:	(annotations=annotation)*
		'method'
		(name=Identifier)
		'(' (parameters+=parameter (',' parameters+=parameter)* ','?)? ')'
		('-' '>' ((returnTypes+=typeReference) | '(' (returnTypes+=typeReference) (',' returnTypes+=typeReference)* ','? ')'))?
		('{' (bodyExpressions+=expression)* '}')?
	;

annotation
	: '@' (name=namePath) ('(' ((arguments+=argument) (',' arguments+=argument)* ','?)? ')')?
	;

parameter
	: (name=Identifier) (':' ((type=typeReference) | (implicitType='implicit')))?
	;

typeReference
	:	name=namePath																#lNamedTypeReference
	|	'(' references=typeReference ('|' references=typeReference)+ ')'			#lUnionTypeReference
	|	'(' references=typeReference ('&' references=typeReference)+ ')'			#lIntersectionTypeReference
	|	'(' references=typeReference (',' references=typeReference)+ ','? ')'		#lCompoundTypeReference
	;

namePath
	: (parts+=Identifier) ('.' parts+=Identifier)*
	;

expression
	: 'return' mExpression=expression																					#lReturnExpression
	| (name=Identifier) '(' ((arguments+=argument) (',' arguments+=argument)* ','?)? ')'								#lFunctionCallExpression
	| value=literal																										#lLiteralExpression
	| name=namePath																										#lNameExpression
	| supplier=expression '.' name=Identifier '(' (arguments+=argument (',' arguments+=argument)* ','?)? ')'			#lMethodCallExpression
	;

argument
	: (annotations+=annotation)* ((name=Identifier) ':')? mExpression=expression		#lArgument
	;

literal
	: value=numberLiteral		#lNumberLiteral
	| value=stringLiteral		#lStringLiteral
	;
	
numberLiteral
	: value=Integer
	;
	
stringLiteral
	: value=String
	;

Identifier
	: [a-zA-Z] [a-zA-Z0-9_]*
	;

Integer
	: [0-9] [0-9_]
	;

String
	: '"' ( ~[\\] | '\\' . )* '"'
	;

SingleLineComment
	: '//' ~[\r\n] -> skip
	;

MultiLineComment
	: '/*' .*? '*/' -> skip
	;

Witespace
	: [ \t\r\n] -> skip
	;