
@Module("WietlangAst")
@Owner("Wietlol")
@Version("1.0")

@RootPackage("me.wietlol.wietlang.ast.data")
@BuilderPackage(".builders")
@ModelPackage(".models")
@SerializerPackage(".serializers")

;

blueprint AstCodeToken {
	property(1) text: String
    property(2) type: String
    property(3) line: Integer
    property(4) column: Integer
    property(5) source: AstCodeSource
}

contract AstCodeSource {
}

blueprint AstFileCodeSource : AstCodeSource {
	property(1) filePath: String
}

contract AstNode {
	value type: String
	value tokens: List<AstCodeToken>
}

@Mutable("AstMutableComponent")
contract AstComponent : AstNode {
	value annotations: List<AstAnnotationExpression>
}

@Mutable("AstMutableNamedComponent")
contract AstNamedComponent : AstComponent {
	value name: String
}

@Mutable("AstMutableFirstClassComponent")
contract AstFirstClassComponent : AstNamedComponent {
	value imports: Collection<AstImport>
}

@Mutable("AstMutableProgram")
blueprint AstProgram : AstNode {
	value type: String = "program"
	property(1) rootPackage: AstPackage
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableNamePath")
blueprint AstNamePath : AstNode {
	value type: String = "namePath"
	property(1) parts: List<String>
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableImport")
blueprint AstImport : AstNode {
	value type: String = "import"
	property(1) path: AstNamePath
	property(2) alias: String?
	property(3) tokens: List<AstCodeToken>
}

@Mutable("AstMutableAnnotation")
blueprint AstAnnotation : AstFirstClassComponent {
	value type: String = "annotation"
	property(1) name: String
	property(2) imports: Collection<AstImport>
	property(3) members: List<AstAnnotationMember>
	property(4) annotations: List<AstAnnotationExpression>
	property(5) tokens: List<AstCodeToken>
}

@Mutable("AstMutableClass")
blueprint AstClass : AstFirstClassComponent {
	value type: String = "class"
	property(1) name: String
	property(2) imports: Collection<AstImport>
	property(3) members: List<AstClassMember>
	property(4) annotations: List<AstAnnotationExpression>
	property(5) supertypes: List<AstTypeReference>
	property(6) tokens: List<AstCodeToken>
}

@Mutable("AstMutableFunction")
blueprint AstFunction : AstFirstClassComponent {
	value type: String = "function"
	property(1) name: String
	property(2) imports: Collection<AstImport>
	property(3) returnType: AstTypeReference?
	property(4) body: List<AstExpression>?
	property(5) parameters: List<AstParameter>
	property(6) annotations: List<AstAnnotationExpression>
	property(7) tokens: List<AstCodeToken>
}

@Mutable("AstMutablePackage")
blueprint AstPackage : AstFirstClassComponent {
	value type: String = "package"
	property(1) name: String
	property(2) imports: Collection<AstImport>
	property(3) members: List<AstFirstClassComponent>
	property(4) annotations: List<AstAnnotationExpression>
	property(8) tokens: List<AstCodeToken>
}

@Mutable("AstMutableAnnotationMember")
contract AstAnnotationMember : AstComponent {
}

@Mutable("AstMutableClassMember")
contract AstClassMember : AstComponent {
}

@Mutable("AstMutableAnnotationValue")
blueprint AstAnnotationValue : AstAnnotationMember, AstNamedComponent {
	value type: String = "annotationProperty"
	property(1) name: String
	property(2) upperBound: AstTypeReference
	property(3) annotations: List<AstAnnotationExpression>
	property(4) tokens: List<AstCodeToken>
}

@Mutable("AstMutableClassMethod")
blueprint AstClassMethod : AstClassMember, AstNamedComponent {
	value type: String = "method"
	property(1) name: String
	property(2) returnType: AstTypeReference?
	property(3) body: List<AstExpression>?
	property(4) parameters: List<AstParameter>
	property(5) annotations: List<AstAnnotationExpression>
	property(6) tokens: List<AstCodeToken>
}

@Mutable("AstMutableAnnotationExpression")
blueprint AstAnnotationExpression : AstNode {
	value type: String = "annotationExpression"
	property(1) namePath: AstNamePath
	property(2) arguments: List<AstArgumentExpression>
	property(3) tokens: List<AstCodeToken>
}

@Mutable("AstMutableTypeReference")
contract AstTypeReference : AstNode {
}

@Mutable("AstMutableNamedTypeReference")
blueprint AstNamedTypeReference : AstTypeReference {
	value type: String = "namedTypeReference"
	property(1) namePath: AstNamePath
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableCompoundTypeReference")
blueprint AstCompoundTypeReference : AstTypeReference {
	value type: String = "compoundTypeReference"
	property(1) types: List<AstTypeReference>
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableAnyOfTypeReference")
blueprint AstAnyOfTypeReference : AstTypeReference {
	value type: String = "anyOfTypeReference"
	property(1) types: List<AstTypeReference>
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableAllOfTypeReference")
blueprint AstAllOfTypeReference : AstTypeReference {
	value type: String = "allOfTypeReference"
	property(1) types: List<AstTypeReference>
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableVariable")
contract AstVariable : AstNamedComponent {
	value upperBound: AstTypeReference
}

@Mutable("AstMutableParameter")
blueprint AstParameter : AstVariable {
	value type: String = "parameter"
	property(1) name: String
	property(2) upperBound: AstTypeReference
	property(3) annotations: List<AstAnnotationExpression>
	property(4) tokens: List<AstCodeToken>
}

@Mutable("AstMutableExpression")
contract AstExpression : AstNode {

}

@Mutable("AstMutableArgumentExpression")
blueprint AstArgumentExpression : AstNode {
	value type: String = "argumentExpression"
	property(1) name: String?
	property(2) expression: AstExpression
	property(3) tokens: List<AstCodeToken>
}

@Mutable("AstMutableFunctionCallExpression")
blueprint AstFunctionCallExpression : AstExpression {
	value type: String = "functionCallExpression"
	property(1) functionName: String
	property(2) arguments: List<AstArgumentExpression>
	property(3) tokens: List<AstCodeToken>
}

@Mutable("AstMutableMethodCallExpression")
blueprint AstMethodCallExpression : AstExpression {
	value type: String = "methodCallExpression"
	property(1) supplierExpression: AstExpression
	property(2) functionName: String
	property(3) arguments: List<AstArgumentExpression>
	property(4) tokens: List<AstCodeToken>
}

@Mutable("AstMutableNameExpression")
blueprint AstNameExpression : AstExpression {
	value type: String = "nameExpression"
	property(1) path: AstNamePath
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableReturnExpression")
blueprint AstReturnExpression : AstExpression {
	value type: String = "returnExpression"
	property(1) valueExpression: AstExpression?
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableLiteralExpression")
contract AstLiteralExpression : AstExpression {
	value value: Anything
}

@Mutable("AstMutableNumberLiteralExpression")
contract AstNumberLiteralExpression : AstLiteralExpression {
	value value: Number
}

@Mutable("AstMutableIntegerLiteralExpression")
blueprint AstIntegerLiteralExpression : AstNumberLiteralExpression {
	value type: String = "integerLiteralExpression"
	property(1) value: Integer
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableStringLiteralExpression")
blueprint AstStringLiteralExpression : AstLiteralExpression {
	value type: String = "stringLiteralExpression"
	property(1) value: String
	property(2) tokens: List<AstCodeToken>
}

@Mutable("AstMutableLocalVariableDeclaration")
blueprint AstLocalVariableDeclaration : AstExpression, AstVariable {
	value type: String = "variableDeclaration"
	property(1) name: String
	property(2) isVariable: Boolean
	property(3) upperBound: AstTypeReference
	property(4) initialValue: AstExpression?
	property(5) annotations: List<AstAnnotationExpression>
	property(6) tokens: List<AstCodeToken>
}
